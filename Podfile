# Uncomment the next line to define a global platform for your project
platform :ios, '11.0'

workspace 'Example'

use_frameworks!

# ignore all warnings from all pods
inhibit_all_warnings!

abstract_target 'Example Abstract' do
  
  # Custom image picker
  pod 'YPImagePicker'
  
  # Kingfisher
  pod 'Kingfisher'
  
  # Input formatting
  pod 'InputMask'
  pod 'PhoneNumberKit', '~> 3.1'
  
  # Google maps
  pod 'GoogleMaps'
  pod 'GooglePlaces'
  
  # SnapKit
  pod 'SnapKit', '~> 5.0.0'

  # Uber RIBs
  pod 'RIBs', git: 'https://github.com/uber/RIBs.git', branch: 'master', submodules: true
  
  # Rating view
  pod 'Cosmos', '~> 21.0'
  
  # TextView + Placeholder
  pod 'UITextView+Placeholder'
  
  # Firebase
  pod 'Firebase/Auth'
  pod 'Firebase/Core'
  pod 'Firebase/Database'
  pod 'Firebase/Storage'
  pod 'Firebase/Firestore'
  pod 'Firebase/Messaging'
  
  # Codable firebase
  pod 'CodableFirebase'
  
  # RxSwift
  pod 'RxSwift'
  pod 'RxCocoa'
  
  # RxGoogleMaps
  
  pod 'RxGoogleMaps'
  
  # RxCoreLocation
  pod 'RxCoreLocation', '~> 1.4'
  
  # RxFirebase
  pod 'RxFirebase/Firestore'
  pod 'RxFirebase/Storage'
  pod 'RxFirebase/Auth'
  
  # Firebase Crashlytics
  pod 'Fabric'
  pod 'Crashlytics'

  # (Recommended) Pod for Google Analytics
  pod 'Firebase/Analytics'

  target 'Example Dev' do
    pod 'SwiftLint'
  end
  
  target 'Example'
  
end
