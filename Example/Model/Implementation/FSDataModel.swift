//
//  File.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 29.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift
import FirebaseFirestore
import RxFirebaseFirestore
import CodableFirebase
import RxCocoa

class FirestoreDataModel {
    
    var firestore: Firestore
    var decoder: FirestoreDecoder
    
    init(with firestore: Firestore, decoder: FirestoreDecoder) {
        self.firestore = firestore
        self.decoder = decoder
    }
    
    enum Path: String {
        case orders = "orders"
        case specialization = "/service/categories/specializations"
        case specializationGroup = "/service/categories/specializationGroups"
    }
    
    internal func collection(_ path: Path) -> CollectionReference {
        firestore.collection(path.rawValue)
    }
}

extension FirestoreDataModel {
    
    internal func decode<T: Decodable & Unique>(_ type: T.Type,
                                                from container: [String: Any],
                                                with identifier: String? = nil) throws -> T {
        do {
            var model = try decoder.decode(type, from: container)
            model.identifier = identifier
            return model
        } catch {
            // Handle error
            print(error.localizedDescription)
            throw error
        }
    }
    
    internal func subscribeOn<T: Unique & Decodable>(query: Query) -> Observable<[Change<T>]> {
        return query.rx
            .listen()
            .map { [unowned self] (snapshot) -> [Change<T>] in
                return snapshot.documentChanges.map { change in
                    let identifier = change.document.documentID
                    let changeType: ChangeType = {
                        switch change.type {
                        case .added: return .added
                        case .removed: return .removed
                        case .modified: return .modified
                        }
                    }()
                    let data = change.document.data()
                    let item = try? self.decode(T.self, from: data, with: identifier)
                    return Change(identifier: identifier, item: item, type: changeType)
                }
            }
    }
    
    internal func getList<T: Unique & Decodable>(query: Query) -> Observable<[T]> {
        return query.rx
            .getDocuments()
            .compactMap { [unowned self] (snapshot) -> [T] in
                return snapshot.documents.compactMap { (documentSnapshot) -> T? in
                    let data = documentSnapshot.data()
                    let identifier = documentSnapshot.documentID
                    let item = try? self.decode(T.self, from: data, with: identifier)
                    return item
                }
            }
    }
    
}

class FSDataModel: FirestoreDataModel, DataModel {
    
    lazy var specializations: Observable<[Specialization]> = {
        let query = collection(.specialization)
        return getList(query: query)
    }()
    
    lazy var specializationGroups: Observable<[SpecializationGroup]> = {
        let query = collection(.specializationGroup)
        return getList(query: query)
    }()
    
    lazy var order: OrderDataModel = FSOrderDataModel(with: firestore, decoder: decoder)
    
    lazy var user: UserDataModel = FSUserDataModel(with: firestore, decoder: decoder)
    
    lazy var orders: OrdersDataModel = FSOrdersDataModel(with: firestore, decoder: decoder)

}

extension FSDataModel {
    class FSOrderDataModel: FirestoreDataModel, OrderDataModel {
        
        func create() -> String {
            return collection(.orders).document().documentID
        }
        
        func delete(orderId: String) -> Observable<Void> {
            return collection(.orders).document(orderId).rx.delete()
        }
        
        func setDescription(orderId: String, _ value: String?) -> Observable<Void> {
            let data = [
                Order.CodingKeys.description.rawValue: value as Any
            ]
            return setData(identifier: orderId, data: data)
        }
        
        func setSpecialization(orderId: String, _ value: Specialization?) -> Observable<Void> {
            let data = [
                Order.CodingKeys.mainCategoryName.rawValue: value?.name as Any,
                Order.CodingKeys.mainCategoryId.rawValue: value?.identifier as Any
            ]
            return setData(identifier: orderId, data: data)
        }
        
        func setSpecializationGroup(orderId: String, _ value: SpecializationGroup?) -> Observable<Void> {
            let data = [
                Order.CodingKeys.subCategoryName.rawValue: value?.name as Any,
                Order.CodingKeys.subCategoryId.rawValue: value?.identifier as Any
            ]
            return setData(identifier: orderId, data: data)
        }
        
        func setOwnerIdentifier(orderId: String, _ value: String?) -> Observable<Void> {
            let data = [
                Order.CodingKeys.ownerIdentifier.rawValue: value as Any
            ]
            return setData(identifier: orderId, data: data)
        }
        
        func setLocation(orderId: String, _ value: Location?) -> Observable<Void> {
            let data = [
                Order.CodingKeys.locationName.rawValue: value?.name as Any,
                Order.CodingKeys.locationCoordinates.rawValue: value?.coordinates as Any
            ]
            return setData(identifier: orderId, data: data)
        }
        
        func setPhotos(orderId: String, _ value: [Photo]?) -> Observable<Void> {
            let data = [
                Order.CodingKeys.attachedPhotosUrs.rawValue: value?.compactMap { $0.downloadUrlString } as Any
            ]
            return setData(identifier: orderId, data: data)
        }
        
        func setMinBudget(orderId: String, _ value: Double?) -> Observable<Void> {
            let data = [
                Order.CodingKeys.minBudget.rawValue: value as Any
            ]
            return setData(identifier: orderId, data: data)
        }
        
        func setMaxBudget(orderId: String, _ value: Double?) -> Observable<Void> {
            let data = [
                Order.CodingKeys.maxBudget.rawValue: value as Any
            ]
            return setData(identifier: orderId, data: data)
        }
        
        func setDisplayName(orderId: String, _ value: String?) -> Observable<Void> {
            let data = [
                Order.CodingKeys.authorName.rawValue: value as Any
            ]
            return setData(identifier: orderId, data: data)
        }
        
        func setDisplayPhoneNumber(orderId: String, _ value: String?) -> Observable<Void> {
            let data = [
                Order.CodingKeys.displayPhoneNumber.rawValue: value as Any
            ]
            return setData(identifier: orderId, data: data)
        }
        
        func setDisplayContactsPermission(orderId: String, _ isAvailable: Bool?) -> Observable<Void> {
            let data = [
                Order.CodingKeys.displayContactsPermission.rawValue: isAvailable as Any
            ]
            return setData(identifier: orderId, data: data)
        }
        
        func setStatus(orderId: String, _ value: OrderStatus?) -> Observable<Void> {
            let data = [
                Order.CodingKeys.status.rawValue: value?.rawValue as Any
            ]
            return setData(identifier: orderId, data: data)
        }
        
        func setDateCreation(orderId: String, _ value: Date?) -> Observable<Void> {
            let timestamp = dateToTimestamp(value)
            let data = [
                Order.CodingKeys.dateCreation.rawValue: timestamp as Any
            ]
            return setData(identifier: orderId, data: data)
        }
        
        // MARK: Helpers
        
        private func setData(identifier: String, data: [String: Any]) -> Observable<Void> {
            return collection(.orders)
                .document(identifier)
                .rx
                .setData(data, merge: true)
        }
        
        private func dateToTimestamp(_ date: Date?) -> Timestamp? {
            if let date = date {
                return Timestamp(date: date)
            } else {
                return nil
            }
        }
        
    }
}

extension FSDataModel {
    class FSUserDataModel: FirestoreDataModel, UserDataModel {
        
        func getOrders(uid: String) -> Observable<[Change<Order>]> {
            let fieldPath = Order.CodingKeys.ownerIdentifier.rawValue
            let query = collection(.orders).whereField(fieldPath, isEqualTo: uid)
            return subscribeOn(query: query)
        }
        
    }
}
extension FSDataModel {
    class FSOrdersDataModel: FirestoreDataModel, OrdersDataModel {
        
        func subscribeOnPublic() -> Observable<[Change<Order>]> {
            let fieldPath = Order.CodingKeys.status.rawValue
            let statusValue = OrderStatus.publishing.rawValue
            let query = collection(.orders).whereField(fieldPath, isEqualTo: statusValue)
            return subscribeOn(query: query)
        }
        
    }
}
