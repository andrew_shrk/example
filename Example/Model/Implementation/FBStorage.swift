//
//  FBStorage.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 02.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import FirebaseStorage
import RxFirebaseStorage
import RxSwift
import UIKit

class FBStorage: StorageModel {
    
    private let storage: Storage
    
    init(storage: Storage) {
        self.storage = storage
    }
    
    func uploadOrderImage(orderId: String, image: UIImage) -> ImageUploadTask {
        let imageName = UUID().uuidString
        let reference = storage.reference()
            .child("orders")
            .child("order_\(orderId)")
            .child("\(imageName).jpg")
        return ImageUploadTask(reference: reference, image: image)
    }
    
    func uploadUserAvatar(userId: String, image: UIImage) -> ImageUploadTask {
        let reference = storage.reference(withPath: "users/\(userId)")
        return ImageUploadTask(reference: reference, image: image)
    }
    
    func delete(reference: StorageReference) -> Observable<Void> {
        return reference.rx.delete()
    }
    
    func deletePhotos(_ photos: [Photo]) -> Observable<[Void]> {
        let photoPaths = photos.compactMap { $0.storagePath }
        let references = photoPaths.map { storage.reference().child($0) }
        let deleteObservables = references.map { $0.rx.delete() }
        if deleteObservables.count == 0 {
            return Observable.just([])
        }
        return Observable.zip(deleteObservables)
    }
    
    private func uploadImage(reference: StorageReference, data: Data) -> Observable<StorageMetadata> {
        return reference.rx.putData(data)
    }
    
}

class ImageUploadTask {
    
    init(reference: StorageReference, image: UIImage) {
        self.storageReference = reference
        self.image = image
    }
    
    var image: UIImage
    var storageReference: StorageReference
    
    var downloadUrl: URL?
    
    var isCompleted: Bool = false
    
    var onSuccess: ((ImageUploadTask) -> Void)?
    var onError: ((ImageUploadTask, Error) -> Void)?

    private var disposeBag: DisposeBag?
    
    @objc func cancel() {
        disposeBag = nil
    }
    
    func upload() {
        if disposeBag != nil {
            return
        }
        let disposeBag = DisposeBag()
        self.disposeBag = disposeBag
        let uploadImageObservable = uploadImage(storageReference, image: image)
       
        uploadImageObservable
            .subscribe(onNext: { [unowned self] _ in
                self.getDownloadUrl()
            }, onError: { [unowned self] (error) in
                self.onError?(self, error)
            })
            .disposed(by: disposeBag)
    }
    private func getDownloadUrl() {
        guard let disposeBag = disposeBag else {
            return
        }
        storageReference.rx
            .downloadURL()
            .subscribe(onNext: { [unowned self] (url) in
                self.downloadUrl = url
                self.isCompleted = true
                self.onSuccess?(self)
            }, onError: { [unowned self] (error) in
                self.onError?(self, error)
            })
            .disposed(by: disposeBag)
    }
    private func uploadImage(_ storageReference: StorageReference, image: UIImage) -> Observable<StorageMetadata> {
        guard let data = image.jpegData(compressionQuality: 1.0) else {
            fatalError()
        }
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        return storageReference.rx.putData(data, metadata: metadata)
    }
}

extension ImageUploadTask: Hashable {
    
    static func == (lhs: ImageUploadTask, rhs: ImageUploadTask) -> Bool {
        return lhs.downloadUrl == rhs.downloadUrl
            && lhs.image == rhs.image
            && lhs.storageReference == rhs.storageReference
            && lhs.isCompleted == rhs.isCompleted
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(downloadUrl)
        hasher.combine(image)
        hasher.combine(storageReference)
        hasher.combine(isCompleted)
    }
    
}
