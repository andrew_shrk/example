//
//  DataModel.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 05.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift

/// Эквивалент Firebase DocumentChange
struct Change<T> {
    var identifier: String
    var item: T?
    var type: ChangeType
}

/// Эквивалент Firebase DocumentChange.Type
enum ChangeType {
    case added
    case removed
    case modified
}

/// Фасад для работы с базой данных
protocol DataModel {
    
    var specializations: Observable<[Specialization]> { get }
    
    var specializationGroups: Observable<[SpecializationGroup]> { get }
    
    var order: OrderDataModel { get }
    
    var user: UserDataModel { get }
    
    var orders: OrdersDataModel { get }
    
}


/// Протокол описывает набор методов для работы с заказом
protocol OrderDataModel {
    
    /// Возвращает ID созданного в базе данных заказа
    func create() -> String
    
    /// Метод удаляет заказ из базы данных
    /// - Parameter orderId: ID заказа
    func delete(orderId: String) -> Observable<Void>
    
    /// Метод изменяет текстовое описание заказа
    ///
    /// Если поле с таким значением отсутсвует метод должен его создать
    /// - Parameters:
    ///   - orderId: ID заказа
    ///   - value: Новое текстовое описание заказа
    func setDescription(orderId: String, _ value: String?) -> Observable<Void>
    
    /// Метод изменяет специализацию заказа
    ///
    /// Если поле с таким значением отсутсвует метод должен его создать
    /// - Parameters:
    ///   - orderId: ID заказа
    ///   - value: Новая специализация
    func setSpecialization(orderId: String, _ value: Specialization?) -> Observable<Void>
    
    /// Метод изменяет группу специализации к которой принадлежит заказ
    ///
    /// Если поле с таким значением отсутсвует метод должен его создать
    /// - Parameters:
    ///   - orderId: ID заказа
    ///   - value: Новая группа специализации
    func setSpecializationGroup(orderId: String, _ value: SpecializationGroup?) -> Observable<Void>
    
    /// Метод изменяет ID пользователя - владельца заказа
    ///
    /// Если поле с таким значением отсутсвует метод должен его создать
    /// - Parameters:
    ///   - orderId: ID заказа
    ///   - value: Новое ID пользователя
    func setOwnerIdentifier(orderId: String, _ value: String?) -> Observable<Void>
    
    /// Метод изменяет  текстовое описание заказа
    ///
    /// Если поле с таким значением отсутсвует метод должен его создать
    /// - Parameters:
    ///   - orderId: ID заказа
    ///   - value: Новое значение для текстового описания заказа
    func setLocation(orderId: String, _ value: Location?) -> Observable<Void>
    
    /// Метод изменяет сохраняет фотографии прикреплённые к заказу
    ///
    /// Метод должен, как сохранять фотографии в отдельной коллекции для детального просмотра,
    /// так и устанавливать поле `attached_photos` у самого объекта заказа с ссылками на миниатюры фотографий.
    /// - Parameters:
    ///   - orderId: ID заказа
    ///   - value: Новое значение для текстового описания заказа
    func setPhotos(orderId: String, _ value: [Photo]?) -> Observable<Void>
    
    /// Метод изменяет минимальный бюджет на выполнение заказа
    ///
    /// Если поле с таким значением отсутсвует метод должен его создать
    /// - Parameters:
    ///   - orderId: ID заказа
    ///   - value: Новый минимальный бюджет
    func setMinBudget(orderId: String, _ value: Double?) -> Observable<Void>
    
    /// Метод изменяет максимальный бюджет на выполнение заказа
    ///
    /// Если поле с таким значением отсутсвует метод должен его создать
    /// - Parameters:
    ///   - orderId: ID заказа
    ///   - value: Новый максимальный бюджета
    func setMaxBudget(orderId: String, _ value: Double?) -> Observable<Void>
    
    /// Метод изменяет отображаемое имя пользователя
    ///
    /// Если поле с таким значением отсутсвует метод должен его создать
    /// - Parameters:
    ///   - orderId: ID заказа
    ///   - value: Новое отображаемое имя пользователя
    func setDisplayName(orderId: String, _ value: String?) -> Observable<Void>
    
    /// Метод изменяет отображаемый номер телефона для связи с автором заказа
    ///
    /// Если поле с таким значением отсутсвует метод должен его создать
    /// - Parameters:
    ///   - orderId: ID заказа
    ///   - value: Новый контактный номер телефона
    func setDisplayPhoneNumber(orderId: String, _ value: String?) -> Observable<Void>
    
    /// Метод изменяет разрешения на показ контактных данных пользователя
    ///
    /// Если значение `true`, то контактные данные `доступны другим пользователям`.
    /// Иначе контактные данные не доступны для отображения другим пользователям.
    /// Если поле с таким значением отсутсвует метод должен его создать.
    /// - Parameters:
    ///   - orderId: ID заказа
    ///   - value: Новый статус отображения контактных данных
    func setDisplayContactsPermission(orderId: String, _ isAvailable: Bool?) -> Observable<Void>
    
    /// Метод изменяет статус заказа
    ///
    /// Если поле с таким значением отсутсвует метод должен его создать
    /// - Parameters:
    ///   - orderId: ID заказа
    ///   - value: Новый статус заказа
    func setStatus(orderId: String, _ value: OrderStatus?) -> Observable<Void>
    
    /// Метод изменяет  дату создания заказа
    ///
    /// Если поле с таким значением отсутсвует метод должен его создать
    /// - Parameters:
    ///   - orderId: ID заказа
    ///   - value: Новая дата создания заказа
    func setDateCreation(orderId: String, _ value: Date?) -> Observable<Void>
    
}

/// Протокол описывает набор методов для получения и изменения данных связанных с текущим пользователем
protocol UserDataModel {
    
    /// Метод подписывается на изменения заказов текущего пользователя
    /// - Parameter uid: ID  текущего пользователя
    func getOrders(uid: String) -> Observable<[Change<Order>]>
    
}

/// Протокол описывает набор методов для работы с доступными данному пользователю заказами
protocol OrdersDataModel {
    
    /// Метод создаёт подписку на все публличные заказы
    func subscribeOnPublic() -> Observable<[Change<Order>]>
    
}
