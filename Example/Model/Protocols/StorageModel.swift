//
//  StorageModel.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 13.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift
import FirebaseStorage
import UIKit


/// Протокол описывает методы для работы с хранилищем
protocol StorageModel {
    
    // TODO: Заменить  UIImage на Data
    /// Метод загружает изображение прикреплённые к заказу на сервер
    /// - Parameters:
    ///   - orderId: ID заказа
    ///   - image: Изображение
    func uploadOrderImage(orderId: String, image: UIImage) -> ImageUploadTask
    
    // TODO: Заменить  UIImage на Data
    /// Метод загражает аватар пользователя на сервер
    /// - Parameters:
    ///   - userId: ID пользователя
    ///   - image: Изображение
    func uploadUserAvatar(userId: String, image: UIImage) -> ImageUploadTask
    
    /// Удаляет фотографии с сервера
    /// - Parameter photos: Фотографии для удаления
    func deletePhotos(_ photos: [Photo]) -> Observable<[Void]>
    
    // TODO: Заменить StorageReference на filePath: String
    /// Удаляет изображение по StorageReference
    /// - Parameter reference: Ссылка на фото в CloudStorage
    func delete(reference: StorageReference) -> Observable<Void>
    
}
