//
//  AppComponent.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 10.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

class AppComponent: Component<EmptyDependency>, RootDependency {
    init() {
        super.init(dependency: EmptyComponent())
    }
}
