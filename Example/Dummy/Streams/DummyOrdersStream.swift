//
//  DummyOrdersStream.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 24.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import Foundation
import FirebaseFirestore

class DummyOrdersStream: OrdersStreamImpl {
    
    override init() {
        super.init()
        update(with: dummyValues)
    }
    
    private lazy var dummyValues = [
        Order(identifier: "",
              mainCategoryName: "Отделочные работы",
              subCategoryName: "Маляр",
              description: dummyTexts[0],
              dateCreation: Timestamp(),
              attachedPhotos: [dummyPhoto],
              minBudget: 5000,
              maxBudget: 10000,
              authorName: "Юлия",
              isFavorite: true),
        Order(identifier: "",
              mainCategoryName: "Отделочные работы",
              subCategoryName: "Маляр",
              description: dummyTexts[1],
              dateCreation: Timestamp(),
              attachedPhotos: nil,
              minBudget: 5000,
              maxBudget: nil,
              authorName: "Юлия",
              isFavorite: false),
        Order(identifier: "",
              mainCategoryName: "Отделочные работы",
              subCategoryName: "Маляр",
              description: dummyTexts[1],
              dateCreation: Timestamp(),
              attachedPhotos: [dummyPhoto, dummyPhoto, dummyPhoto],
              minBudget: nil,
              maxBudget: 10000,
              authorName: "Юлия",
            isFavorite: false)
    ]
    
    private lazy var dummyTexts = [
        """
        Нужно покрасить стены и потолок в квартире 43 квадрата в один слой, посредников не беспокоить. За собой убраться
        """,
        """
        Закончить ремонт в квартире , кое что докрасить, вытяжку сделать , линолеум поставить и тд
        Нужно приехать завтра к нам чтобы договориться о работе
        """
    ]
    private var dummyPhoto: Photo {
        let urlIndex = Int.random(in: 0..<photoUrls.count)
        let url = photoUrls[urlIndex]
        return Photo(storagePath: nil, downloadUrlString: url)
    }
    // swiftlint:disable line_length
    private lazy var photoUrls = [
        "https://st3.depositphotos.com/1000423/13153/i/450/depositphotos_131530636-stock-photo-renovation-interior-3d-render.jpg",
        "https://images.ru.prom.st/537352412_kosmeticheskij-remont.jpg",
        "https://88.img.avito.st/640x480/5402497788.jpg",
        "https://rego-remont-msk.ru/assets/images/remontie-raboti/vystavka-komnata/20-dsc01371.jpg"
    ]
}
