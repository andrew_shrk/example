//
//  RIBOrderCreatorInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 26.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift

protocol RIBOrderCreatorRouting: ViewableRouting {
    func presentDescription()
    func presentBudget()
    func presentContacts()
}

protocol RIBOrderCreatorPresentable: Presentable {
    var listener: RIBOrderCreatorPresentableListener? { get set }
    func showCloseAlert(withDraft: Bool)
}

protocol RIBOrderCreatorListener: class {
    func ribOrderCreatorClose()
}

typealias RIBOrderCreatorInteractorType = PresentableInteractor<RIBOrderCreatorPresentable>
    & RIBOrderCreatorInteractable
    & RIBOrderCreatorPresentableListener

final class RIBOrderCreatorInteractor: RIBOrderCreatorInteractorType {

    weak var router: RIBOrderCreatorRouting?
    weak var listener: RIBOrderCreatorListener?

    init(presenter: RIBOrderCreatorPresentable, args: RIBOrderCreatorInteractorArgsProtocol) {
        self.args = args
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        setupDefaultValues()
        subscribe()
        router?.presentDescription()
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - PresenterListener
    
    func presenterDelete() {
        let orderId = args.orderId
        let deleteOrder = args.dataModel.order.delete(orderId: orderId)
        let orderPhotos = args.streams.photos.currentValue
        let deleteOrderPhotos = args.storage.deletePhotos(orderPhotos)
        Observable
            .combineLatest(deleteOrder, deleteOrderPhotos)
            .subscribe(onNext: { [unowned self] _, _ in
                self.listener?.ribOrderCreatorClose()
            })
            .disposeOnDeactivate(interactor: self)
    }
    func presenterSaveDraft() {
        endEditing(withStatus: .draft)
        listener?.ribOrderCreatorClose()
    }
    
    // MARK: - Listeners
    
    // MARK: RIBOCDescriptionListener
    
    func cancel() {
        args.streams
            .validation.value.single()
            .subscribe(onNext: { [unowned self] isValid in
                self.presenter.showCloseAlert(withDraft: isValid)
            })
            .disposeOnDeactivate(interactor: self)
    }
    func publish() {
        endEditing(withStatus: .publishing)
        listener?.ribOrderCreatorClose()
    }
    func ribOCDescriptionContinue() {
        router?.presentBudget()
    }
    
    // MARK: RIBOCBudgetListener
    
    func ribOCBudgetContinue() {
        router?.presentContacts()
    }
    
    // MARK: - Private
    
    private let args: RIBOrderCreatorInteractorArgsProtocol
    
    private func setupDefaultValues() {
        let userDisplayName = args.auth.currentUser?.displayName
        let userDisplayNumber = args.auth.currentUser?.phoneNumber
        args.streams.contacts.updateDisplayName(with: userDisplayName)
        args.streams.contacts.updatePhoneNumber(with: userDisplayNumber)
        args.streams.order.updateContactDisplayPermission(displayAvailable: true)
        args.streams.order.updateStatus(with: .creating)
        args.streams.owner.update(with: args.auth.currentUser?.uid)
    }
    
    // MARK: - RX
    
    private func subscribe() {
        subscribeOnFormValidation()
        subscribeOnDescriptionStream()
        subscribeOnSpecializationStream()
        subscribeOnSpecializationGroupStream()
        subscribeOnOwnerStream()
        subscribeOnPhotosStream()
        subscribeOnLocationStream()
        subscribeOnMinBudgetStream()
        subscribeOnMaxBudgetStream()
        subscribeOnDisplayNameStream()
        subscribeOnDisplayPhoneNumberStream()
        subscribeOnDisplayContactsPermissionStream()
        subscribeOnStatusStream()
        subscribeOnDateCreationStream()
    }
    
    // MARK: Subscriptions
    
    private func subscribeOnFormValidation() {
        let descriptionObserver = args.streams.description.value
        let specializationObserver = args.streams.specialization.value
        Observable.combineLatest(descriptionObserver, specializationObserver)
            .map({ $0 != nil && $0?.count != 0 && $1 != nil })
            .subscribe(onNext: { [unowned self] validation in
                self.args.streams.validation.update(with: validation)
            })
            .disposeOnDeactivate(interactor: self)
    }
    
    private func subscribeOnOwnerStream() {
        args.streams.owner.identifier
            .subscribe(onNext: { [unowned self] value in self.onNextOwnerIdentifier(value) })
            .disposeOnDeactivate(interactor: self)
    }
    private func subscribeOnDescriptionStream() {
        args.streams.description.value
            .subscribe(onNext: { [unowned self] value in self.onNextDescription(value) })
            .disposeOnDeactivate(interactor: self)
    }
    private func subscribeOnSpecializationStream() {
        args.streams.specialization.value
            .subscribe(onNext: { [unowned self] value in self.onNextSpecialization(value) })
            .disposeOnDeactivate(interactor: self)
    }
    private func subscribeOnSpecializationGroupStream() {
        args.streams.specialization.specializationGroupValue
            .subscribe(onNext: { [unowned self] value in self.onNextSpecializationGroup(value) })
            .disposeOnDeactivate(interactor: self)
    }
    private func subscribeOnPhotosStream() {
        args.streams.photos.value
            .subscribe(onNext: { [unowned self] value in self.onNextPhotos(value) })
            .disposeOnDeactivate(interactor: self)
    }
    private func subscribeOnLocationStream() {
        args.streams.location.value
            .subscribe(onNext: { [unowned self] value in self.onNextLocation(value) })
            .disposeOnDeactivate(interactor: self)
    }
    private func subscribeOnMinBudgetStream() {
        args.streams.budget.minBudgetValue
            .subscribe(onNext: { [unowned self] value in self.onNextMinBudget(value) })
            .disposeOnDeactivate(interactor: self)
    }
    private func subscribeOnMaxBudgetStream() {
        args.streams.budget.maxBudgetValue
            .subscribe(onNext: { [unowned self] value in self.onNextMaxBudget(value) })
            .disposeOnDeactivate(interactor: self)
    }
    private func subscribeOnDisplayNameStream() {
        args.streams.contacts.displayNameValue
            .subscribe(onNext: { [unowned self] value in self.onNextDisplayName(value) })
            .disposeOnDeactivate(interactor: self)
    }
    private func subscribeOnDisplayPhoneNumberStream() {
        args.streams.contacts.phoneNumberValue
            .subscribe(onNext: { [unowned self] value in self.onNextDisplayPhoneNumber(value) })
            .disposeOnDeactivate(interactor: self)
    }
    private func subscribeOnDisplayContactsPermissionStream() {
        args.streams.order.contactDisplayPermissionValue
            .subscribe(onNext: { [unowned self] value in self.onNextDisplayContactsPermission(value) })
            .disposeOnDeactivate(interactor: self)
    }
    private func subscribeOnStatusStream() {
        args.streams.order.statusValue
            .subscribe(onNext: { [unowned self] value in self.onNextStatus(value) })
            .disposeOnDeactivate(interactor: self)
    }
    private func subscribeOnDateCreationStream() {
        args.streams.order.dateCreationValue
            .subscribe(onNext: { [unowned self] value in self.onNextDateCreation(value) })
            .disposeOnDeactivate(interactor: self)
    }
    
    // MARK: Handlers
    
    private func onNextOwnerIdentifier(_ value: String?) {
        let orderId = self.args.orderId
        self.orderDataModel
            .setOwnerIdentifier(orderId: orderId, value)
            .subscribe(onError: { error in print(error) })
            .disposeOnDeactivate(interactor: self)
    }
    private func onNextDescription(_ value: String?) {
        let orderId = self.args.orderId
        self.orderDataModel
            .setDescription(orderId: orderId, value)
            .subscribe(onError: { error in print(error) })
            .disposeOnDeactivate(interactor: self)
    }
    private func onNextSpecialization(_ value: Specialization?) {
        let orderId = self.args.orderId
        self.orderDataModel
            .setSpecialization(orderId: orderId, value)
            .subscribe(onError: { error in  print(error) })
            .disposeOnDeactivate(interactor: self)
    }
    private func onNextSpecializationGroup(_ value: SpecializationGroup?) {
        let orderId = self.args.orderId
        self.orderDataModel
            .setSpecializationGroup(orderId: orderId, value)
            .subscribe(onError: { error in  print(error) })
            .disposeOnDeactivate(interactor: self)
    }
    private func onNextPhotos(_ value: [Photo]?) {
        let orderId = self.args.orderId
        self.orderDataModel
            .setPhotos(orderId: orderId, value)
            .subscribe(onError: { error in  print(error) })
            .disposeOnDeactivate(interactor: self)
    }
    private func onNextLocation(_ value: Location?) {
        let orderId = self.args.orderId
        self.orderDataModel
            .setLocation(orderId: orderId, value)
            .subscribe(onError: { error in print(error) })
            .disposeOnDeactivate(interactor: self)
    }
    private func onNextMinBudget(_ value: Double?) {
        let orderId = self.args.orderId
        self.orderDataModel
            .setMinBudget(orderId: orderId, value)
            .subscribe(onError: { error in print(error) })
            .disposeOnDeactivate(interactor: self)
    }
    private func onNextMaxBudget(_ value: Double?) {
        let orderId = self.args.orderId
        self.orderDataModel
            .setMaxBudget(orderId: orderId, value)
            .subscribe(onError: { error in print(error) })
            .disposeOnDeactivate(interactor: self)
    }
    private func onNextDisplayName(_ value: String?) {
        let orderId = self.args.orderId
        self.orderDataModel
            .setDisplayName(orderId: orderId, value)
            .subscribe(onError: { error in  print(error) })
            .disposeOnDeactivate(interactor: self)
    }
    private func onNextDisplayPhoneNumber(_ value: String?) {
        let orderId = self.args.orderId
        self.orderDataModel
            .setDisplayPhoneNumber(orderId: orderId, value)
            .subscribe(onError: { error in print(error) })
            .disposeOnDeactivate(interactor: self)
    }
    private func onNextDisplayContactsPermission(_ value: Bool?) {
        let orderId = self.args.orderId
        self.orderDataModel
            .setDisplayContactsPermission(orderId: orderId, value)
            .subscribe(onError: { error in  print(error) })
            .disposeOnDeactivate(interactor: self)
    }
    private func onNextStatus(_ value: OrderStatus?) {
        let orderId = self.args.orderId
        self.orderDataModel
            .setStatus(orderId: orderId, value)
            .subscribe(onError: { error in print(error) })
            .disposeOnDeactivate(interactor: self)
    }
    private func onNextDateCreation(_ value: Date?) {
        let orderId = self.args.orderId
        self.orderDataModel
            .setDateCreation(orderId: orderId, value)
            .subscribe(onError: { error in print(error) })
            .disposeOnDeactivate(interactor: self)
    }
    
    // MARK: - Helpers
    
    private var orderDataModel: OrderDataModel {
        args.dataModel.order
    }
    private var orderId: String {
        args.orderId
    }
    
    private func endEditing(withStatus status: OrderStatus) {
        args.streams.order.updateStatus(with: status)
        args.streams.order.updateCreationDate(with: Date())
    }
    
}
