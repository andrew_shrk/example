//
//  RIBOrderCreatorBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 26.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import FirebaseFirestore
import FirebaseAuth

protocol RIBOrderCreatorDependency: RIBOrderCreatorDependencyRIBOCBudget {
    var dataModel: DataModel { get }
    var storage: StorageModel { get }
    var auth: Auth { get }
}
protocol RIBOrderCreatorChildrenProtocol {
    var description: RIBOCDescriptionBuildable { get }
    var budget: RIBOCBudgetBuildable { get }
    var contacts: RIBOCContactsBuildable { get }
}

// MARK: Interactor args
protocol RIBOrderCreatorInteractorArgsProtocol {
    var auth: Auth { get }
    var streams: RIBOrderCreatorStreams & RIBOCDescriptionStreams { get }
    var dataModel: DataModel { get }
    var orderId: String { get }
    var storage: StorageModel { get }
}

protocol RIBOrderCreatorStreams {
    var budget: MutableOCBudgetStream { get }
    var contacts: MutableOCContactsStream { get }
    var order: MutableOCOrderStream { get }
    var owner: MutableOCOwnerStream { get }
    var validation: MutableOCValidationStream { get }
}

final class RIBOrderCreatorComponent: Component<RIBOrderCreatorDependency> {
    
    var dataModel: DataModel {
        dependency.dataModel
    }
    var storage: StorageModel {
        dependency.storage
    }
    var uiBuilder: UIBuilder {
        dependency.uiBuilder
    }
    var auth: Auth {
        dependency.auth
    }
    var streams: RIBOrderCreatorStreams & RIBOCDescriptionStreams {
        shared { RIBOrderCreatorStreamsImpl(specialization: OCSpecializationStreamImpl(),
                                            description: OCDescriptionStreamImpl(),
                                            photos: OCPhotosStreamImpl(),
                                            location: OCLocationStreamImpl(),
                                            budget: OCBudgetStreamImpl(),
                                            contacts: OCContactsStreamImpl(),
                                            order: OCOrderStreamImpl(),
                                            owner: OCOwnerStreamImpl(),
                                            validation: OCValidationStreamImpl()) }
    }
    var orderIdentifier: String {
        dataModel.order.create()
    }
}

// MARK: - Builder

protocol RIBOrderCreatorBuildable: Buildable {
    func build(withListener listener: RIBOrderCreatorListener) -> RIBOrderCreatorRouting
}

final class RIBOrderCreatorBuilder: Builder<RIBOrderCreatorDependency>, RIBOrderCreatorBuildable {

    override init(dependency: RIBOrderCreatorDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: RIBOrderCreatorListener) -> RIBOrderCreatorRouting {
        
        let component = RIBOrderCreatorComponent(dependency: dependency)
        let viewController = RIBOrderCreatorViewController()
        
        // Interactor
        let interactorArgs =
            RIBOrderCreatorInteractorArgs(auth: component.auth,
                                          streams: component.streams,
                                          dataModel: component.dataModel,
                                          orderId: component.orderIdentifier,
                                          storage: component.storage)
        let interactor = RIBOrderCreatorInteractor(presenter: viewController, args: interactorArgs)
        interactor.listener = listener
        
        // Router
        let children = RIBOrderCreatorChildren(
            description: RIBOCDescriptionBuilder(dependency: component),
            budget: RIBOCBudgetBuilder(dependency: component),
            contacts: RIBOCContactsBuilder(dependency: component))
        
        return RIBOrderCreatorRouter(interactor: interactor,
                                     viewController: viewController,
                                     childrenBuilder: children)
    }
}

struct RIBOrderCreatorChildren: RIBOrderCreatorChildrenProtocol {
    var description: RIBOCDescriptionBuildable
    var budget: RIBOCBudgetBuildable
    var contacts: RIBOCContactsBuildable
}
struct RIBOrderCreatorStreamsImpl: RIBOrderCreatorStreams & RIBOCDescriptionStreams {
    let specialization: OCMutableSpecializationStream
    let description: OCMutableDescriptionStream
    let photos: OCMutablePhotosStream
    let location: OCMutableLocationStream
    var budget: MutableOCBudgetStream
    var contacts: MutableOCContactsStream
    var order: MutableOCOrderStream
    var owner: MutableOCOwnerStream
    var validation: MutableOCValidationStream
}
struct RIBOrderCreatorInteractorArgs: RIBOrderCreatorInteractorArgsProtocol {
    var auth: Auth
    var streams: RIBOCDescriptionStreams & RIBOrderCreatorStreams
    var dataModel: DataModel
    var orderId: String
    var storage: StorageModel
}
