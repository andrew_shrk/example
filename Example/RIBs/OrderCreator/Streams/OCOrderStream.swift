//
//  OCOrderStream.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 04.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift
import RxCocoa

protocol OCOrderStream {
    var statusValue: Observable<OrderStatus> { get }
    var contactDisplayPermissionValue: Observable<Bool> { get }
    var dateCreationValue: Observable<Date?> { get }
}
protocol MutableOCOrderStream: OCOrderStream {
    func updateStatus(with newValue: OrderStatus)
    func updateContactDisplayPermission(displayAvailable newValue: Bool)
    func updateCreationDate(with newValue: Date?)
}
class OCOrderStreamImpl: MutableOCOrderStream {

    func updateStatus(with newValue: OrderStatus) {
        statusRelay.accept(newValue)
    }
    func updateContactDisplayPermission(displayAvailable newValue: Bool) {
        contactDisplayPermissionRelay.accept(newValue)
    }
    func updateCreationDate(with newValue: Date?) {
        creationDateRelay.accept(newValue)
    }
    
    var statusValue: Observable<OrderStatus> {
        statusRelay.asObservable().distinctUntilChanged()
    }
    var contactDisplayPermissionValue: Observable<Bool> {
        contactDisplayPermissionRelay.asObservable().distinctUntilChanged()
    }
    var dateCreationValue: Observable<Date?> {
        creationDateRelay.asObservable().distinctUntilChanged()
    }
    
    private let statusRelay = BehaviorRelay<OrderStatus>(value: .none)
    private let contactDisplayPermissionRelay = BehaviorRelay<Bool>(value: false)
    private let creationDateRelay = BehaviorRelay<Date?>(value: nil)
}
