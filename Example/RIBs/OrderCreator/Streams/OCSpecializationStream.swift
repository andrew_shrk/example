//
//  OCSpecializationStream.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 02.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift
import RxCocoa

protocol OCSpecializationStream {
    var value: Observable<Specialization?> { get }
    var specializationGroupValue: Observable<SpecializationGroup?> { get }
}
protocol OCMutableSpecializationStream: OCSpecializationStream {
    func update(with newSpecialization: Specialization?)
    func updateSpecializationGroup(with newValue: SpecializationGroup?)
}
class OCSpecializationStreamImpl: OCMutableSpecializationStream {

    var value: Observable<Specialization?> {
        relay.asObservable().distinctUntilChanged()
    }
    var specializationGroupValue: Observable<SpecializationGroup?> {
        specializationGroupRelay.asObservable().distinctUntilChanged()
    }
    
    func update(with newSpecialization: Specialization?) {
        relay.accept(newSpecialization)
    }
    func updateSpecializationGroup(with newValue: SpecializationGroup?) {
        specializationGroupRelay.accept(newValue)
    }
    
    private let relay = BehaviorRelay<Specialization?>(value: nil)
    private let specializationGroupRelay = BehaviorRelay<SpecializationGroup?>(value: nil)
}
