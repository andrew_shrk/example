//
//  OCBudgetSteam.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 04.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift
import RxCocoa

protocol OCBudgetStream {
    var minBudgetValue: Observable<Double?> { get }
    var maxBudgetValue: Observable<Double?> { get }
}
protocol MutableOCBudgetStream: OCBudgetStream {
    func updateMinBudget(with newValue: Double?)
    func updateMaxBudget(with newValue: Double?)
}
class OCBudgetStreamImpl: MutableOCBudgetStream {
    
    func updateMinBudget(with newValue: Double?) {
        minBudgetRelay.accept(newValue)
    }
    
    func updateMaxBudget(with newValue: Double?) {
        maxBudgetRelay.accept(newValue)
    }
    
    var minBudgetValue: Observable<Double?> {
        minBudgetRelay.asObservable().distinctUntilChanged()
    }
    
    var maxBudgetValue: Observable<Double?> {
        maxBudgetRelay.asObservable().distinctUntilChanged()
    }
    
    private let minBudgetRelay = BehaviorRelay<Double?>(value: nil)
    private let maxBudgetRelay = BehaviorRelay<Double?>(value: nil)
}
