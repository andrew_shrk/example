//
//  OCValidationStream.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 05.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift
import RxCocoa

protocol OCValidationStream {
    var value: Observable<Bool> { get }
}
protocol MutableOCValidationStream: OCValidationStream {
    func update(with newValue: Bool)
}
class OCValidationStreamImpl: MutableOCValidationStream {
    
    var value: Observable<Bool> {
        relay.asObservable()
    }
    
    func update(with newValue: Bool) {
        relay.accept(newValue)
    }
    
    private let relay = BehaviorRelay<Bool>(value: false)
}
