//
//  OCDescriptionStream.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 02.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift
import RxCocoa

protocol OCDescriptionStream {
    var value: Observable<String?> { get }
}
protocol OCMutableDescriptionStream: OCDescriptionStream {
    func update(with newDescription: String?)
}
class OCDescriptionStreamImpl: OCMutableDescriptionStream {
    
    var value: Observable<String?> {
        relay.asObservable().distinctUntilChanged()
    }
    
    func update(with newDescription: String?) {
        relay.accept(newDescription)
    }
    
    private let relay = BehaviorRelay<String?>(value: nil)
}
