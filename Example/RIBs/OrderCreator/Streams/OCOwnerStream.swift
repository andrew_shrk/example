//
//  OCOwnerStream.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 05.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift
import RxCocoa

protocol OCOwnerStream {
    var identifier: Observable<String?> { get }
}
protocol MutableOCOwnerStream: OCOwnerStream {
    func update(with newValue: String?)
}
class OCOwnerStreamImpl: MutableOCOwnerStream {
    
    var identifier: Observable<String?> {
        relay.asObservable().distinctUntilChanged()
    }
    
    func update(with newValue: String?) {
        relay.accept(newValue)
    }
    
    private let relay = BehaviorRelay<String?>(value: nil)
}
