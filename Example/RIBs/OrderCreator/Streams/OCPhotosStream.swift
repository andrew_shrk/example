//
//  OCPhotosStream.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 02.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift
import RxCocoa

protocol OCPhotosStream {
    var value: Observable<[Photo]> { get }
    var currentValue: [Photo] { get }
}
protocol OCMutablePhotosStream: OCPhotosStream {
    func update(with newValue: [Photo])
    func append(_ newImage: Photo)
    func remove(_ image: Photo)
    func remove(at index: Int)
}
class OCPhotosStreamImpl: OCMutablePhotosStream {
    
    var value: Observable<[Photo]> {
        relay.asObservable()
    }
    var currentValue: [Photo] {
        relay.value
    }
    
    func update(with newValue: [Photo]) {
        relay.accept(newValue)
    }
    func append(_ newImage: Photo) {
        var value = relay.value
        value.append(newImage)
        relay.accept(value)
    }
    func remove(_ image: Photo) {
        let oldValue = relay.value
        let newValue = oldValue.filter { $0 != image }
        relay.accept(newValue)
    }
    func remove(at index: Int) {
        var value = relay.value
        value.remove(at: index)
        relay.accept(value)
    }
    
    private let relay = BehaviorRelay<[Photo]>(value: [])
}
