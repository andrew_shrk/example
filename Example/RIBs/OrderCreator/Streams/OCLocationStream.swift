//
//  OCLocationStream.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 02.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift
import RxCocoa

protocol OCLocationStream {
    var value: Observable<Location?> { get }
}
protocol OCMutableLocationStream: OCLocationStream {
    func update(with newValue: Location?)
}
class OCLocationStreamImpl: OCMutableLocationStream {
    
    var value: Observable<Location?> {
        relay.asObservable()
    }
    
    func update(with newValue: Location?) {
        relay.accept(newValue)
    }
    
    private let relay = BehaviorRelay<Location?>(value: nil)
}
