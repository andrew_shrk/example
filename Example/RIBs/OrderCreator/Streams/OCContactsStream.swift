//
//  OCContactsStream.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 04.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift
import RxCocoa

protocol OCContactsStream {
    var displayNameValue: Observable<String?> { get }
    var phoneNumberValue: Observable<String?> { get }
}
protocol MutableOCContactsStream: OCContactsStream {
    func updateDisplayName(with newValue: String?)
    func updatePhoneNumber(with newValue: String?)
}
class OCContactsStreamImpl: MutableOCContactsStream {
    
    func updateDisplayName(with newValue: String?) {
        displayNameRelay.accept(newValue)
    }
    
    func updatePhoneNumber(with newValue: String?) {
        phoneNumberRelay.accept(newValue)
    }
    
    var displayNameValue: Observable<String?> {
        displayNameRelay.asObservable().distinctUntilChanged()
    }
    
    var phoneNumberValue: Observable<String?> {
        phoneNumberRelay.asObservable().distinctUntilChanged()
    }
    
    private let displayNameRelay = BehaviorRelay<String?>(value: nil)
    private let phoneNumberRelay = BehaviorRelay<String?>(value: nil)
}
