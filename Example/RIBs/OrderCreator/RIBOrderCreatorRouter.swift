//
//  RIBOrderCreatorRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 26.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

typealias RIBOrderCreatorInteractableType = Interactable
    & RIBOCDescriptionListener
    & RIBOCBudgetListener
    & RIBOCContactsListener

protocol RIBOrderCreatorInteractable: RIBOrderCreatorInteractableType {
    var router: RIBOrderCreatorRouting? { get set }
    var listener: RIBOrderCreatorListener? { get set }
}

protocol RIBOrderCreatorViewControllable: ViewControllable {
    func show(viewController: ViewControllable)
}

typealias RIBOrderCreatorRouterType =
    ViewableRouter<RIBOrderCreatorInteractable, RIBOrderCreatorViewControllable>
    & RIBOrderCreatorRouting

final class RIBOrderCreatorRouter: RIBOrderCreatorRouterType {

    init(interactor: RIBOrderCreatorInteractable,
         viewController: RIBOrderCreatorViewControllable,
         childrenBuilder: RIBOrderCreatorChildrenProtocol) {
        self.childrenBuilder = childrenBuilder
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
    
    // MARK: - Routing
    
    func presentDescription() {
        attachDescription()
    }
    func presentBudget() {
        attachBudget()
    }
    func presentContacts() {
        attachContacts()
    }
    
    // MARK: - Private
    
    private let childrenBuilder: RIBOrderCreatorChildrenProtocol
    
    // MARK: - Children
    
    // MARK: Attaching
    
    private func attachDescription() {
        let description = childrenBuilder.description.build(withListener: interactor)
        attachChild(description)
        viewController.show(viewController: description.viewControllable)
    }
    private func attachBudget() {
        let budget = childrenBuilder.budget.build(withListener: interactor)
        attachChild(budget)
        viewController.show(viewController: budget.viewControllable)
    }
    private func attachContacts() {
        let contacts = childrenBuilder.contacts.build(withListener: interactor)
        attachChild(contacts)
        viewController.show(viewController: contacts.viewControllable)
    }
    
}
