//
//  RIBOCBudgetViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 26.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import UIKit
import InputMask

protocol RIBOCBudgetPresentableListener: class {
    func presenterContinue()
    func presenterCancel()
    func didUpdateMinBudget(withStringValue value: String?)
    func didUpdateMaxBudget(withStringValue value: String?)
}

typealias RIBOCBudgetViewControllerType = UITableViewController
    & RIBOCBudgetPresentable
    & RIBOCBudgetViewControllable
    & OnMaskedTextChangedListener

final class RIBOCBudgetViewController: RIBOCBudgetViewControllerType {
    
    init(style: UITableView.Style, args: RIBOCBudgetViewControllerArgsProtocol) {
        self.args = args
        super.init(style: style)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    weak var listener: RIBOCBudgetPresentableListener?
    
    // MARK: UIViewController override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Бюджет"
        addRightBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(didTapCancelButton))
        minBudgetTextFieldCell.textField.delegate = priceInputMask
        maxBudgetTextFieldCell.textField.delegate = priceInputMask
        minBudgetTextFieldCell.textField.addTarget(self,
                                                   action: #selector(didEndBudgetEditing),
                                                   for: .editingDidEnd)
        maxBudgetTextFieldCell.textField.addTarget(self,
                                                   action: #selector(didEndBudgetEditing),
                                                   for: .editingDidEnd)
        priceInputMask.listener = self
        subscribe()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        minBudgetTextFieldCell.textField.resignFirstResponder()
        maxBudgetTextFieldCell.textField.resignFirstResponder()
    }
    
    // MARK: - OnMaskedTextChangedListener
    
    func textInput(_ textInput: UITextInput, didExtractValue: String, didFillMandatoryCharacters: Bool) {
        guard let sender = textInput as? UITextField else {
            return
        }
        switch sender {
        case minBudgetTextFieldCell.textField:
            tmpMinBudgetValue = didExtractValue
        case maxBudgetTextFieldCell.textField:
            tmpMaxBudgetValue = didExtractValue
        default:
            break
        }
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        return sections[section][row]
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            switch cell {
            case continueButtonCell: listener?.presenterContinue()
            default:
                break
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Private
    
    private let args: RIBOCBudgetViewControllerArgsProtocol
    
    private var tmpMinBudgetValue: String?
    private var tmpMaxBudgetValue: String?
    
    // MARK: - UI
    
    // MARK: Actions
    
    @objc func didEndBudgetEditing(_ sender: UITextField) {
        switch sender {
        case minBudgetTextFieldCell.textField:
            listener?.didUpdateMinBudget(withStringValue: tmpMinBudgetValue)
        case maxBudgetTextFieldCell.textField:
            listener?.didUpdateMaxBudget(withStringValue: tmpMaxBudgetValue)
        default:
            break
        }
    }
    @objc func didTapCancelButton(_ sender: UIBarButtonItem) {
        listener?.presenterCancel()
    }
    
    // MARK: Cells
    
    private lazy var minBudgetTextFieldCell: UITextFieldTableViewCell = {
        return buildBudgetTextField(title: "От", placeholder: "Минимальный бюджет")
    }()
    private lazy var maxBudgetTextFieldCell: UITextFieldTableViewCell = {
        return buildBudgetTextField(title: "До", placeholder: "Максимальный бюджет")
    }()
    private lazy var continueButtonCell = {
        return args.uiBuilder.tableViewCellBuilder.buildFilledButtonCell(withTitle: "Продолжить")
    }()
    private lazy var priceInputMask: MaskedTextInputListener = {
        let priceInputMask = MaskedTextInputListener()
        priceInputMask.primaryMaskFormat = "[000] [000] [000] руб."
        priceInputMask.rightToLeft = true
        return priceInputMask
    }()
    
    // MARK: Sections
    
    // Budget section
    private lazy var _budgetSection = [
        minBudgetTextFieldCell,
        maxBudgetTextFieldCell
    ]
    private var budgetSection: [UITableViewCell] {
        _budgetSection.filter { !$0.isHidden }
    }
    
    // Control section
    private lazy var _controlSection = [
        continueButtonCell
    ]
    private var controlSection: [UITableViewCell] {
        _controlSection.filter { !$0.isHidden }
    }
    
    // Sections
    private lazy var _sections = [
        budgetSection,
        controlSection
    ]
    private var sections: [[UITableViewCell]] {
        _sections.filter { $0.count != 0 }
    }
    
    // MARK: - RX
    
    private let disposeBag = DisposeBag()
    
    // MARK: Subscriptions
    
    private func subscribe() {
        bindMinBudget()
        bindMaxBudget()
    }
    private func bindMinBudget() {
        let textField = minBudgetTextFieldCell.textField
        args.budgetStream
            .minBudgetValue
            .bind(onNext: { [unowned textField, unowned self] (value) in
                if let stringValue = self.doubleToString(value) {
                    self.priceInputMask.put(text: stringValue, into: textField)
                } else {
                    textField.text = nil
                }
            })
            .disposed(by: disposeBag)
    }
    private func bindMaxBudget() {
        let textField = maxBudgetTextFieldCell.textField
        args.budgetStream
            .maxBudgetValue
            .bind(onNext: { [unowned textField, unowned self] (value) in
                if let stringValue = self.doubleToString(value) {
                    self.priceInputMask.put(text: stringValue, into: textField)
                } else {
                    textField.text = nil
                }
            })
            .disposed(by: disposeBag)
    }
    
    // MARK: - Helpers
    
    private func buildBudgetTextField(title: String, placeholder: String) -> UITextFieldTableViewCell {
        let budgetTextFieldCell = args.uiBuilder.tableViewCellBuilder.buildTextFieldCell()
        budgetTextFieldCell.titleLabel.text = title
        budgetTextFieldCell.textField.placeholder = placeholder
        budgetTextFieldCell.textField.keyboardType = .numberPad
        budgetTextFieldCell.selectionStyle = .none
        return budgetTextFieldCell
    }
    
    private func doubleToString(_ value: Double?) -> String? {
        if let value = value {
            return String(format: "%.0f", value)
        } else {
            return nil
        }
    }

}
