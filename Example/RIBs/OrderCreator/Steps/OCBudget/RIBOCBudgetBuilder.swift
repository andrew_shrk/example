//
//  RIBOCBudgetBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 26.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RIBOCBudgetDependency: Dependency {
    var uiBuilder: UIBuilder { get }
    var mutableBudgetStream: MutableOCBudgetStream { get }
}

// MARK: ViewController args
protocol RIBOCBudgetViewControllerArgsProtocol {
    var uiBuilder: UIBuilder { get }
    var budgetStream: OCBudgetStream { get }
}
// MARK: Interactor args
protocol RIBOCBudgetInteractorArgsProtocol {
    var mutableBudgetStream: MutableOCBudgetStream { get }
}

final class RIBOCBudgetComponent: Component<RIBOCBudgetDependency> {
    fileprivate var uiBuilder: UIBuilder {
        dependency.uiBuilder
    }
    fileprivate var mutableBudgetStream: MutableOCBudgetStream {
        dependency.mutableBudgetStream
    }
}

// MARK: - Builder

protocol RIBOCBudgetBuildable: Buildable {
    func build(withListener listener: RIBOCBudgetListener) -> RIBOCBudgetRouting
}

final class RIBOCBudgetBuilder: Builder<RIBOCBudgetDependency>, RIBOCBudgetBuildable {

    override init(dependency: RIBOCBudgetDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: RIBOCBudgetListener) -> RIBOCBudgetRouting {
        let component = RIBOCBudgetComponent(dependency: dependency)
        
        // View controller
        let viewControllerArgs = RIBOCBudgetViewControllerArgs(uiBuilder: component.uiBuilder,
                                                               budgetStream: component.mutableBudgetStream)
        let viewController = RIBOCBudgetViewController(style: .insetGroupedIfAvailable, args: viewControllerArgs)
        
        // Interactor
        let interactorArgs = RIBOCBudgetInteractorArgs(mutableBudgetStream: component.mutableBudgetStream)
        let interactor = RIBOCBudgetInteractor(presenter: viewController, args: interactorArgs)
        
        interactor.listener = listener
        return RIBOCBudgetRouter(interactor: interactor, viewController: viewController)
    }
}

struct RIBOCBudgetViewControllerArgs: RIBOCBudgetViewControllerArgsProtocol {
    var uiBuilder: UIBuilder
    var budgetStream: OCBudgetStream
}
struct RIBOCBudgetInteractorArgs: RIBOCBudgetInteractorArgsProtocol {
    var mutableBudgetStream: MutableOCBudgetStream
}
