//
//  RIBOCBudgetRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 26.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RIBOCBudgetInteractable: Interactable {
    var router: RIBOCBudgetRouting? { get set }
    var listener: RIBOCBudgetListener? { get set }
}

protocol RIBOCBudgetViewControllable: ViewControllable {
}

typealias RIBOCBudgetRouterType = ViewableRouter<RIBOCBudgetInteractable, RIBOCBudgetViewControllable>
    & RIBOCBudgetRouting

final class RIBOCBudgetRouter: RIBOCBudgetRouterType {

    override init(interactor: RIBOCBudgetInteractable, viewController: RIBOCBudgetViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
