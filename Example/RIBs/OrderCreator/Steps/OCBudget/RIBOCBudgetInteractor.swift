//
//  RIBOCBudgetInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 26.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift

protocol RIBOCBudgetRouting: ViewableRouting {}

protocol RIBOCBudgetPresentable: Presentable {
    var listener: RIBOCBudgetPresentableListener? { get set }
}

protocol RIBOCBudgetListener: class {
    func ribOCBudgetContinue()
    func cancel()
}

typealias RIBOCBudgetInteractorType = PresentableInteractor<RIBOCBudgetPresentable>
    & RIBOCBudgetInteractable
    & RIBOCBudgetPresentableListener

final class RIBOCBudgetInteractor: RIBOCBudgetInteractorType {

    weak var router: RIBOCBudgetRouting?
    weak var listener: RIBOCBudgetListener?

    init(presenter: RIBOCBudgetPresentable, args: RIBOCBudgetInteractorArgsProtocol) {
        self.args = args
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - Presenterlistener
    
    func presenterContinue() {
        listener?.ribOCBudgetContinue()
    }
    func presenterCancel() {
        listener?.cancel()
    }
    func didUpdateMaxBudget(withStringValue value: String?) {
        let doubleValue = parseDouble(from: value)
        args.mutableBudgetStream.updateMaxBudget(with: doubleValue)
    }
    func didUpdateMinBudget(withStringValue value: String?) {
        let doubleValue = parseDouble(from: value)
        args.mutableBudgetStream.updateMinBudget(with: doubleValue)
    }
    
    // MARK: - Private
    
    private let args: RIBOCBudgetInteractorArgsProtocol
    
    // MARK: - Helpers
    
    private func parseDouble(from string: String?) -> Double? {
        if let stringValue = string {
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            let doubleValue = formatter.number(from: stringValue)?.doubleValue
            return doubleValue
        } else {
            return nil
        }
    }
    
}
