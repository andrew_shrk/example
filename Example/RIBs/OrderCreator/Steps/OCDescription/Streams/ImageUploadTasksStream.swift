//
//  ImageUploadTasksStream.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 04.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift
import RxCocoa

protocol ImageUploadTasksStream {
    var value: Observable<[ImageUploadTask]> { get }
}
protocol MutableImageUploadTasksStream: ImageUploadTasksStream {
    func update(with newValue: [ImageUploadTask])
    func remove(_ item: ImageUploadTask)
    func remove(at index: Int)
    func element(at index: Int) -> ImageUploadTask
    func append(_ item: ImageUploadTask)
}
class ImageUploadTasksStreamImpl: MutableImageUploadTasksStream {
    
    var value: Observable<[ImageUploadTask]> {
        relay.asObservable()
    }
    
    func update(with newValue: [ImageUploadTask]) {
        relay.accept(newValue)
    }
    func remove(_ item: ImageUploadTask) {
        let oldValue = relay.value
        let newValue = oldValue.filter { $0 !== item }
        relay.accept(newValue)
    }
    func remove(at index: Int) {
        var value = relay.value
        value.remove(at: index)
        relay.accept(value)
    }
    func element(at index: Int) -> ImageUploadTask {
        return relay.value[index]
    }
    func append(_ item: ImageUploadTask) {
        var value = relay.value
        value.append(item)
        relay.accept(value)
    }
    
    private let relay = BehaviorRelay<[ImageUploadTask]>(value: [])
}
