//
//  RIBOCDescriptionViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 26.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import UIKit
import UITextView_Placeholder

protocol RIBOCDescriptionPresentableListener: class {
    func presenterDidSelectSpecialization()
    func presenterDidSelectDescription()
    func presenterDidSelectAddPhoto()
    func presenterDidSelectAddLocation()
    func presenterDidSelectRemoveLocation()
    func presenterDidSelectLocation()
    func removeImage(with index: Int)
    func presenterClose()
    func presenterContinue()
}

typealias RIBOCDescriptionVC = UITableViewController
    & RIBOCDescriptionPresentable
    & RIBOCDescriptionViewControllable

final class RIBOCDescriptionViewController: RIBOCDescriptionVC {
    
    weak var listener: RIBOCDescriptionPresentableListener?
    
    // MARK: Inits
    
    init(style: UITableView.Style, args: RIBOCDescriptionVCArgsProtocol) {
        self.args = args
        super.init(style: style)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: UIViewController override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Описание заказа"
        addRightBarButtonItem(barButtonSystemItem: .cancel,
                              target: self,
                              action: #selector(didTapCloseButton))
        bind()
    }
    
    // MARK: - Presentable
    
    var isImageViewerHidden: Bool = false {
        didSet {
            imagesViewCell.isHidden = isImageViewerHidden
        }
    }
    var isLocationViewerHidden: Bool = false {
        didSet {
            locationViewCell.isHidden = isLocationViewerHidden
        }
    }
    var isLocationAddButtonHidden: Bool = false {
        didSet {
            locationEditButtonCell.isHidden = isLocationAddButtonHidden
        }
    }
    var isLocationRemoveButtonHidden: Bool = false {
        didSet {
            locationRemoveButtonCell.isHidden = isLocationRemoveButtonHidden
        }
    }
    var isOrderDescriptionHidden: Bool = false {
        didSet {
            descriptionCell.isHidden = isOrderDescriptionHidden
            descriptionPlaceholderCell.isHidden = !isOrderDescriptionHidden
        }
    }
    
    func setSpecialization(name: String?) {
        specializationNameCell.textLabel?.text = name
        if #available(iOS 13.0, *) {
            specializationNameCell.textLabel?.textColor = .label
        } else {
            specializationNameCell.textLabel?.textColor = .black
        }
    }
    func setSpecialization(placeholder: String?) {
        specializationNameCell.textLabel?.text = placeholder
        if #available(iOS 13.0, *) {
            specializationNameCell.textLabel?.textColor = .placeholderText
        } else {
            specializationNameCell.textLabel?.textColor = .lightGray
        }
    }
    func setDescription(name: String?) {
        tableView.beginUpdates()
        descriptionCell.textLabel?.text = name
        if #available(iOS 13.0, *) {
            descriptionCell.textLabel?.textColor = .label
        } else {
            descriptionCell.textLabel?.textColor = .black
        }
        tableView.endUpdates()
    }
    func setDescription(placeholder: String?) {
        tableView.beginUpdates()
        descriptionPlaceholderCell.textLabel?.text = placeholder
        tableView.endUpdates()
    }
    
    func setFormValidation(isValid: Bool) {
        continueButtonCell.isUserInteractionEnabled = isValid
        continueButtonCell.backgroundColor = isValid ? UIColor.systemBlue : UIColor.systemBlue.withAlphaComponent(0.3)  
    }
    
    func reloadData() {
        tableView.reloadData()
    }
    func reloadImageCollectionView() {
        imagesViewCell.collectionView.reloadData()
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let selectedCell = tableView.cellForRow(at: indexPath) {
            switch selectedCell {
            case specializationNameCell: listener?.presenterDidSelectSpecialization()
            case descriptionCell, descriptionPlaceholderCell: listener?.presenterDidSelectDescription()
            case addPhotoCell: listener?.presenterDidSelectAddPhoto()
            case locationEditButtonCell: listener?.presenterDidSelectAddLocation()
            case locationRemoveButtonCell: listener?.presenterDidSelectRemoveLocation()
            case locationViewCell: listener?.presenterDidSelectLocation()
            case continueButtonCell: listener?.presenterContinue()
            default:
                break
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        return sections[section][row]
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: return "Специализация"
        case 1: return "Описание работ"
        default: return nil
        }
    }
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch section {
        case 0, 1:
            return "Обязательное поле"
        default: return nil
        }
    }
    
    // MARK: - Private
    
    private let args: RIBOCDescriptionVCArgsProtocol
    
    private let imageCellReusableIdentifier = "image_cell"
    
    private let disposeBag = DisposeBag()
    
    // MARK: RX
    
    private func bind() {
        bindDescription()
        bindLocation()
        bindSpecialization()
        bindPhotos()
    }
    
    private func bindDescription() {
        args.descriptionStream
            .value
            .bind { [unowned self] (description) in
                self.descriptionCell.textLabel?.text = description
                self.isOrderDescriptionHidden = description?.count == 0 || description == nil
                self.tableView.reloadData()
            }
            .disposed(by: disposeBag)
    }
    private func bindLocation() {
        args.locationStream.value.bind { [unowned self] (location) in
            guard let location = location else { return }
            self.locationViewCell.setCoordinates(location.coordinates.clGeoPoint())
            self.locationViewCell.setLocationName(location.name)
        }
        .disposed(by: disposeBag)
    }
    private func bindSpecialization() {
        args.specializationStream.value.bind { [unowned self] (specialization) in
            guard let specialization = specialization else { return }
            self.specializationNameCell.textLabel?.text = specialization.name
        }
        .disposed(by: disposeBag)
    }
    private func bindPhotos() {
        let collectionViewRx = imagesViewCell
            .collectionView.rx
        args.imageUploadTasksStream
            .value
            .bind(to: collectionViewRx
                .items(cellIdentifier: "image_cell",
                       cellType: UIImageCollectionViewCell.self)) { [unowned self] index, task, cell in
                        if task.isCompleted {
                            cell.stopActivityAnimating()
                        } else {
                            cell.startActivityAnimating()
                        }
                        cell.imageView.image = task.image
                        cell.removeButton.tag = index
                        cell.removeButton.addTarget(self,
                                                    action: #selector(self.didTapImageRemove),
                                                    for: .touchUpInside)
            }
            .disposed(by: disposeBag)
        args.imageUploadTasksStream
            .value
            .subscribe(onNext: { [unowned self] value in
                self.imagesViewCell.isHidden = value.count == 0
                self.tableView.reloadData()
            })
            .disposed(by: disposeBag)
    }
    
    // MARK: UI
    
    // MARK: Actions
    
    @objc func didTapImageRemove(_ sender: UIButton) {
        listener?.removeImage(with: sender.tag)
    }
    @objc func didTapCloseButton() {
        listener?.presenterClose()
    }
    
    // MARK: Views
    
    private lazy var specializationNameCell: UITableViewCell = {
        let sectionNameCell = UITableViewCell()
        sectionNameCell.accessoryType = .disclosureIndicator
        return sectionNameCell
    }()
    private lazy var descriptionCell: UITableViewCell = {
        let descriptionCell = UITableViewCell()
        descriptionCell.accessoryType = .none
        descriptionCell.textLabel?.numberOfLines = 0
        return descriptionCell
    }()
    private lazy var descriptionPlaceholderCell: UITableViewCell = {
        let descriptionPlaceholderCell = UITableViewCell()
        descriptionPlaceholderCell.accessoryType = .none
        descriptionPlaceholderCell.textLabel?.numberOfLines = 0
        if #available(iOS 13.0, *) {
            descriptionPlaceholderCell.textLabel?.textColor = .placeholderText
        } else {
            descriptionPlaceholderCell.textLabel?.textColor = .lightGray
        }
        return descriptionPlaceholderCell
    }()
    private lazy var continueButtonCell: UITableViewCell = {
        let continueButtonCell = UITableViewCell()
        continueButtonCell.backgroundColor = .systemBlue
        continueButtonCell.textLabel?.textAlignment = .center
        continueButtonCell.textLabel?.textColor = .white
        continueButtonCell.selectionStyle = .blue
        continueButtonCell.textLabel?.text = "Продолжить"
        return continueButtonCell
    }()
    private lazy var imagesViewCell: UIImagesTableViewCell = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = .init(width: 128, height: 128)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 10
        let imagesViewCell = UIImagesTableViewCell(layout: layout)
        imagesViewCell.collectionView.register(UIImageCollectionViewCell.self,
                                               forCellWithReuseIdentifier: imageCellReusableIdentifier)
        imagesViewCell.collectionView.backgroundColor = .clear      
        imagesViewCell.collectionView.snp.makeConstraints { (maker) in
            maker.height.greaterThanOrEqualTo(128.5)
        }
        return imagesViewCell
    }()
    private lazy var locationEditButtonCell: UITableViewCell = {
        let locationEditButtonCell = buildButtonWithAccessoryCell(imageName: "icon_my_location_24dp",
                                                                  title: "Указать местоположение",
                                                                  tintColor: .systemBlue)
        return locationEditButtonCell
    }()
    private lazy var addPhotoCell: UITableViewCell = {
        let addPhotoCell = buildButtonWithAccessoryCell(imageName: "icon_add_a_photo_24dp",
                                                        title: "Добавить фотографию",
                                                        tintColor: .systemBlue)
        return addPhotoCell
    }()
    private lazy var locationRemoveButtonCell: UITableViewCell = {
        let locationRemoveButtonCell = buildButtonWithAccessoryCell(imageName: "icon_location_disabled_24dp",
                                                                    title: "Сбросить местоположение",
                                                                    tintColor: .systemRed)
        return locationRemoveButtonCell
    }()
    private lazy var locationViewCell: UIMapTableViewCell = {
        let locationViewCell = UIMapTableViewCell()
        locationViewCell.isHidden = false
        return locationViewCell
    }()
    
    private func buildButtonWithAccessoryCell(imageName: String, title: String, tintColor: UIColor) -> UITableViewCell {
        let accessoryImage = UIImage(named: imageName)
        let accessoryView = UIImageView(image: accessoryImage, highlightedImage: nil)
        accessoryView.tintColor = tintColor
        let buttonWithAccessoryCell = UITableViewCell()
        buttonWithAccessoryCell.textLabel?.text = title
        buttonWithAccessoryCell.textLabel?.textColor = tintColor
        buttonWithAccessoryCell.accessoryView = accessoryView
        return buttonWithAccessoryCell
    }
    
    // MARK: - Table structure
    
    private lazy var _specializationSection = [
        specializationNameCell
    ]
    private lazy var _descriptionSection = [
        descriptionCell,
        descriptionPlaceholderCell
    ]
    private lazy var _photoSection = [
        imagesViewCell,
        addPhotoCell
    ]
    private lazy var _locationSection = [
        locationViewCell,
        locationRemoveButtonCell,
        locationEditButtonCell
    ]
    private lazy var _locationControlSection: [UITableViewCell] = [
        
    ]
    private lazy var _controlSection = [
        continueButtonCell
    ]
    
    private lazy var specializationSection = _specializationSection
    private var descriptionSection: [UITableViewCell] {
        _descriptionSection.filter { !$0.isHidden }
    }
    private var photoSection: [UITableViewCell] {
        _photoSection.filter { !$0.isHidden }
    }
    private var locationSection: [UITableViewCell] {
        _locationSection.filter { !$0.isHidden }
    }
    private var locationControlSection: [UITableViewCell] {
        _locationControlSection.filter { !$0.isHidden }
    }
    private lazy var controlSection = _controlSection
    
    private var _sections: [[UITableViewCell]] {
        [
            specializationSection,
            descriptionSection,
            photoSection,
            locationSection,
            locationControlSection,
            controlSection
        ]
    }
    private var sections: [[UITableViewCell]] {
        _sections.filter { $0.count != 0 }
    }
    
}
