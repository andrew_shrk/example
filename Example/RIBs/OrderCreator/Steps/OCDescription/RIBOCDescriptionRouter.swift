//
//  RIBOCDescriptionRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 26.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

typealias RIBOCDescriptionInteractableListener = RIBCategorySelectorListener
    & RIBTextEditorListener
    & ImagePickerListener
    & LocationEditorListener

protocol RIBOCDescriptionInteractable: Interactable, RIBOCDescriptionInteractableListener {
    var router: RIBOCDescriptionRouting? { get set }
    var listener: RIBOCDescriptionListener? { get set }
}

protocol RIBOCDescriptionViewControllable: ViewControllable {
    func presentAsSheet(viewController: ViewControllable)
    func dismiss(viewController: ViewControllable)
}

typealias RIBOCDescriptionRouterType =
    ViewableRouter<RIBOCDescriptionInteractable, RIBOCDescriptionViewControllable>
    & RIBOCDescriptionRouting

final class RIBOCDescriptionRouter: RIBOCDescriptionRouterType {

    init(interactor: RIBOCDescriptionInteractable,
         viewController: RIBOCDescriptionViewControllable,
         childrenBuildable: RIBOCDescriptionChildrenProtocol) {
        self.childrenBuilder = childrenBuildable
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
    
    // MARK: - Routing
    
    func presentCategorySelector() {
        attachCategorySelector()
    }
    func presentTextEditor(withPlaceholder placeholder: String?, text: String?) {
        attachTextEditor(withPlaceholder: placeholder, text: text)
    }
    func presentImagePicker() {
        attachImagePicker()
    }
    func presentLocationEditor(withDefaultLocation defaultLocation: Location?) {
        attachLocationEditor(withDefaultLocation: defaultLocation)
    }
    func dismissCurrentChild() {
        detachCurrentChild()
    }
    
    // MARK: - Private
    
    private let childrenBuilder: RIBOCDescriptionChildrenProtocol
    
    private var currentChild: ViewableRouting?
    private var currentChildPresenter: ViewControllable?
    
    // MARK: - Children
    
    // MARK: Attach
    
    private func attachLocationEditor(withDefaultLocation defaultLocation: Location?) {
        let locationEditor = childrenBuilder.locationEditor.build(withListener: interactor,
                                                                  defaultLocation: defaultLocation)
        currentChild = locationEditor
        currentChildPresenter = locationEditor.viewControllable
        attachChild(locationEditor)
        viewController.presentAsSheet(viewController: locationEditor.viewControllable)
    }
    private func attachImagePicker() {
        let imagePicker = childrenBuilder.imagePicker.build(withListener: interactor)
        currentChild = imagePicker
        currentChildPresenter = imagePicker.viewControllable
        attachChild(imagePicker)
        viewController.presentAsSheet(viewController: imagePicker.viewControllable)
    }
    private func attachCategorySelector() {
        let categorySelector = childrenBuilder.categorySelector.build(withListener: interactor)
        let navController = UINavigationController(rootViewController: categorySelector.viewControllable)
        currentChild = categorySelector
        currentChildPresenter = navController
        attachChild(categorySelector)
        viewController.presentAsSheet(viewController: navController)
    }
    private func attachTextEditor(withPlaceholder placeholder: String?, text: String?) {
        let args = RIBTextEditorArgs(text: text, placeholder: placeholder, title: "Описание")
        let textEditor = childrenBuilder.textEditor.build(withListener: interactor, args: args)
        let navController = UINavigationController(rootViewController: textEditor.viewControllable)
        currentChild = textEditor
        currentChildPresenter = navController
        attachChild(textEditor)
        viewController.presentAsSheet(viewController: navController)
    }
    
    // MARK: Detach
    
    private func detachCurrentChild() {
        if let currentChild = currentChild {
            detachChild(currentChild)
        }
        if let currentChildPresenter = currentChildPresenter {
            viewController.dismiss(viewController: currentChildPresenter)
        }
    }
    
}
