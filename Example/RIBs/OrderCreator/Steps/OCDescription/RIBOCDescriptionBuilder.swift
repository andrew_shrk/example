//
//  RIBOCDescriptionBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 26.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import FirebaseFirestore

protocol RIBOCDescriptionDependency: Dependency {
    var dataModel: DataModel { get }
    var storage: StorageModel { get }
    var streams: RIBOCDescriptionStreams & RIBOrderCreatorStreams { get }
    var orderIdentifier: String { get }
    var validationStream: OCValidationStream { get }
}

// MARK: - Children
protocol RIBOCDescriptionChildrenProtocol {
    var categorySelector: RIBCategorySelectorBuildable { get }
    var textEditor: RIBTextEditorBuildable { get }
    var imagePicker: ImagePickerBuildable { get }
    var locationEditor: LocationEditorBuildable { get }
}

// MARK: ViewController args
protocol RIBOCDescriptionVCArgsProtocol {
    var descriptionStream: OCDescriptionStream { get }
    var specializationStream: OCSpecializationStream { get }
    var locationStream: OCLocationStream { get }
    var photosStream: OCPhotosStream { get }
    var imageUploadTasksStream: ImageUploadTasksStream { get }
}

// MARK: Interactor args
protocol RIBOCDescriptionInteractorArgsProtocol {
    var mutableDescriptionStream: OCMutableDescriptionStream { get }
    var mutableLocationStream: OCMutableLocationStream { get }
    var mutableSpecializationStream: OCMutableSpecializationStream { get }
    var mutablePhotosStream: OCMutablePhotosStream { get }
    var mutableImageUploadTasksStream: MutableImageUploadTasksStream { get }
    var storage: StorageModel { get }
    var orderIdentifier: String { get }
    var validationStream: OCValidationStream { get }
}

// MARK: Streams
protocol RIBOCDescriptionStreams {
    var specialization: OCMutableSpecializationStream { get }
    var description: OCMutableDescriptionStream { get }
    var photos: OCMutablePhotosStream { get }
    var location: OCMutableLocationStream { get }
}

final class RIBOCDescriptionComponent: Component<RIBOCDescriptionDependency> {
    
    var dataModel: DataModel {
        dependency.dataModel
    }
    var storage: StorageModel {
        dependency.storage
    }
    var streams: RIBOCDescriptionStreams {
        dependency.streams
    }
    fileprivate lazy var mutableImageUploadTasksStream: MutableImageUploadTasksStream = {
        shared { ImageUploadTasksStreamImpl() }
    }()
    fileprivate var orderIdentifier: String {
        dependency.orderIdentifier
    }
    fileprivate var validationStream: OCValidationStream {
        dependency.validationStream
    }
    
}

// MARK: - Builder

protocol RIBOCDescriptionBuildable: Buildable {
    func build(withListener listener: RIBOCDescriptionListener) -> RIBOCDescriptionRouting
}

final class RIBOCDescriptionBuilder: Builder<RIBOCDescriptionDependency>, RIBOCDescriptionBuildable {

    override init(dependency: RIBOCDescriptionDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: RIBOCDescriptionListener) -> RIBOCDescriptionRouting {
        let component = RIBOCDescriptionComponent(dependency: dependency)
        
        // View controller
        let viewControllerArgs = RIBOCDescriptionVCArgs(imageUploadTasksStream: component.mutableImageUploadTasksStream,
                                                        descriptionStream: component.streams.description,
                                                        specializationStream: component.streams.specialization,
                                                        locationStream: component.streams.location,
                                                        photosStream: component.streams.photos)
        
        let viewController = RIBOCDescriptionViewController(style: .insetGroupedIfAvailable,
                                                            args: viewControllerArgs)
        
        // Interactor
        let interactorArgs =
            RIBOCDescriptionInteractorArgs(mutableLocationStream: component.streams.location,
                                           mutableSpecializationStream: component.streams.specialization,
                                           mutablePhotosStream: component.streams.photos,
                                           mutableDescriptionStream: component.streams.description,
                                           mutableImageUploadTasksStream: component.mutableImageUploadTasksStream,
                                           storage: component.storage,
                                           orderIdentifier: component.orderIdentifier,
                                           validationStream: component.validationStream)
        
        let interactor = RIBOCDescriptionInteractor(presenter: viewController, args: interactorArgs)
        interactor.listener = listener
        
        // Router
        let children =
            RIBOCDescriptionChildren(locationEditor: LocationEditorBuilder(dependency: component),
                                     categorySelector: RIBCategorySelectorBuilder(dependency: component),
                                     textEditor: RIBTextEditorBuilder(dependency: component),
                                     imagePicker: ImagePickerBuilder(dependency: component))
        
        return RIBOCDescriptionRouter(interactor: interactor,
                                      viewController: viewController,
                                      childrenBuildable: children)
    }
}

struct RIBOCDescriptionChildren: RIBOCDescriptionChildrenProtocol {
    var locationEditor: LocationEditorBuildable
    var categorySelector: RIBCategorySelectorBuildable
    var textEditor: RIBTextEditorBuildable
    var imagePicker: ImagePickerBuildable
}
struct RIBOCDescriptionVCArgs: RIBOCDescriptionVCArgsProtocol {
    var imageUploadTasksStream: ImageUploadTasksStream
    var descriptionStream: OCDescriptionStream
    var specializationStream: OCSpecializationStream
    var locationStream: OCLocationStream
    var photosStream: OCPhotosStream
}
struct RIBOCDescriptionInteractorArgs: RIBOCDescriptionInteractorArgsProtocol {
    var mutableLocationStream: OCMutableLocationStream
    var mutableSpecializationStream: OCMutableSpecializationStream
    var mutablePhotosStream: OCMutablePhotosStream
    var mutableDescriptionStream: OCMutableDescriptionStream
    var mutableImageUploadTasksStream: MutableImageUploadTasksStream
    var storage: StorageModel
    var orderIdentifier: String
    var validationStream: OCValidationStream
}
