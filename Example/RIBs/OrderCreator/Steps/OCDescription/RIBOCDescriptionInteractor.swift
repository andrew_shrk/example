//
//  RIBOCDescriptionInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 26.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift

protocol RIBOCDescriptionRouting: ViewableRouting {
    func presentCategorySelector()
    func presentTextEditor(withPlaceholder placeholder: String?, text: String?)
    func presentImagePicker()
    func presentLocationEditor(withDefaultLocation defaultLocation: Location?)
    func dismissCurrentChild()
}

protocol RIBOCDescriptionPresentable: Presentable {
    var listener: RIBOCDescriptionPresentableListener? { get set }
    
    var isImageViewerHidden: Bool { get set }
    var isLocationViewerHidden: Bool { get set }
    var isLocationRemoveButtonHidden: Bool { get set }
    var isLocationAddButtonHidden: Bool { get set }
    
    func setSpecialization(placeholder: String?)
    func setSpecialization(name: String?)
    func setDescription(placeholder: String?)
    func setDescription(name: String?)
    
    func setFormValidation(isValid: Bool)
    
    func reloadImageCollectionView()
    
    func reloadData()
}

protocol RIBOCDescriptionListener: class {
    func cancel()
    func ribOCDescriptionContinue()
}

typealias RIBOCDescriptionInteractorType = PresentableInteractor<RIBOCDescriptionPresentable>
    & RIBOCDescriptionInteractable
    & RIBOCDescriptionPresentableListener

final class RIBOCDescriptionInteractor: RIBOCDescriptionInteractorType {
    
    weak var router: RIBOCDescriptionRouting?
    weak var listener: RIBOCDescriptionListener?

    init(presenter: RIBOCDescriptionPresentable, args: RIBOCDescriptionInteractorArgsProtocol) {
        self.args = args
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        presenter.setSpecialization(placeholder: "Выберите специализацию")
        presenter.setDescription(placeholder: descriptionPlaceholder)
        presenter.isImageViewerHidden = true
        presenter.isLocationViewerHidden = true
        presenter.isLocationAddButtonHidden = false
        presenter.isLocationRemoveButtonHidden = true
        presenter.reloadData()
        subscribe()
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - PresentableListener
    
    func presenterDidSelectSpecialization() {
        router?.presentCategorySelector()
    }
    func presenterDidSelectDescription() {
        args.mutableDescriptionStream
            .value
            .subscribe(onNext: { [unowned self] (value) in
                self.router?.presentTextEditor(withPlaceholder: self.descriptionPlaceholder, text: value)
            })
            .dispose()
    }
    func presenterDidSelectAddPhoto() {
        router?.presentImagePicker()
    }
    func presenterDidSelectAddLocation() {
        router?.presentLocationEditor(withDefaultLocation: nil)
    }
    func presenterDidSelectRemoveLocation() {
        args.mutableLocationStream.update(with: nil)
        presenter.isLocationAddButtonHidden = false
        presenter.isLocationViewerHidden = true
        presenter.isLocationRemoveButtonHidden = true
        presenter.reloadData()
    }
    func presenterDidSelectLocation() {
        args.mutableLocationStream
            .value
            .subscribe(onNext: { [unowned self] (value) in
                self.router?.presentLocationEditor(withDefaultLocation: value)
            })
            .dispose()
    }
    func removeImage(with index: Int) {
        let imageUploadTask = args.mutableImageUploadTasksStream.element(at: index)
        let storageReference = imageUploadTask.storageReference
        if !imageUploadTask.isCompleted {
            imageUploadTask.cancel()
            args.mutableImageUploadTasksStream.remove(at: index)
        } else {
            args.storage
                .delete(reference: storageReference)
                .single()
                .subscribe(onNext: {
                    self.args.mutableImageUploadTasksStream.remove(at: index)
                    self.args.mutablePhotosStream.remove(at: index)
                }, onError: { (error) in
                    debugPrint(error)
                })
                .disposeOnDeactivate(interactor: self)
        }
    }
    func presenterClose() {
        listener?.cancel()
    }
    func presenterContinue() {
        listener?.ribOCDescriptionContinue()
    }
    
    // MARK: - RIBCategorySelectorListener
    
    func didSelectSpecialization(_ specialization: Specialization, _ specializationGroup: SpecializationGroup) {
        presenter.setSpecialization(name: specialization.name)
        args.mutableSpecializationStream.update(with: specialization)
        args.mutableSpecializationStream.updateSpecializationGroup(with: specializationGroup)
        router?.dismissCurrentChild()
    }
    func categorySelectorDidEndSelection() {
        router?.dismissCurrentChild()
    }
    
    // MARK: - LocationEditorListener
    
    func didLocationEditFinish() {
        router?.dismissCurrentChild()
    }
    func locationEditor(didSelectLocation location: Location) {
        presenter.isLocationViewerHidden = false
        presenter.isLocationRemoveButtonHidden = false
        presenter.isLocationAddButtonHidden = true
        presenter.reloadData()
        args.mutableLocationStream.update(with: location)
        router?.dismissCurrentChild()
    }
    
    // MARK: - RIBTextEditorListener
    
    func textEditor(didEditingCompleteWithText text: String?) {
        args.mutableDescriptionStream.update(with: text)
        router?.dismissCurrentChild()
    }
    
    // MARK: - RIBImagePickerListener
    
    func imagePickerFinishPick() {
        router?.dismissCurrentChild()
    }
    func imagePicker(didSelectImage image: UIImage?) {
        guard let image = image else {
            return
        }
        let orderId = args.orderIdentifier
        let imageUploadTask = args.storage.uploadOrderImage(orderId: orderId, image: image)
        imageUploadTask.onSuccess = { [unowned self ] task in
            let photo = Photo(storagePath: task.storageReference.fullPath,
                              downloadUrlString: task.downloadUrl?.absoluteString)
            self.args.mutablePhotosStream.append(photo)
            self.presenter.reloadImageCollectionView()
        }
        imageUploadTask.onError = { [unowned self] task, error in
            self.args.mutableImageUploadTasksStream.remove(task)
            debugPrint(error.localizedDescription)
        }
        args.mutableImageUploadTasksStream.append(imageUploadTask)
        imageUploadTask.upload()
        router?.dismissCurrentChild()
    }
    
    // MARK: - Private
    
    private let args: RIBOCDescriptionInteractorArgsProtocol
    
    private var photos: [Photo] = []
    
    private let descriptionPlaceholder =
    """
    Подробно опишите суть и особенности заказа
    """
    
    // MARK: - RX
    
    private let disposeBag = DisposeBag()
    
    private func subscribe() {
        subscribeOnValidation()
    }
    
    // MARK: Subscriptions
    
    private func subscribeOnValidation() {
        args.validationStream.value
            .subscribe(onNext: { [unowned self] validation in
                self.onNextValidation(isValid: validation)
            })
            .disposed(by: disposeBag)
    }
    
    // MARK: Handlers
    
    private func onNextValidation(isValid: Bool) {
        presenter.setFormValidation(isValid: isValid)
    }
    
}
