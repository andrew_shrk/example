//
//  RIBOCDescriptionComponent+RIBCategorySelector.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 29.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RIBOCDescriptionDependencyRIBCategorySelector: Dependency {}

extension RIBOCDescriptionComponent: RIBCategorySelectorDependency {}
