//
//  RIBOCDescriptionComponent+LocationEditor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 02.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

/// The dependencies needed from the parent scope of RiBOCDescription to provide for the LocationEditor scope.
protocol RIBOCDescriptionDependencyLocationEditor: Dependency {}

extension RIBOCDescriptionComponent: LocationEditorDependency {}
