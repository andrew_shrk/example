//
//  RIBOCDescriptionComponent+ImagePicker.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 29.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

/// The dependencies needed from the parent scope of RIBOCDescription to provide for the ImagePicker scope.
protocol RIBOCDescriptionDependencyImagePicker: Dependency {}

extension RIBOCDescriptionComponent: ImagePickerDependency {}
