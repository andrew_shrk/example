//
//  RIBOCContactsRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 26.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RIBOCContactsInteractable: Interactable {
    var router: RIBOCContactsRouting? { get set }
    var listener: RIBOCContactsListener? { get set }
}

protocol RIBOCContactsViewControllable: ViewControllable {}

typealias RIBOCContactsRouterType = ViewableRouter<RIBOCContactsInteractable, RIBOCContactsViewControllable>
    & RIBOCContactsRouting

final class RIBOCContactsRouter: RIBOCContactsRouterType {

    override init(interactor: RIBOCContactsInteractable, viewController: RIBOCContactsViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
