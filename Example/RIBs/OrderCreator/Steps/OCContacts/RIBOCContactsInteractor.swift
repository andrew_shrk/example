//
//  RIBOCContactsInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 26.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift

protocol RIBOCContactsRouting: ViewableRouting {}

protocol RIBOCContactsPresentable: Presentable {
    var listener: RIBOCContactsPresentableListener? { get set }
}

protocol RIBOCContactsListener: class {
    func cancel()
    func publish()
}

typealias RIBOCContactsInteractorType = PresentableInteractor<RIBOCContactsPresentable>
    & RIBOCContactsInteractable
    & RIBOCContactsPresentableListener

final class RIBOCContactsInteractor: RIBOCContactsInteractorType {

    weak var router: RIBOCContactsRouting?
    weak var listener: RIBOCContactsListener?

    init(presenter: RIBOCContactsPresentable, args: RIBOCContactsInteractorArgsProtocol) {
        self.args = args
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: PresenterListener
    
    func presenterDidUpdateDisplayName(with value: String?) {
        args.mutableContactsStream.updateDisplayName(with: value)
    }
    func presenterDidUpdateDisplayPhoneNumber(with value: String?) {
        args.mutableContactsStream.updatePhoneNumber(with: value)
    }
    func presenterDidUpdateOnlyApplication(with value: Bool) {
        args.mutableOrderSteam.updateContactDisplayPermission(displayAvailable: !value)
    }
    func presenterCancel() {
        listener?.cancel()
    }
    func presenterPublish() {
        listener?.publish()
    }
    
    // MARK: - Private
    
    private let args: RIBOCContactsInteractorArgsProtocol
    
}
