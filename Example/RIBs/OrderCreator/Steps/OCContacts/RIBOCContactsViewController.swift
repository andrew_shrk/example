//
//  RIBOCContactsViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 26.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol RIBOCContactsPresentableListener: class {
    func presenterDidUpdateDisplayName(with value: String?)
    func presenterDidUpdateDisplayPhoneNumber(with value: String?)
    func presenterDidUpdateOnlyApplication(with value: Bool)
    func presenterCancel()
    func presenterPublish()
}

typealias RIBOCContactsViewControllerType = UITableViewController
    & RIBOCContactsPresentable
    & RIBOCContactsViewControllable

final class RIBOCContactsViewController: RIBOCContactsViewControllerType {
    
    init(style: UITableView.Style, args: RIBOCContactsViewControllerArgsProtocol) {
        self.args = args
        super.init(style: style)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    weak var listener: RIBOCContactsPresentableListener?
    
    // MARK: - UIViewController override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Контакты"
        addRightBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(didTapCancelButton))
        displayNameTextFieldCell.textField.addTarget(self,
                                                     action: #selector(didTextFieldEditingEnd),
                                                     for: .editingDidEnd)
        displayPhoneNumberTextFieldCell.textField.addTarget(self,
                                                            action: #selector(didTextFieldEditingEnd),
                                                            for: .editingDidEnd)
        onlyApplicationSwitchCell.switcher.addTarget(self,
                                                     action: #selector(didSwitchValueChanged),
                                                     for: .valueChanged)
        bind()
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        return sections[section][row]
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) {
            switch cell {
            case publishButtonCell: listener?.presenterPublish()
            default:
                break
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: - Private
    
    private let args: RIBOCContactsViewControllerArgsProtocol
    
    // MARK: UI
    
    // MARK: Actions
    
    @objc func didTextFieldEditingEnd(_ sender: UITextField) {
        let value = sender.text
        switch sender {
        case displayNameTextFieldCell.textField:
            listener?.presenterDidUpdateDisplayName(with: value)
        case displayPhoneNumberTextFieldCell.textField:
            listener?.presenterDidUpdateDisplayPhoneNumber(with: value)
        default:
            break
        }
    }
    @objc func didSwitchValueChanged(_ sender: UISwitch) {
        let value = sender.isOn
        switch sender {
        case onlyApplicationSwitchCell.switcher:
            listener?.presenterDidUpdateOnlyApplication(with: value)
        default:
            break
        }
    }
    @objc func didTapCancelButton(_ sender: UIBarButtonItem) {
        listener?.presenterCancel()
    }
    
    // MARK: Cells
    
    // User's contacts
    private lazy var displayNameTextFieldCell: UITextFieldTableViewCell = {
        let displayNameTextFieldCell = args.uiBuilder.tableViewCellBuilder.buildTextFieldCell()
        displayNameTextFieldCell.titleLabel.text = "Имя"
        displayNameTextFieldCell.textField.placeholder = "Ваше имя"
        displayNameTextFieldCell.selectionStyle = .none
        return displayNameTextFieldCell
    }()
    private lazy var displayPhoneNumberTextFieldCell: UIPhoneFieldTableViewCell = {
        let displayPhoneNumberTextFieldCell = args.uiBuilder.tableViewCellBuilder.buildPhoneFieldCell()
        displayPhoneNumberTextFieldCell.titleLabel.text = "Контактный телефон"
        displayPhoneNumberTextFieldCell.selectionStyle = .none
        return displayPhoneNumberTextFieldCell
    }()
    private lazy var onlyApplicationSwitchCell: UISwitchTableViewCell = {
        let onlyApplicationSwitchCell = args.uiBuilder.tableViewCellBuilder.buildSwitchCell()
        onlyApplicationSwitchCell.textLabel?.text = "Получать только завявки"
        return onlyApplicationSwitchCell
    }()
    
    // Controls
    private lazy var publishButtonCell = args.uiBuilder.tableViewCellBuilder
        .buildFilledButtonCell(withTitle: "Опубликовать")
    
    // MARK: Sections
    
    // User's contacts
    private lazy var _userContactsSection = [
        displayNameTextFieldCell,
        displayPhoneNumberTextFieldCell
    ]
    private var userContactsSection: [UITableViewCell] {
        _userContactsSection.filter { !$0.isHidden }
    }
    
    // Control section
    private lazy var _controlSection = [
        publishButtonCell
    ]
    private var controlSection: [UITableViewCell] {
        _controlSection.filter { !$0.isHidden }
    }
    
    // Applications
    private lazy var _applicationSection = [
        onlyApplicationSwitchCell
    ]
    private var applicationSection: [UITableViewCell] {
        _applicationSection.filter { !$0.isHidden }
    }
    
    // Sections
    private lazy var _sections = [
        userContactsSection,
        applicationSection,
        controlSection
    ]
    private var sections: [[UITableViewCell]] {
        _sections.filter { $0.count != 0 }
    }
    
    // MARK: - RX
    
    private let disposeBag = DisposeBag()
    
    // MARK: Bind
    
    private func bind() {
        bindDisplayName()
        bindDisplayPhoneNumber()
        bindOnlyApplications()
    }
    private func bindDisplayName() {
        let textField = displayNameTextFieldCell.textField
        args.contactsStream
            .displayNameValue
            .bind(to: textField.rx.text)
            .disposed(by: disposeBag)
    }
    private func bindDisplayPhoneNumber() {
        let textField = displayPhoneNumberTextFieldCell.textField
        args.contactsStream
            .phoneNumberValue
            .bind(to: textField.rx.text)
            .disposed(by: disposeBag)
    }
    private func bindOnlyApplications() {
        let switcher = onlyApplicationSwitchCell.switcher
        args.orderStream
            .contactDisplayPermissionValue
            .bind { [unowned switcher] (value) in
                switcher.isOn = !value
            }
            .disposed(by: disposeBag)
    }
    
}
