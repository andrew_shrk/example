//
//  RIBOCContactsBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 26.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RIBOCContactsDependency: Dependency {
    var uiBuilder: UIBuilder { get }
    var mutableOrderSteam: MutableOCOrderStream { get }
    var mutableContactsStream: MutableOCContactsStream { get }
}

// MARK: ViewController args
protocol RIBOCContactsViewControllerArgsProtocol {
    var uiBuilder: UIBuilder { get }
    var orderStream: OCOrderStream { get }
    var contactsStream: OCContactsStream { get }
}
// MARK: Interactor args
protocol RIBOCContactsInteractorArgsProtocol {
    var mutableOrderSteam: MutableOCOrderStream { get }
    var mutableContactsStream: MutableOCContactsStream { get }
}

final class RIBOCContactsComponent: Component<RIBOCContactsDependency> {
    var uiBuilder: UIBuilder {
        dependency.uiBuilder
    }
    var mutableOrderSteam: MutableOCOrderStream {
        dependency.mutableOrderSteam
    }
    var mutableContactsStream: MutableOCContactsStream {
        dependency.mutableContactsStream
    }
}

// MARK: - Builder

protocol RIBOCContactsBuildable: Buildable {
    func build(withListener listener: RIBOCContactsListener) -> RIBOCContactsRouting
}

final class RIBOCContactsBuilder: Builder<RIBOCContactsDependency>, RIBOCContactsBuildable {

    override init(dependency: RIBOCContactsDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: RIBOCContactsListener) -> RIBOCContactsRouting {
        let component = RIBOCContactsComponent(dependency: dependency)
        
        // View controller
        let viewControllerArgs = RIBOCContactsViewControllerArgs(uiBuilder: component.uiBuilder,
                                                                 orderStream: component.mutableOrderSteam,
                                                                 contactsStream: component.mutableContactsStream)
        let viewController = RIBOCContactsViewController(style: .insetGroupedIfAvailable, args: viewControllerArgs)
        
        // Interactor
        let interactorArgs = RIBOCContactsInteractorArgs(mutableOrderSteam: component.mutableOrderSteam,
                                                         mutableContactsStream: component.mutableContactsStream)
        let interactor = RIBOCContactsInteractor(presenter: viewController, args: interactorArgs)
        
        interactor.listener = listener
        return RIBOCContactsRouter(interactor: interactor, viewController: viewController)
    }
}

struct RIBOCContactsViewControllerArgs: RIBOCContactsViewControllerArgsProtocol {
    var uiBuilder: UIBuilder
    var orderStream: OCOrderStream
    var contactsStream: OCContactsStream
}
struct RIBOCContactsInteractorArgs: RIBOCContactsInteractorArgsProtocol {
    var mutableOrderSteam: MutableOCOrderStream
    var mutableContactsStream: MutableOCContactsStream
}
