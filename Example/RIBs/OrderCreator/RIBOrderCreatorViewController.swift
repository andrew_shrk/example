//
//  RIBOrderCreatorViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 26.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol RIBOrderCreatorPresentableListener: class {
    func presenterSaveDraft()
    func presenterDelete()
}

typealias RIBOrderCreatorVC = UINavigationController
    & RIBOrderCreatorPresentable
    & RIBOrderCreatorViewControllable

final class RIBOrderCreatorViewController: RIBOrderCreatorVC {

    weak var listener: RIBOrderCreatorPresentableListener?
    
    // MARK: UIViewController override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
    }
    
    // MARK: - ViewControllable
    
    func show(viewController: ViewControllable) {
        show(viewController.uiviewController, sender: self)
        addChild(viewController.uiviewController)
    }
    
    // MARK: - Presentable
    
    func showCloseAlert(withDraft: Bool) {
        let closeCancelAction = buildCloseCancelAction()
        let closeSaveDraftAction = buildCloseSaveDraftAction { [unowned self] _ in
            self.listener?.presenterSaveDraft()
        }
        let closeDeleteAction = buildCloseRemoveAction { [unowned self] _ in
            self.listener?.presenterDelete()
        }
        let closeAlert = buildCloseAlert()
        if withDraft {
            closeAlert.addAction(closeSaveDraftAction)
        }
        closeAlert.addAction(closeDeleteAction)
        closeAlert.addAction(closeCancelAction)
        present(closeAlert, animated: true, completion: nil)
    }
    
    // MARK: UI
    
    private func buildCloseAlert() -> UIAlertController {
        let closeAlert = UIAlertController(title: "Отмена создания заказ",
                                           message: "Вы уверены, что хотите отменить создание заказа?",
                                           preferredStyle: .actionSheet)
        return closeAlert
    }
    private func buildCloseCancelAction() -> UIAlertAction {
        return UIAlertAction(title: "Отмена", style: .cancel)
    }
    private func buildCloseSaveDraftAction(_ handler: ((UIAlertAction) -> Void)?) -> UIAlertAction {
        return UIAlertAction(title: "Сохранить как черновик",
                             style: .default,
                             handler: handler)
    }
    private func buildCloseRemoveAction(_ handler: ((UIAlertAction) -> Void)?) -> UIAlertAction {
        return UIAlertAction(title: "Удалить заказ",
                             style: .destructive,
                             handler: handler)
    }
}
