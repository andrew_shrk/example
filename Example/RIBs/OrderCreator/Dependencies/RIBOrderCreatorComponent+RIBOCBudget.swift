//
//  RIBOrderCreatorComponent+RIBOCBudget.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 27.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

/// The dependencies needed from the parent scope of RIBOrderCreator to provide for the RIBOCBudget scope.
protocol RIBOrderCreatorDependencyRIBOCBudget: Dependency {
    var uiBuilder: UIBuilder { get }
}

extension RIBOrderCreatorComponent: RIBOCBudgetDependency {
    var mutableBudgetStream: MutableOCBudgetStream {
        streams.budget
    }
}
