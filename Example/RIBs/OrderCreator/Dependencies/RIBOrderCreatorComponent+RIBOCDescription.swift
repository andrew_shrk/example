//
//  RIBOrderCreatorComponent+RIBOCDescription.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 27.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

/// The dependencies needed from the parent scope of RIBOrderCreator to provide for the RIBOCDescription scope.
protocol RIBOrderCreatorDependencyRIBOCDescription: Dependency {}

extension RIBOrderCreatorComponent: RIBOCDescriptionDependency {
    var validationStream: OCValidationStream {
        streams.validation
    }
}
