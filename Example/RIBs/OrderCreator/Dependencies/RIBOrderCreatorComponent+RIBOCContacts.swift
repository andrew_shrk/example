//
//  RIBOrderCreatorComponent+RIBOCContacts.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 27.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

/// The dependencies needed from the parent scope of RIBOrderCreator to provide for the RIBOCContacts scope.
protocol RIBOrderCreatorDependencyRIBOCContacts: Dependency {}

extension RIBOrderCreatorComponent: RIBOCContactsDependency {
    var mutableOrderSteam: MutableOCOrderStream {
        streams.order
    }
    
    var mutableContactsStream: MutableOCContactsStream {
        streams.contacts
    }
}
