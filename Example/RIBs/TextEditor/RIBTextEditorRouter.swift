//
//  RIBTextEditorRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 29.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RIBTextEditorInteractable: Interactable {
    var router: RIBTextEditorRouting? { get set }
    var listener: RIBTextEditorListener? { get set }
}

protocol RIBTextEditorViewControllable: ViewControllable {}

typealias RIBTextEditorRouterType =
    ViewableRouter<RIBTextEditorInteractable, RIBTextEditorViewControllable>
    & RIBTextEditorRouting

final class RIBTextEditorRouter: RIBTextEditorRouterType {

    override init(interactor: RIBTextEditorInteractable, viewController: RIBTextEditorViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
