//
//  RIBTextEditorViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 29.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol RIBTextEditorPresentableListener: class {
    func didEditingComplete(withText text: String?)
}

final class RIBTextEditorViewController: UIViewController, RIBTextEditorPresentable, RIBTextEditorViewControllable {

    weak var listener: RIBTextEditorPresentableListener?
    
    // MARK: - UIViewController override
    
    override func loadView() {
        super.loadView()
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        link()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        textView.resignFirstResponder()
    }
    
    // MARK: - Presentable
    
    func setText(_ text: String?) {
        textView.text = text
    }
    func setPlaceholder(_ placeholder: String?) {
        textView.placeholder = placeholder
    }
    func setTitle(_ title: String?) {
        self.title = title
    }
    
    // MARK: - UI
    
    // MARK: Actions
    
    @objc func didTapDoneButton() {
        let text = textView.text
        listener?.didEditingComplete(withText: text)
    }
    
    // MARK: Builders
    
    private lazy var textView: UITextView = {
        let textView = UITextView()
        textView.font = .preferredFont(forTextStyle: .body)
        return textView
    }()
    private lazy var doneButton: UIBarButtonItem = {
        var item: UIBarButtonItem.SystemItem = {
            if #available(iOS 13.0, *) {
                return .close
            } else {
                return .done
            }
        }()
        let doneButton = UIBarButtonItem(barButtonSystemItem: item,
                                         target: self,
                                         action: #selector(didTapDoneButton))
        return doneButton
    }()
    
    // MARK: Linking
    
    private func link() {
        navigationItem.rightBarButtonItem = doneButton
        view.addSubview(textView)
        textView.snp.makeConstraints { (maker) in
            maker.top.equalTo(view.snp.topMargin)
            maker.leading.equalTo(view.snp.leadingMargin)
            maker.trailing.equalTo(view.snp.trailingMargin)
            maker.bottom.equalTo(view.snp.centerY)
        }
        textView.becomeFirstResponder()
    }
}
