//
//  RIBTextEditorInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 29.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift

protocol RIBTextEditorRouting: ViewableRouting {}

protocol RIBTextEditorPresentable: Presentable {
    var listener: RIBTextEditorPresentableListener? { get set }
    func setText(_ text: String?)
    func setPlaceholder(_ placeholder: String?)
    func setTitle(_ title: String?)
}

protocol RIBTextEditorListener: class {
    func textEditor(didEditingCompleteWithText text: String?)
}

final class RIBTextEditorInteractor: PresentableInteractor<RIBTextEditorPresentable>, RIBTextEditorInteractable, RIBTextEditorPresentableListener {

    weak var router: RIBTextEditorRouting?
    weak var listener: RIBTextEditorListener?

    init(presenter: RIBTextEditorPresentable, args: RIBTextEditorInteractorArgsProtocol) {
        self.args = args
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        presenter.setText(args.text)
        presenter.setTitle(args.title)
        presenter.setPlaceholder(args.placeholder)
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - PresentableListener
    
    func didEditingComplete(withText text: String?) {
        listener?.textEditor(didEditingCompleteWithText: text)
    }
    
    // MARK: - Private
    
    private let args: RIBTextEditorInteractorArgsProtocol
    
}
