//
//  RIBTextEditorBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 29.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RIBTextEditorDependency: Dependency {}
protocol RIBTextEditorArgsProtocol {
    var text: String? { get }
    var placeholder: String? { get }
    var title: String? { get }
}
protocol RIBTextEditorInteractorArgsProtocol {
    var text: String? { get }
    var placeholder: String? { get }
    var title: String? { get }
}

final class RIBTextEditorComponent: Component<RIBTextEditorDependency> {}

// MARK: - Builder

protocol RIBTextEditorBuildable: Buildable {
    func build(withListener listener: RIBTextEditorListener, args: RIBTextEditorArgsProtocol) -> RIBTextEditorRouting
}

final class RIBTextEditorBuilder: Builder<RIBTextEditorDependency>, RIBTextEditorBuildable {

    override init(dependency: RIBTextEditorDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: RIBTextEditorListener, args: RIBTextEditorArgsProtocol) -> RIBTextEditorRouting {
        _ = RIBTextEditorComponent(dependency: dependency)
        let viewController = RIBTextEditorViewController()
        
        let interactorArgs = RIBTextEditorInteractorArgs(text: args.text,
                                                         placeholder: args.placeholder,
                                                         title: args.title)
        let interactor = RIBTextEditorInteractor(presenter: viewController, args: interactorArgs)
        interactor.listener = listener
        
        return RIBTextEditorRouter(interactor: interactor, viewController: viewController)
    }
}

struct RIBTextEditorInteractorArgs: RIBTextEditorInteractorArgsProtocol {
    var text: String?
    var placeholder: String?
    var title: String?
}
struct RIBTextEditorArgs: RIBTextEditorArgsProtocol {
    var text: String?
    var placeholder: String?
    var title: String?
}
