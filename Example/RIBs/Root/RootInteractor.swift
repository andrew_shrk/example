//
//  RootInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import FirebaseAuth

protocol RootRouting: ViewableRouting {
    func routeToLoggedIn()
    func routeToLoggedOut()
}

protocol RootPresentable: Presentable {
    var listener: RootPresentableListener? { get set }
}

protocol RootListener: class {}

final class RootInteractor: PresentableInteractor<RootPresentable>, RootInteractable, RootPresentableListener {
    
    weak var router: RootRouting?
    weak var listener: RootListener?

    init(presenter: RootPresentable, authStateStream: MutableAuthStateStream, auth: Auth) {
        self.authStateStream = authStateStream
        self.auth = auth
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        self.subscribeOnAuthStateStream()
        auth.addStateDidChangeListener { (_, user) in
            if user == nil {
                self.authStateStream.update(with: .loggedOut)
            } else {
                if user?.displayName != nil {
                    self.authStateStream.update(with: .loggedInWithRegistration)
                } else {
                    self.authStateStream.update(with: .loggedInWithoutRegistration)
                }
            }
        }
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - Private
    
    private var auth: Auth
    private var authStateStream: MutableAuthStateStream
   
    // MARK: - RX
    
    // MARK: Subscriptions
    
    private func subscribeOnAuthStateStream() {
        authStateStream
            .authState
            .subscribe(onNext: { [weak self] authState in
                self?.onNextAuthState(authState)
            })
            .disposeOnDeactivate(interactor: self)
    }
    
    // MARK: Handles
    
    private func onNextAuthState(_ authState: AuthState) {
        switch authState {
        case .loggedInWithRegistration:
            router?.routeToLoggedIn()
        case .loggedOut, .loggedInWithoutRegistration:
            router?.routeToLoggedOut()
        case .none:
            break
        }
    }
    
}
