//
//  RootComponent+LoggedOut.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RootDependencyLoggedOut: Dependency {}

extension RootComponent: LoggedOutDependency {
    var loggedOutViewController: LoggedOutViewControllable {
        rootViewController
    }
}
