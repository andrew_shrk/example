//
//  RootComponent+LoggedIn.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

/// The dependencies needed from the parent scope of Root to provide for the LoggedIn scope.
protocol RootDependencyLoggedIn: Dependency {}

extension RootComponent: LoggedInDependency {
    var loggedInViewController: LoggedInViewControllable {
        self.rootViewController
    }
}
