//
//  RootRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RootInteractable: Interactable, LoggedInListener, LoggedOutListener {
    var router: RootRouting? { get set }
    var listener: RootListener? { get set }
}

protocol RootViewControllable: LoggedOutViewControllable {
    func present(viewController: ViewControllable)
    func dismiss(viewController: ViewControllable)
}

final class RootRouter: LaunchRouter<RootInteractable, RootViewControllable>, RootRouting {

    init(interactor: RootInteractable,
         viewController: RootViewControllable,
         loggedInBuilder: LoggedInBuildable,
         loggedOutBuilder: LoggedOutBuildable) {
        
        self.loggedInBuilder = loggedInBuilder
        self.loggedOutBuilder = loggedOutBuilder
        
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
    
    // MARK: - RootRouting
    
    func routeToLoggedIn() {
        detachCurrentChild()
        attachLoggedIn()
    }
    func routeToLoggedOut() {
        detachCurrentChild()
        attachLoggedOut()
    }
    
    // MARK: - Private
    
    private var currentChild: Routing?
    
    // MARK: Builders
    
    private var loggedInBuilder: LoggedInBuildable
    private var loggedOutBuilder: LoggedOutBuildable
    
    // MARK: RIBs management
    
    private func attachLoggedIn() {
        let loggedIn = loggedInBuilder.build(withListener: interactor)
        self.currentChild = loggedIn
        attachChild(loggedIn)
    }
    private func attachLoggedOut() {
        let loggedOut = loggedOutBuilder.build(withListener: interactor)
        self.currentChild = loggedOut
        attachChild(loggedOut)
    }
    
    private func detachCurrentChild() {
        if let currentChild = currentChild {
            detachChild(currentChild)
        }
        if let currentChild = currentChild as? ViewableRouting {
            viewController.dismiss(viewController: currentChild.viewControllable)
        }
        currentChild = nil
    }

}
