//
//  RootBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage
import CodableFirebase

protocol RootDependency: RootDependencyLoggedIn {}

final class RootComponent: Component<RootDependency> {
    
    let rootViewController: RootViewController

    init(dependency: RootDependency,
         rootViewController: RootViewController) {
        self.rootViewController = rootViewController
        super.init(dependency: dependency)
    }

    var mutableAuthStateStream: MutableAuthStateStream {
        shared { AuthStateStreamImpl() }
    }
    var auth: Auth {
        shared {
            let auth = Auth.auth()
            auth.languageCode = "Ru"
            return auth
        }
    }
    var appVersion: AppVersionProtocol {
        shared {
            guard
                let dictionary = Bundle.main.infoDictionary,
                let version = dictionary["CFBundleShortVersionString"] as? String,
                let build = dictionary["CFBundleVersion"] as? String
            else {
                fatalError("Error: Unavailable version or build number")
            }
            return AppVersion(versionNumber: version, buildNumber: build)
        }
    }
    var dataModel: DataModel {
        shared {
            let firestore = Firestore.firestore()
            let decoder = FirestoreDecoder()
            return FSDataModel(with: firestore, decoder: decoder)
        }
    }
    var storage: StorageModel {
        shared {
            let storage = Storage.storage()
            return FBStorage(storage: storage)
        }
    }
    var uiBuilder: UIBuilder {
        shared {
            return UIBuilder()
        }
    }
    
}

// MARK: - Builder

protocol RootBuildable: Buildable {
    func build() -> LaunchRouting
}

final class RootBuilder: Builder<RootDependency>, RootBuildable {

    override init(dependency: RootDependency) {
        super.init(dependency: dependency)
    }

    func build() -> LaunchRouting {
        let viewController = RootViewController()
        
        let component = RootComponent(dependency: dependency,
                                      rootViewController: viewController)
        
        let interactor = RootInteractor(presenter: viewController,
                                        authStateStream: component.mutableAuthStateStream,
                                        auth: component.auth)
        
        let loggedInBuilder = LoggedInBuilder(dependency: component)
        let loggedOutBuilder = LoggedOutBuilder(dependency: component)
        
        return RootRouter(interactor: interactor,
                          viewController: viewController,
                          loggedInBuilder: loggedInBuilder,
                          loggedOutBuilder: loggedOutBuilder)
    }
}
