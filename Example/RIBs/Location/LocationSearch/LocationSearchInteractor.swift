//
//  LocationSearchInteractor.swift
//  TicTacToe
//
//  Created by Андрей Дурыманов on 31.01.2020.
//  Copyright © 2020 Uber. All rights reserved.
//

import RIBs
import RxSwift
import GooglePlaces

protocol LocationSearchRouting: ViewableRouting {}

protocol LocationSearchPresentable: Presentable {
    var listener: LocationSearchPresentableListener? { get set }
}

protocol LocationSearchListener: class {
    func didFinishSelectingPlace(with location: Location)
    func didUndoSelection()
}

typealias LocationSearchInteractorType = PresentableInteractor<LocationSearchPresentable>
    & LocationSearchInteractable
    & LocationSearchPresentableListener

final class LocationSearchInteractor: LocationSearchInteractorType {
    
    weak var router: LocationSearchRouting?
    weak var listener: LocationSearchListener?

    override init(presenter: LocationSearchPresentable) {
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - LocationSearchPresentableListener
    
    func didSelectPlace(_ place: GMSPlace) {
        let location = Location(name: place.name, coordinates: place.coordinate.fsCoordinate())
        listener?.didFinishSelectingPlace(with: location)
    }
    
    func didUndoSelection() {
        listener?.didUndoSelection()
    }
}
