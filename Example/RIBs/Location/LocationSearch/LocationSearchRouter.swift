//
//  LocationSearchRouter.swift
//  TicTacToe
//
//  Created by Андрей Дурыманов on 31.01.2020.
//  Copyright © 2020 Uber. All rights reserved.
//

import RIBs

protocol LocationSearchInteractable: Interactable {
    var router: LocationSearchRouting? { get set }
    var listener: LocationSearchListener? { get set }
}

protocol LocationSearchViewControllable: ViewControllable {}

typealias LocationSearchRouterType =
    ViewableRouter<LocationSearchInteractable, LocationSearchViewControllable>
    & LocationSearchRouting

final class LocationSearchRouter: LocationSearchRouterType {

    override init(interactor: LocationSearchInteractable, viewController: LocationSearchViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
