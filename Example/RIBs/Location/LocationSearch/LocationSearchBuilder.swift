//
//  LocationSearchBuilder.swift
//  TicTacToe
//
//  Created by Андрей Дурыманов on 31.01.2020.
//  Copyright © 2020 Uber. All rights reserved.
//

import RIBs
import GoogleMaps

protocol LocationSearchDependency: Dependency {}

final class LocationSearchComponent: Component<LocationSearchDependency> {
    
    init(dependency: LocationSearchDependency, bounds: GMSCoordinateBounds?, query: String?) {
        self.bounds = bounds
        self.query = query
        super.init(dependency: dependency)
    }
    
    fileprivate var bounds: GMSCoordinateBounds?
    fileprivate var query: String?
    
}

// MARK: - Builder

protocol LocationSearchBuildable: Buildable {
    func build(withListener listener: LocationSearchListener,
               bounds: GMSCoordinateBounds?,
               query: String?) -> LocationSearchRouting
}

final class LocationSearchBuilder: Builder<LocationSearchDependency>, LocationSearchBuildable {
    override init(dependency: LocationSearchDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: LocationSearchListener,
               bounds: GMSCoordinateBounds?,
               query: String?) -> LocationSearchRouting
    {
        _ = LocationSearchComponent(dependency: dependency,
                                        bounds: bounds,
                                        query: query)
        let viewController = LocationSearchViewController(bounds: bounds, 
                                                          query: query)
        let interactor = LocationSearchInteractor(presenter: viewController)
        interactor.listener = listener
        return LocationSearchRouter(interactor: interactor, viewController: viewController)
    }
}
