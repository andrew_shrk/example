//
//  LocationSearchViewController.swift
//  TicTacToe
//
//  Created by Андрей Дурыманов on 31.01.2020.
//  Copyright © 2020 Uber. All rights reserved.
//

import RIBs
import RxSwift
import UIKit
import GooglePlaces
import GoogleMaps

protocol LocationSearchPresentableListener: class {
    func didSelectPlace(_ place: GMSPlace)
    func didUndoSelection()
}

final class LocationSearchViewController: UIViewController, LocationSearchPresentable, LocationSearchViewControllable {

    weak var listener: LocationSearchPresentableListener?
    
    init(bounds: GMSCoordinateBounds?, query: String?) {
        self.bounds = bounds
        self.query = query
        super.init(nibName: nil, bundle: nil)
    }
    
    init() {
        self.bounds = nil
        self.query = nil
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Method is not supported")
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        let resultViewController = buildResultViewController()
        buildSearchController(withResultViewController: resultViewController, query: query)
        buildCancelButton()
        setupNavBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchController?.isActive = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        searchController.isActive = false
        super.viewDidDisappear(animated)
    }
    
    // MARK: - Actions
    
    @objc func didTapCancelButton() {
        listener?.didUndoSelection()
    }
    
    // MARK: - Private
    
    private let query: String?
    private let bounds: GMSCoordinateBounds?
    
    private var searchController: UISearchController!
    private var resultViewController: GMSAutocompleteResultsViewController!
    
    private func setupNavBar() {
        title = "Местоположение"
    }
    
    private func buildResultViewController() -> GMSAutocompleteResultsViewController {
        let resultViewController = GMSAutocompleteResultsViewController()
        self.resultViewController = resultViewController
        resultViewController.delegate = self
        resultViewController.autocompleteBoundsMode = .bias
        resultViewController.autocompleteBounds = bounds
        return resultViewController
    }
    
    private func buildSearchController(withResultViewController resultViewController: GMSAutocompleteResultsViewController,
                                       query: String?) {
        let searchBar = UISearchController(searchResultsController: resultViewController)
        searchBar.delegate = self
        navigationItem.searchController = searchBar
        navigationItem.searchController?.searchResultsUpdater = resultViewController
        definesPresentationContext = true
        // Prevent the navigation bar from being hidden when searching.
        searchBar.hidesNavigationBarDuringPresentation = false
        searchBar.searchBar.text = query
        if #available(iOS 13.0, *) {
            searchBar.automaticallyShowsCancelButton = false
        }
        searchController = searchBar
    }
    
    private func buildCancelButton() {
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .undo,
                                           target: self,
                                           action: #selector(didTapCancelButton))
        navigationItem.rightBarButtonItem = cancelButton
    }
}

// Handle the user's selection.
extension LocationSearchViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        listener?.didSelectPlace(place)
    }

    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }

    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }

    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

extension LocationSearchViewController: UISearchControllerDelegate {
    func presentSearchController(_ searchController: UISearchController) {
        searchController.searchBar.becomeFirstResponder()
    }
}
