//
//  LocationStream.swift
//  TicTacToe
//
//  Created by Андрей Дурыманов on 05.02.2020.
//  Copyright © 2020 Uber. All rights reserved.
//

import RxSwift
import MapKit
import RxCocoa

protocol LocationStream: class {
    var location: Observable<Location?> { get }
}

protocol MutableLocationStream: LocationStream {
    func update(with coordinates: CLLocationCoordinate2D, name: String?)
    func update(with newLocation: Location?)
}

class LocationStreamImpl: MutableLocationStream {
    
    var location: Observable<Location?> {
        return variable.asObservable()
    }
    
    func update(with coordinates: CLLocationCoordinate2D, name: String?) {
        let newLocation = {
            return Location(name: name, coordinates: coordinates.fsCoordinate())
        }()
        variable.accept(newLocation)
    }
    
    func update(with newLocation: Location?) {
        variable.accept(newLocation)
    }
    
    // MARK: - Private
    private var variable = BehaviorRelay<Location?>(value: nil)
}
