//
//  MapStateStream.swift
//  TicTacToe
//
//  Created by Андрей Дурыманов on 06.02.2020.
//  Copyright © 2020 Uber. All rights reserved.
//

import RxSwift
import RxCocoa

enum MapState {
    case loaded, none
}

protocol MapStateStream: class {
    var state: Observable<MapState> { get }
}

protocol MutableMapStateStream: MapStateStream {
    func update(with newMapState: MapState)
}

class MapStateStreamImpl: MutableMapStateStream {
    var state: Observable<MapState> {
        variable.asObservable().distinctUntilChanged()
    }
    func update(with newMapState: MapState) {
        variable.accept(newMapState)
    }
    // MARK: - Private
    var variable = BehaviorRelay<MapState>(value: .none)
    
}
