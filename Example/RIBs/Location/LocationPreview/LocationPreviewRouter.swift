//
//  LocationPreviewRouter.swift
//  TicTacToe
//
//  Created by Андрей Дурыманов on 06.02.2020.
//  Copyright © 2020 Uber. All rights reserved.
//

import RIBs

protocol LocationPreviewInteractable: Interactable {
    var router: LocationPreviewRouting? { get set }
    var listener: LocationPreviewListener? { get set }
}

protocol LocationPreviewViewControllable: ViewControllable {}

typealias LocationPreviewRouterType =
    ViewableRouter<LocationPreviewInteractable, LocationPreviewViewControllable>
    & LocationPreviewRouting

final class LocationPreviewRouter: LocationPreviewRouterType {
    override init(interactor: LocationPreviewInteractable, viewController: LocationPreviewViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
