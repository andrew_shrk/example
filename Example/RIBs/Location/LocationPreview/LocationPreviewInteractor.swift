//
//  LocationPreviewInteractor.swift
//  TicTacToe
//
//  Created by Андрей Дурыманов on 06.02.2020.
//  Copyright © 2020 Uber. All rights reserved.
//

import RIBs
import RxSwift

protocol LocationPreviewRouting: ViewableRouting {}

protocol LocationPreviewPresentable: Presentable {
    var listener: LocationPreviewPresentableListener? { get set }
    func showLocation(location: LocationType)
    func showCurrentLocationMarker()
}

protocol LocationPreviewListener: class {
    func finishLocationPresentation()
}

typealias LocationPreviewInteractorType = PresentableInteractor<LocationPreviewPresentable>
    & LocationPreviewInteractable
    & LocationPreviewPresentableListener

final class LocationPreviewInteractor: LocationPreviewInteractorType {

    weak var router: LocationPreviewRouting?
    weak var listener: LocationPreviewListener?

    override init(presenter: LocationPreviewPresentable) {
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - LocationPreviewPresentableListener
    
    func didRequestClose() {
        listener?.finishLocationPresentation()
    }
    
}
