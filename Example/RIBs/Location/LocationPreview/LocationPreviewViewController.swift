//
//  LocationPreviewViewController.swift
//  TicTacToe
//
//  Created by Андрей Дурыманов on 06.02.2020.
//  Copyright © 2020 Uber. All rights reserved.
//

import RIBs
import RxSwift
import UIKit
import GoogleMaps
import SnapKit

protocol LocationPreviewPresentableListener: class {
    func didRequestClose()
}

typealias LocationPreviewViewControllerType = UIViewController
    & LocationPreviewPresentable
    & LocationPreviewViewControllable

final class LocationPreviewViewController: LocationPreviewViewControllerType {

    weak var listener: LocationPreviewPresentableListener?
    
    init(locationStream: LocationStream, style: LocationPreviewStyle = .none) {
        self.locationStream = locationStream
        self.style = style
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let mapView = buildMapView(withStyle: style)
        // UI Build
        buildCurrentPositionMarker(withMapView: mapView)
        if style != .compact {
            buildCloseButton()
            buildNavBar()
            buildLocationNameButton()
        }
        view.snp.makeConstraints { (maker) in
            maker.height.greaterThanOrEqualTo(128)
        }
        // Subscriptions
        subscribeOnLocationStream()
    }
    
    // MARK: - LocationPreviewPresentable
    
    func showLocation(location: LocationType) {
        let coordinates = location.coordinates.clGeoPoint()
        let cameraUpdate = GMSCameraUpdate.setTarget(coordinates, zoom: 17)
        currentPositionMarker.position = coordinates
        mapView.animate(with: cameraUpdate)
        locationNameButton?.setTitle(location.name, for: .normal)
    }
    
    func showCurrentLocationMarker() {
        let markerPosition = currentPositionMarker.position
        let cameraUpdate = GMSCameraUpdate.setTarget(markerPosition)
        mapView.animate(with: cameraUpdate)
    }
    
    // MARK: - Actions
    
    @objc func didTapCloseButton() {
        listener?.didRequestClose()
    }
    
    @objc func didTapCurrentLocationName() {
        showCurrentLocationMarker()
    }
    
    // MARK: - Private
    
    private var mapView: GMSMapView!
    private var currentPositionMarker: GMSMarker!
    private var locationNameButton: UIButton?
    
    private var locationStream: LocationStream
    private var style: LocationPreviewStyle
    
    private let disposeBag = DisposeBag()
    
    // MARK: - Subscriptions
    
    private func subscribeOnLocationStream() {
        locationStream
            .location
            .subscribe(onNext: { location in
                if let location = location {
                    self.showLocation(location: location)
                } else {
                    self.listener?.didRequestClose()
                }
            })
            .disposed(by: disposeBag)
    }
    
    // MARK: - UI Builders
    
    private func buildMapView(withStyle style: LocationPreviewStyle) -> GMSMapView {
        let mapView = GMSMapView()
        view.addSubview(mapView)
        mapView.isMyLocationEnabled = true
        mapView.isUserInteractionEnabled = style != .compact ? true : false
        mapView.snp.makeConstraints { (maker: ConstraintMaker) in
            maker.trailing.leading.bottom.top.equalTo(self.view)
        }
        do {
          mapView.mapStyle = try GMSMapStyle(jsonString: kMapStyle)
        } catch {
          NSLog("One or more of the map styles failed to load. \(error)")
        }
        self.mapView = mapView
        return mapView
    }
    
    private func buildCurrentPositionMarker(withMapView mapView: GMSMapView) {
        let currentPositionMarker = GMSMarker()
        currentPositionMarker.map = mapView
        self.currentPositionMarker = currentPositionMarker
    }
    
    private func buildCloseButton() {
        let item: UIBarButtonItem.SystemItem = {
            if #available(iOS 13.0, *) {
                return .close
            } else {
                return .done
            }
        }()
        let closeButton = UIBarButtonItem(barButtonSystemItem: item,
                                          target: self,
                                          action: #selector(didTapCloseButton))
        navigationItem.rightBarButtonItem = closeButton
    }
    
    private func buildNavBar() {
        title = "Местоположение"
    }
    
    private func buildLocationNameButton() {
        let locationNameButton = UIButton(type: .system)
        locationNameButton.backgroundColor = .white
        locationNameButton.layer.cornerRadius = 10
        locationNameButton.setShadow(.default)
        view.addSubview(locationNameButton)
        locationNameButton.snp.makeConstraints { (maker: ConstraintMaker) in
            maker.bottom.equalTo(self.view.snp_bottomMargin).offset(-40)
            maker.leading.trailing.equalTo(self.view).inset(20)
            maker.height.equalTo(48)
        }
        locationNameButton.addTarget(self,
                                     action: #selector(didTapCurrentLocationName),
                                     for: .touchUpInside)
        self.locationNameButton = locationNameButton
    }
}
