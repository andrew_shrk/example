//
//  LocationPreviewBuilder.swift
//  TicTacToe
//
//  Created by Андрей Дурыманов on 06.02.2020.
//  Copyright © 2020 Uber. All rights reserved.
//

import RIBs

protocol LocationPreviewDependency: Dependency {
    var locationStream: LocationStream { get }
}

final class LocationPreviewComponent: Component<LocationPreviewDependency> {

    fileprivate var locationStream: LocationStream {
        dependency.locationStream
    }
}

// MARK: - Builder

protocol LocationPreviewBuildable: Buildable {
    func build(withListener listener: LocationPreviewListener,
               style: LocationPreviewStyle) -> LocationPreviewRouting
}

final class LocationPreviewBuilder: Builder<LocationPreviewDependency>, LocationPreviewBuildable {

    override init(dependency: LocationPreviewDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: LocationPreviewListener,
               style: LocationPreviewStyle = .none
    ) -> LocationPreviewRouting {
        _ = LocationPreviewComponent(dependency: dependency)
        let viewController = LocationPreviewViewController(locationStream: dependency.locationStream, style: style)
        let interactor = LocationPreviewInteractor(presenter: viewController)
        interactor.listener = listener
        return LocationPreviewRouter(interactor: interactor, viewController: viewController)
    }
}

enum LocationPreviewStyle {
    case none, compact
}
