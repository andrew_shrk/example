//
//  LocationEditorInteractor.swift
//  TicTacToe
//
//  Created by Андрей Дурыманов on 25.01.2020.
//  Copyright © 2020 Uber. All rights reserved.
//

import RIBs
import RxSwift
import GooglePlaces
import GoogleMaps
import MapKit

protocol LocationEditorRouting: ViewableRouting {
    func routeToLocationSearch(withQueryString query: String?, bounds: GMSCoordinateBounds?)
    func closeLocationSearch()
}

protocol LocationEditorPresentable: Presentable, CLLocationManagerDelegate {
    var listener: LocationEditorPresentableListener? { get set }
    func zoomToRegion(_ region: MKCoordinateRegion, animated: Bool)
    func showError(_ error: Error)
    func showLocationName(_ name: String)
}

protocol LocationEditorListener: class {
    func didLocationEditFinish()
    func locationEditor(didSelectLocation location: Location)
}

typealias LocationEditorInteractorType = PresentableInteractor<LocationEditorPresentable>
    & LocationEditorInteractable
    & LocationEditorPresentableListener

final class LocationEditorInteractor: LocationEditorInteractorType {

    weak var router: LocationEditorRouting?
    weak var listener: LocationEditorListener?

    init(presenter: LocationEditorPresentable,
         locationManager: CLLocationManager,
         mutableLocationStream: MutableLocationStream
    ) {
        self.locationManager = locationManager
        self.mutableLocationStream = mutableLocationStream
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        buildLocationManager(withPresenter: presenter)
        buildGeocoder()
        let mapStateObservable = mutableMapStateStream.state.filter({ $0 != .none })
        let locationObservable = mutableLocationStream.location.elementAt(0)
        Observable.combineLatest(mapStateObservable, locationObservable) { _, location in
            self.setupMap(withInitialLocation: location)
        }
        .subscribe()
        .disposeOnDeactivate(interactor: self)
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - LocationEditorPresentableListener
    
    func finishEditing(with name: String?, coordinates: CLLocationCoordinate2D) {
        let location = Location(name: name, coordinates: coordinates.fsCoordinate())
        mutableLocationStream.update(with: location)
        listener?.locationEditor(didSelectLocation: location)
    }
    
    func undoEditing() {
        listener?.didLocationEditFinish()
    }
    
    func startSearch(withQuery query: String?, bounds: GMSCoordinateBounds?) {
        router?.routeToLocationSearch(withQueryString: query, bounds: bounds)
    }
    
    func moveToUserLocation() {
        zoomToUserLocation(withLocationManager: locationManager, animated: true)
    }
    
    func didChangeVisibleRegionCenter(_ center: CLLocationCoordinate2D) {
        geocoder.reverseGeocodeCoordinate(center) { (response, error) in
            if let error = error {
                self.presenter.showError(error)
            }
            if let response = response {
                self.updateCurrentLocationName(withGeocoderResponse: response)
            }
        }
    }
    
    func didLoadMapView() {
        mutableMapStateStream.update(with: .loaded)
    }
    
    // MARK: - CLLocationManager delegate
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        mutableLocationStream
            .location
            .subscribe(onNext: {[unowned self] (location) in
                if location == nil {
                    self.zoomToUserLocation(withLocationManager: manager, animated: false)
                }
            })
            .dispose()
    }
    
    // MARK: - Private
    
    private var mutableLocationStream: MutableLocationStream
    private var mutableMapStateStream = MapStateStreamImpl()
    
    private var locationManager: CLLocationManager
    private var geocoder: GMSGeocoder!
    
    private func setupMap(withInitialLocation initialLocation: Location?) {
        if let location = initialLocation {
            self.zoomToLocation(location, animated: false)
        } else {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                self.requestUserPermissionForLocationServicesIfNeeded()
            case .authorizedAlways, .authorizedWhenInUse:
                self.zoomToUserLocation(withLocationManager: self.locationManager, animated: false)
            default:
                break
            }
        }
    }
    
    private func buildLocationManager(withPresenter presenter: LocationEditorPresentable) {
        let locationManager = CLLocationManager()
        self.locationManager = locationManager
        locationManager.delegate = presenter
    }
    
    private func buildGeocoder() {
        self.geocoder = GMSGeocoder()
    }
    
    private func requestUserPermissionForLocationServicesIfNeeded() {
        if CLLocationManager.authorizationStatus() == .notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    private func zoomToUserLocation(withLocationManager locationManager: CLLocationManager,
                                    animated: Bool) {
        if let coords = locationManager.location?.coordinate {
            zoomToCoords(coords, animated: true)
        }
    }
    
    private func zoomToLocation(_ location: Location, animated: Bool) {
        zoomToCoords(location.coordinates.clGeoPoint(), animated: animated)
    }
    
    private func zoomToCoords(_ coordinates: CLLocationCoordinate2D, animated: Bool) {
        let region = MKCoordinateRegion(center: coordinates, latitudinalMeters: 200, longitudinalMeters: 200)
        presenter.zoomToRegion(region, animated: animated)
    }
    
    private func updateCurrentLocationName(withGeocoderResponse response: GMSReverseGeocodeResponse) {
        if let firstLine = response.firstResult()?.lines?.first {
            let locationNameArray = firstLine
                .split(separator: ",")
                .filter({ $0 != "Unnamed Road" })
            
            let locationName: String = {
                switch locationNameArray.count {
                case 0...2:
                    return "Неизвестный адрес"
                default:
                    return locationNameArray[...1].joined(separator: ",")
                }
            }()
            
            presenter.showLocationName(locationName)
        }
    }
    
}

// MARK: - LocationSearchListener
extension LocationEditorInteractor {
    func didFinishSelectingPlace(with location: Location) {
        zoomToLocation(location, animated: false)
        router?.closeLocationSearch()
    }
    
    func didUndoSelection() {
        router?.closeLocationSearch()
    }
}
