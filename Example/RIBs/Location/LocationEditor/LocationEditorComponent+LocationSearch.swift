//
//  LocationEditorComponent+LocationSearch.swift
//  TicTacToe
//
//  Created by Андрей Дурыманов on 31.01.2020.
//  Copyright © 2020 Uber. All rights reserved.
//

import RIBs

/// The dependencies needed from the parent scope of LocationEditor to provide for the LocationSearch scope.
protocol LocationEditorDependencyLocationSearch: Dependency {}

extension LocationEditorComponent: LocationSearchDependency {}
