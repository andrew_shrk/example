//
//  LocationEditorViewController.swift
//  TicTacToe
//
//  Created by Андрей Дурыманов on 25.01.2020.
//  Copyright © 2020 Uber. All rights reserved.
//

import RIBs
import RxSwift
import UIKit
import SnapKit
import GoogleMaps
import MapKit

protocol LocationEditorPresentableListener: class {
    func finishEditing(with name: String?, coordinates: CLLocationCoordinate2D)
    func undoEditing()
    func startSearch(withQuery query: String?, bounds: GMSCoordinateBounds?)
    func moveToUserLocation()
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus)
    func didChangeVisibleRegionCenter(_ center: CLLocationCoordinate2D)
    func didLoadMapView()
}

final class LocationEditorViewController: UIViewController, LocationEditorPresentable, LocationEditorViewControllable {
    
    weak var listener: LocationEditorPresentableListener?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let mapView = buildMapView()
        buildCurrentPositionAnnotation(withMapView: mapView)
        let confirmButton = buildConfirmButton()
        buildCloseButton()
        let locationNameTextField = buildLocationNameTextField()
        buildLocationSearchButton(withLocationNameView: locationNameTextField)
        buildToUserLocationButton(withConfirmButton: confirmButton)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        buildMapCamera()
    }
    
    // MARK: - LocationEditorPresentable
    func show(viewController: ViewControllable) {
        show(viewController.uiviewController, sender: self)
    }
    
    // MARK: - Actions
    
    @objc func didTapConfirmButton() {
        let locationName = locationNameTextField.text
        let locationCoordinates = currentPositionMarker.position
        listener?.finishEditing(with: locationName, coordinates: locationCoordinates)
    }
    
    @objc func didTapCloseButton() {
        listener?.undoEditing()
    }
    
    @objc func didTapSearchButton() {
        let query = locationNameTextField?.text
        let bounds: GMSCoordinateBounds? = {
            if let visibleRegion = mapView?.projection.visibleRegion() {
                return GMSCoordinateBounds(region: visibleRegion)
            } else {
                return nil
            }
        }()
        listener?.startSearch(withQuery: query, bounds: bounds)
    }
    
    @objc func didTapToUserLocationButton() {
        listener?.moveToUserLocation()
    }
    
    // MARK: - LocationPickerPresentable
    
    func zoomToRegion(_ region: MKCoordinateRegion, animated: Bool) {
        let cameraUpdate = GMSCameraUpdate.setTarget(region.center, zoom: 17)
        if animated {
            mapView?.animate(with: cameraUpdate)
        } else {
            mapView?.moveCamera(cameraUpdate)
        }
        currentPositionMarker?.position = region.center
    }
    
    func showLocationName(_ name: String) {
        locationNameTextField?.text = name
    }
    
    func showError(_ error: Error) {
        let errorAlert = buildErrorDialog(withMessage: error.localizedDescription)
        present(errorAlert, animated: true, completion: nil)
    }
    
    // MARK: - CLLocationManager delegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        listener?.locationManager(manager, didChangeAuthorization: status)
    }
    
    // MARK: - Private
    
    private var mapView: GMSMapView!
    private var mapCamera: GMSCameraPosition!
    private var locationNameTextField: UILabel!
    private var currentPositionMarker: GMSMarker!
    
    // MARK: - UI Builders
    
    private func buildErrorDialog(withMessage message: String) -> UIAlertController {
        let alert = UIAlertController(title: "Ошибка",
                                      message: message,
                                      preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Закрыть",
                                        style: .cancel,
                                        handler: nil)
        alert.addAction(closeAction)
        return alert
    }
    
    private func buildToUserLocationButton(withConfirmButton confirmButton: UIButton) {
        let toUserLocationButton = UIButton(type: .system)
        let locationImage = UIImage(named: "icon_my_location_24dp")
        toUserLocationButton.setImage(locationImage, for: .normal)
        toUserLocationButton.layer.cornerRadius = 18
        toUserLocationButton.backgroundColor = .white
        view.addSubview(toUserLocationButton)
        toUserLocationButton.setShadow(.default)
        toUserLocationButton.snp.makeConstraints { (maker: ConstraintMaker) in
            maker.bottom.equalTo(confirmButton.snp.top).offset(-40)
            maker.trailing.equalTo(self.view.snp_trailingMargin)
            maker.height.width.equalTo(36)
        }
        toUserLocationButton.addTarget(self,
                                       action: #selector(didTapToUserLocationButton),
                                       for: .touchUpInside)
    }
    
    private func buildMapView() -> GMSMapView {
        let mapView = GMSMapView(frame: .zero)
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.mapType = .normal
        mapView.isIndoorEnabled = false
        do {
          mapView.mapStyle = try GMSMapStyle(jsonString: kMapStyle)
        } catch {
          NSLog("One or more of the map styles failed to load. \(error)")
        }
        view.addSubview(mapView)
        self.mapView = mapView
        mapView.snp.makeConstraints { (maker: ConstraintMaker) in
            maker.leading.trailing.bottom.top.equalTo(self.view)
        }
        return mapView
    }
    
    private func buildMapCamera() {
        if self.mapCamera == nil {
            let camera = GMSCameraPosition()
            mapView?.camera = camera
            self.mapCamera = camera
            listener?.didLoadMapView()
        }
    }
    
    private func buildCurrentPositionAnnotation(withMapView mapView: GMSMapView) {
        let currentPositionMarker = GMSMarker()
        currentPositionMarker.map = mapView
        currentPositionMarker.position = mapView.camera.target
        self.currentPositionMarker = currentPositionMarker
    }
    
    private func buildCloseButton() {
        let systemItemType: UIBarButtonItem.SystemItem = {
            if #available(iOS 13.0, *) {
                return .close
            } else {
                return .cancel
            }
        }()
        let closeButton = UIBarButtonItem(barButtonSystemItem: systemItemType,
                                          target: self,
                                          action: #selector(didTapCloseButton))
        navigationItem.rightBarButtonItem = closeButton
    }
    
    private func buildLocationNameTextField() -> UILabel {
        let tapGestureRecognizer =
            UITapGestureRecognizer(target: self,
                                   action: #selector(didTapSearchButton))
        let locationNameLabel = UILabel()
        self.locationNameTextField = locationNameLabel
        locationNameLabel.textAlignment = .center
        locationNameLabel.addGestureRecognizer(tapGestureRecognizer)
        locationNameLabel.font = UIFont.preferredFont(forTextStyle: .headline)
        locationNameLabel.numberOfLines = 0
        locationNameLabel.isUserInteractionEnabled = true
        locationNameLabel.textColor = .darkText
        view.addSubview(locationNameLabel)
        locationNameLabel.snp.makeConstraints { (maker: ConstraintMaker) in
            maker.top.equalTo(self.view.snp_topMargin).offset(40)
            maker.leading.trailing.equalTo(self.view).inset(20)
        }
        return locationNameLabel
    }
    
    private func buildLocationSearchButton(withLocationNameView locationNameView: UIView) {
        let locationSearchButton = UIButton()
        locationSearchButton.setTitle("Изменить адрес", for: .normal)
        locationSearchButton.setTitleColor(.darkGray, for: .normal)
        locationSearchButton.titleLabel?.font = UIFont
            .preferredFont(forTextStyle: .subheadline)
        view.addSubview(locationSearchButton)
        locationSearchButton.snp.makeConstraints { (maker: ConstraintMaker) in
            maker.top.equalTo(locationNameView.snp.bottom)
            maker.centerX.equalTo(self.view.snp.centerX)
        }
        locationSearchButton.addTarget(self,
                                       action: #selector(didTapSearchButton),
                                       for: .touchUpInside)
    }
    
    private func buildConfirmButton() -> UIButton {
        let confirmButton = UIButton(type: .system)
        confirmButton.backgroundColor = .systemBlue
        confirmButton.setTitle("Выбрать", for: .normal)
        confirmButton.tintColor = .white
        confirmButton.layer.cornerRadius = 10
        view.addSubview(confirmButton)
        confirmButton.setShadow(.default)
        confirmButton.snp.makeConstraints { (maker: ConstraintMaker) in
            maker.bottom.equalTo(self.view).offset(-40)
            maker.leading.trailing.equalTo(self.view).inset(20)
            maker.height.equalTo(48)
        }
        confirmButton.addTarget(self,
                                action: #selector(didTapConfirmButton),
                                for: .touchUpInside)
        return confirmButton
    }

}

// MARK: - GMSMapViewDelegate
extension LocationEditorViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        CATransaction.begin()
        CATransaction.setAnimationDuration(0.0)
        currentPositionMarker?.position = position.target
        CATransaction.commit()
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        listener?.didChangeVisibleRegionCenter(position.target)
    }
    
}

let kMapStyle = """
[
  {
    "featureType": "poi.business",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  }
]
"""
