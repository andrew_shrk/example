//
//  LocationEditorRouter.swift
//  TicTacToe
//
//  Created by Андрей Дурыманов on 25.01.2020.
//  Copyright © 2020 Uber. All rights reserved.
//

import RIBs
import CoreLocation
import GoogleMaps

protocol LocationEditorInteractable: Interactable, LocationSearchListener {
    var router: LocationEditorRouting? { get set }
    var listener: LocationEditorListener? { get set }
}

protocol LocationEditorViewControllable: ViewControllable {
    func show(viewController: ViewControllable)
    func dismiss(viewController: ViewControllable)
}

typealias LocationEditorRouterType =
    ViewableRouter<LocationEditorInteractable, LocationEditorViewControllable>
    & LocationEditorRouting

final class LocationEditorRouter: LocationEditorRouterType {
    
    init(interactor: LocationEditorInteractable,
         viewController: LocationEditorViewControllable,
         locationSearchBuilder: LocationSearchBuildable) {
        self.locationSearchBuilder = locationSearchBuilder
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
    
    // MARK: - LocationEditorRouting
    func routeToLocationSearch(withQueryString query: String?, bounds: GMSCoordinateBounds?) {
        attachLocationSearch(withQuery: query, bounds: bounds)
    }
    
    func closeLocationSearch() {
        detachLocationSearch()
    }

    // MARK: - Private
    
    private let locationSearchBuilder: LocationSearchBuildable
    
    private var locationSearch: LocationSearchRouting?
    
    private var locationSearchPresentable: ViewControllable?
    
    private func attachLocationSearch(withQuery query: String?, bounds: GMSCoordinateBounds?) {
        let locationSearch = locationSearchBuilder.build(withListener: interactor,
                                                         bounds: bounds,
                                                         query: query)
        let navController = UINavigationController(rootViewController: locationSearch.viewControllable)
        attachChild(locationSearch)
        viewController.show(viewController: navController)
        self.locationSearch = locationSearch
        self.locationSearchPresentable = navController
    }
    
    private func detachLocationSearch() {
        if let locationSearch = locationSearch {
            detachChild(locationSearch)
            self.locationSearch = nil
        }
        if let locationSearchPresentable = locationSearchPresentable {
            viewController.dismiss(viewController: locationSearchPresentable)
            self.locationSearchPresentable = nil
        }
    }
}
