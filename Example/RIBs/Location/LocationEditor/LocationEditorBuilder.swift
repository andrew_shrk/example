//
//  LocationEditorBuilder.swift
//  TicTacToe
//
//  Created by Андрей Дурыманов on 25.01.2020.
//  Copyright © 2020 Uber. All rights reserved.
//

import RIBs
import MapKit

protocol LocationEditorDependency: Dependency {}

final class LocationEditorComponent: Component<LocationEditorDependency> {
    var mutableLocationStream: MutableLocationStream {
        shared { LocationStreamImpl() }
    }
}

// MARK: - Builder

protocol LocationEditorBuildable: Buildable {
    func build(withListener listener: LocationEditorListener, defaultLocation: Location?) -> LocationEditorRouting
}

final class LocationEditorBuilder: Builder<LocationEditorDependency>, LocationEditorBuildable {

    override init(dependency: LocationEditorDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: LocationEditorListener, defaultLocation: Location?) -> LocationEditorRouting {
        let component = LocationEditorComponent(dependency: dependency)
        
        component.mutableLocationStream.update(with: defaultLocation)
         
        let viewController = LocationEditorViewController()
        let locationManager = CLLocationManager()
        let interactor = LocationEditorInteractor(presenter: viewController,
                                                  locationManager: locationManager,
                                                  mutableLocationStream: component.mutableLocationStream)
        interactor.listener = listener
    
        let locationSearchBuilder = LocationSearchBuilder(dependency: component)
        
        return LocationEditorRouter(interactor: interactor,
                                    viewController: viewController,
                                    locationSearchBuilder: locationSearchBuilder)
    }
}
