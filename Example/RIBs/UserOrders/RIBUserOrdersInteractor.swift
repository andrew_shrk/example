//
//  RIBUserOrdersInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 23.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift

protocol RIBUserOrdersRouting: ViewableRouting {}

protocol RIBUserOrdersPresentable: Presentable {
    var listener: RIBUserOrdersPresentableListener? { get set }
}

protocol RIBUserOrdersListener: class {
    func ribUserOrdersShowOrderDetailed(withOrderId orderId: String)
}

typealias RIBUserOrdersInteractorType = PresentableInteractor<RIBUserOrdersPresentable>
    & RIBUserOrdersInteractable
    & RIBUserOrdersPresentableListener

final class RIBUserOrdersInteractor: RIBUserOrdersInteractorType {

    weak var router: RIBUserOrdersRouting?
    weak var listener: RIBUserOrdersListener?
    
    init(presenter: RIBUserOrdersPresentable, args: RIBUserOrdersInteractorArgsProtocol) {
        self.args = args
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - PresentableListener
    
    func didSelectRow(at indexPath: IndexPath) {
        let order = args.ordersStream.element(at: indexPath.row)
        guard let orderId = order.identifier else { return }
        listener?.ribUserOrdersShowOrderDetailed(withOrderId: orderId)
    }
    
    // MARK: - Private
    
    private let args: RIBUserOrdersInteractorArgsProtocol
    
}
