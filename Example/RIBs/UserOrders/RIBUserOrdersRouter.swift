//
//  RIBUserOrdersRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 23.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RIBUserOrdersInteractable: Interactable {
    var router: RIBUserOrdersRouting? { get set }
    var listener: RIBUserOrdersListener? { get set }
}

protocol RIBUserOrdersViewControllable: ViewControllable {}

typealias RIBUserOrdersRouterType = ViewableRouter<RIBUserOrdersInteractable, RIBUserOrdersViewControllable>
    & RIBUserOrdersRouting

final class RIBUserOrdersRouter: RIBUserOrdersRouterType {

    override init(interactor: RIBUserOrdersInteractable, viewController: RIBUserOrdersViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
