//
//  RIBUserOrdersBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 23.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RIBUserOrdersDependency: Dependency {
    var mutableOrdersStream: MutableOrdersStream { get }
}
protocol RIBUserOrdersInteractorArgsProtocol {
    var ordersStream: OrdersStream { get }
}
protocol RIBUserOrdersPresenterArgsProtocol {
    var ordersStream: OrdersStream { get }
}

final class RIBUserOrdersComponent: Component<RIBUserOrdersDependency> {
    fileprivate var mutableOrdersStream: MutableOrdersStream {
        dependency.mutableOrdersStream
    }
}

// MARK: - Builder

protocol RIBUserOrdersBuildable: Buildable {
    func build(withListener listener: RIBUserOrdersListener) -> RIBUserOrdersRouting
}

final class RIBUserOrdersBuilder: Builder<RIBUserOrdersDependency>, RIBUserOrdersBuildable {

    override init(dependency: RIBUserOrdersDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: RIBUserOrdersListener) -> RIBUserOrdersRouting {
        let component = RIBUserOrdersComponent(dependency: dependency)
        
        // View controller
        let presenterArgs = RIBUserOrdersPresenterArgs(ordersStream: component.mutableOrdersStream)
        let viewController = RIBUserOrdersViewController(style: .grouped, args: presenterArgs)
        
        // Interactor
        let interactorArgs = RIBUserOrdersInteractorArgs(ordersStream: component.mutableOrdersStream)
        let interactor = RIBUserOrdersInteractor(presenter: viewController, args: interactorArgs)
        interactor.listener = listener
        
        return RIBUserOrdersRouter(interactor: interactor, viewController: viewController)
    }
}

struct RIBUserOrdersInteractorArgs: RIBUserOrdersInteractorArgsProtocol {
    var ordersStream: OrdersStream
}
struct RIBUserOrdersPresenterArgs: RIBUserOrdersPresenterArgsProtocol {
    var ordersStream: OrdersStream
}
