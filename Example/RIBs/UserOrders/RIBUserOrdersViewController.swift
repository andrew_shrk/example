//
//  RIBUserOrdersViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 23.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol RIBUserOrdersPresentableListener: class {
    func didSelectRow(at indexPath: IndexPath)
}

typealias RIBUserOrdersVC = UITableViewController
    & RIBUserOrdersPresentable
    & RIBUserOrdersViewControllable

final class RIBUserOrdersViewController: RIBUserOrdersVC {

    weak var listener: RIBUserOrdersPresentableListener?
    
    init(style: UITableView.Style, args: RIBUserOrdersPresenterArgs) {
        self.args = args
        super.init(style: style)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UIViewController override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewController()
        setupTableView()
        bindOrderList()
    }
    
    // MARK: UITableView delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        listener?.didSelectRow(at: indexPath)
    }
    
    // MARK: - Private
    
    private let args: RIBUserOrdersPresenterArgs
    
    private let disposeBag = DisposeBag()
    
    private let reusableIdentifier = "order_cell"
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
        return dateFormatter
    }()
    
    // MARK: - UI
    
    private func setupViewController() {
        title = "Мои заказы"
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    private func setupTableView() {
        tableView.register(UIOrderTableViewCell.self, forCellReuseIdentifier: reusableIdentifier)
        tableView.dataSource = nil
    }
    
    // MARK: - RX
    
    // MARK: Subscriptions
    
    private func bindOrderList() {
        args.ordersStream
            .value
            .map({ (value) -> [OrderProtocol] in
                let newValue = value.filter { $0.dateCreation != nil }
                return newValue.sorted { (lhs, rhs) -> Bool in
                    if let leftDateCreation = lhs.dateCreation, let rightDateCreation = rhs.dateCreation {
                        return leftDateCreation.seconds > rightDateCreation.seconds
                    } else {
                        return false
                    }
                }
            })
            .bind(to: tableView.rx.items(cellIdentifier: reusableIdentifier)) { (_, order: OrderProtocol, cell: UIOrderTableViewCell) in
                cell.setTitle(title: order.mainCategoryName)
                cell.setSubTitle(subTitle: order.subCategoryName)
                cell.setBudget(budgetFrom: order.minBudget, budgetTo: order.maxBudget)
                cell.setAuthorName(name: order.authorName)
                cell.setDescriptionText(text: order.description)
                if let dateCreation = order.dateCreation {
                    cell.setDateCreation(dateString: self.dateFormatter.string(from: dateCreation.dateValue()))
                } else {
                    cell.setDateCreation(dateString: nil)
                }
                cell.setFavorite(isFavorite: order.isFavorite)
                cell.setPhotos(withDownloadUrls: order.attachedPhotosUrs)
                cell.setStatus(order.status)
                
            }.disposed(by: disposeBag)
    }
    
}
