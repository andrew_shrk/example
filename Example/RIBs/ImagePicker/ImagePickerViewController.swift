//
//  ImagePickerViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 15.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import YPImagePicker

protocol ImagePickerPresentableListener: class {
    func didCancelSelection()
    func didSelectImage(image: UIImage?)
}

final class ImagePickerViewController: YPImagePicker, ImagePickerPresentable, ImagePickerViewControllable {

    weak var listener: ImagePickerPresentableListener?
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        buildImagePicker()
    }
    
    // MARK: - Private
    
    // MARK: - UI
    
    // MARK: Builders
    
    private func buildImagePicker() {
        didFinishPicking { [unowned self] items, canceling in
            if canceling {
                self.listener?.didCancelSelection()
            } else if let photo = items.singlePhoto {
                self.listener?.didSelectImage(image: photo.image)
            } else {
                self.listener?.didSelectImage(image: nil)
            }
        }
    }
}
