//
//  ImagePickerInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 15.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift

protocol ImagePickerRouting: ViewableRouting {}

protocol ImagePickerPresentable: Presentable {
    var listener: ImagePickerPresentableListener? { get set }
}

protocol ImagePickerListener: class {
    func imagePickerFinishPick()
    func imagePicker(didSelectImage image: UIImage?)
}

final class ImagePickerInteractor: PresentableInteractor<ImagePickerPresentable>,
ImagePickerInteractable,
ImagePickerPresentableListener {

    weak var router: ImagePickerRouting?
    weak var listener: ImagePickerListener?

    override init(presenter: ImagePickerPresentable) {
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - Presentable
    
    func didCancelSelection() {
        listener?.imagePickerFinishPick()
    }
    func didSelectImage(image: UIImage?) {
        listener?.imagePicker(didSelectImage: image)
    }
    
}
