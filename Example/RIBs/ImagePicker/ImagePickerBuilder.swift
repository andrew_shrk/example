//
//  ImagePickerBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 15.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import YPImagePicker

protocol ImagePickerDependency: Dependency {}

final class ImagePickerComponent: Component<ImagePickerDependency> {
    fileprivate var config: YPImagePickerConfiguration {
        var config = YPImagePickerConfiguration()
        config.showsPhotoFilters = false
        config.colors.tintColor = .systemBlue
        return config
    }
}

// MARK: - Builder

protocol ImagePickerBuildable: Buildable {
    func build(withListener listener: ImagePickerListener) -> ImagePickerRouting
}

final class ImagePickerBuilder: Builder<ImagePickerDependency>, ImagePickerBuildable {

    override init(dependency: ImagePickerDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: ImagePickerListener) -> ImagePickerRouting {
        _ = ImagePickerComponent(dependency: dependency)
        var configuration = YPImagePickerConfiguration()
        configuration.showsPhotoFilters = false
        let viewController = ImagePickerViewController(configuration: configuration)
        let interactor = ImagePickerInteractor(presenter: viewController)
        interactor.listener = listener
        return ImagePickerRouter(interactor: interactor, viewController: viewController)
    }
}
