//
//  ImagePickerRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 15.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol ImagePickerInteractable: Interactable {
    var router: ImagePickerRouting? { get set }
    var listener: ImagePickerListener? { get set }
}

protocol ImagePickerViewControllable: ViewControllable {}

final class ImagePickerRouter: ViewableRouter<ImagePickerInteractable,
ImagePickerViewControllable>,
ImagePickerRouting {

    override init(interactor: ImagePickerInteractable, viewController: ImagePickerViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
