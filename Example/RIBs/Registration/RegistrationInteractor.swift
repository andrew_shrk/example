//
//  RegistrationInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import FirebaseAuth

protocol RegistrationRouting: Routing {
    func cleanupViews()
    // Routing
    func routeToUserName()
}

protocol RegistrationListener: class {}

final class RegistrationInteractor: Interactor, RegistrationInteractable {

    weak var router: RegistrationRouting?
    weak var listener: RegistrationListener?

    init(auth: Auth, authStateStream: MutableAuthStateStream) {
        self.auth = auth
        self.authStateStream = authStateStream
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        routeToNextStep()
    }

    override func willResignActive() {
        super.willResignActive()
        router?.cleanupViews()
    }
    
    // MARK: - User name listener
    
    func didCompleteEnterUserName(withUserName name: String) {
        setUserName(with: name)
        routeToNextStep()
    }
    
    func didRequestPreviousStep() {
        routeToPreviousStep()
    }
    
    // MARK: - Private
    
    private let auth: Auth
    private let authStateStream: MutableAuthStateStream
    
    private var currentStep = -1
    
    // MARK: Methods
    
    private func finishRegistration() {
        authStateStream.update(with: .loggedInWithRegistration)
    }
    
    private func cancelRegistration() {
        do {
            try auth.signOut()
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
    // MARK: User info controls
    
    private func setUserName(with newName: String) {
        let profileChangeRequest = auth.currentUser?.createProfileChangeRequest()
        profileChangeRequest?.displayName = newName
        profileChangeRequest?.commitChanges(completion: nil)
    }
    
    // MARK: Steps control
    
    private func routeToNextStep() {
        currentStep += 1
        routeToStep(currentStep)
    }
    
    private func routeToPreviousStep() {
        currentStep -= 1
        routeToStep(currentStep)
    }
    
    private func routeToStep(_ step: Int) {
        switch step {
        case ..<0:
            cancelRegistration()
        case 0:
            router?.routeToUserName()
        default:
            finishRegistration()
        }
    }
    
}
