//
//  RegistrationComponent+RegistrationUserName.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

/// The dependencies needed from the parent scope of Registration to provide for the RegistrationUserName scope.
protocol RegistrationDependencyRegistrationUserName: Dependency {}

extension RegistrationComponent: RegistrationUserNameDependency {}
