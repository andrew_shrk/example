//
//  RegistrationBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import FirebaseAuth

protocol RegistrationDependency: Dependency {
    var registrationViewController: RegistrationViewControllable { get }
    var auth: Auth { get }
    var mutableAuthStateStream: MutableAuthStateStream { get }
}

final class RegistrationComponent: Component<RegistrationDependency> {

    fileprivate var registrationViewController: RegistrationViewControllable {
        return dependency.registrationViewController
    }
    fileprivate var auth: Auth {
        dependency.auth
    }
    fileprivate var mutableAuthStateStream: MutableAuthStateStream {
        dependency.mutableAuthStateStream
    }
}

// MARK: - Builder

protocol RegistrationBuildable: Buildable {
    func build(withListener listener: RegistrationListener) -> RegistrationRouting
}

final class RegistrationBuilder: Builder<RegistrationDependency>, RegistrationBuildable {

    override init(dependency: RegistrationDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: RegistrationListener) -> RegistrationRouting {
        let component = RegistrationComponent(dependency: dependency)
        let interactor =
            RegistrationInteractor(auth: component.auth,
                                   authStateStream: component.mutableAuthStateStream)
        interactor.listener = listener
        
        let registrationUserNameBuilder = RegistrationUserNameBuilder(dependency: component)
        
        return RegistrationRouter(interactor: interactor,
                                  viewController: component.registrationViewController,
                                  registrationUserNameBuilder: registrationUserNameBuilder)
    }
}
