//
//  RegistrationRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RegistrationInteractable: Interactable, RegistrationUserNameListener {
    var router: RegistrationRouting? { get set }
    var listener: RegistrationListener? { get set }
}

protocol RegistrationViewControllable: ViewControllable {
    func present(viewController: ViewControllable)
    func dismiss(viewController: ViewControllable)
}

final class RegistrationRouter: Router<RegistrationInteractable>, RegistrationRouting {

    init(interactor: RegistrationInteractable,
         viewController: RegistrationViewControllable,
         registrationUserNameBuilder: RegistrationUserNameBuildable) {
        self.viewController = viewController
        self.registrationUserNameBuilder = registrationUserNameBuilder
        super.init(interactor: interactor)
        interactor.router = self
    }

    func cleanupViews() {
        detachCurrentChild()
    }
    
    // MARK: - Routing
    
    func routeToUserName() {
        detachCurrentChild()
        attachRegistrationUserName()
    }

    // MARK: - Private

    private let viewController: RegistrationViewControllable
    
    private var currentChild: Routing?
    
    // MARK: - Submodules
    
    // MARK: Builders
    
    private let registrationUserNameBuilder: RegistrationUserNameBuildable
    
    // MARK: Attaching
    
    private func attachRegistrationUserName() {
        let registrationUserName = registrationUserNameBuilder
            .build(withListener: interactor)
        self.currentChild = registrationUserName
        attachChild(registrationUserName)
        registrationUserName
            .viewControllable
            .uiviewController
            .modalPresentationStyle = .fullScreen
        viewController.present(viewController: registrationUserName.viewControllable)
    }
    
    // MARK: Detaching
    
    private func detachCurrentChild() {
        if let currentChild = currentChild {
            detachChild(currentChild)
        }
        if let currentChildViewable = currentChild as? ViewableRouting {
            viewController.dismiss(viewController: currentChildViewable.viewControllable)
        }
        currentChild = nil
    }
     
}
