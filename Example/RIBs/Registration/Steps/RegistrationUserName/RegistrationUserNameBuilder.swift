//
//  RegistrationUserNameBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RegistrationUserNameDependency: Dependency {}

final class RegistrationUserNameComponent: Component<RegistrationUserNameDependency> {}

// MARK: - Builder

protocol RegistrationUserNameBuildable: Buildable {
    func build(withListener listener: RegistrationUserNameListener) -> RegistrationUserNameRouting
}

final class RegistrationUserNameBuilder: Builder<RegistrationUserNameDependency>, RegistrationUserNameBuildable {

    override init(dependency: RegistrationUserNameDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: RegistrationUserNameListener) -> RegistrationUserNameRouting {
        _ = RegistrationUserNameComponent(dependency: dependency)
        let viewController = RegistrationUserNameViewController()
        let interactor = RegistrationUserNameInteractor(presenter: viewController)
        interactor.listener = listener
        return RegistrationUserNameRouter(interactor: interactor, viewController: viewController)
    }
}
