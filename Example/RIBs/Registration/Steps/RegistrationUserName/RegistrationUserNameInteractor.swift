//
//  RegistrationUserNameInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift

protocol RegistrationUserNameRouting: ViewableRouting {}

protocol RegistrationUserNamePresentable: Presentable {
    var listener: RegistrationUserNamePresentableListener? { get set }
}

protocol RegistrationUserNameListener: class {
    func didCompleteEnterUserName(withUserName name: String)
    func didRequestPreviousStep()
}

final class RegistrationUserNameInteractor: PresentableInteractor<RegistrationUserNamePresentable>,
    RegistrationUserNameInteractable,
    RegistrationUserNamePresentableListener {

    weak var router: RegistrationUserNameRouting?
    weak var listener: RegistrationUserNameListener?

    override init(presenter: RegistrationUserNamePresentable) {
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - RegistrationUserNamePresentableListener
    
    func didFinishInputUserName(withUserName name: String) {
        listener?.didCompleteEnterUserName(withUserName: name)
    }
    
    func didRequestPreviousStep() {
        listener?.didRequestPreviousStep()
    }
}
