//
//  RegistrationUserNameRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RegistrationUserNameInteractable: Interactable {
    var router: RegistrationUserNameRouting? { get set }
    var listener: RegistrationUserNameListener? { get set }
}

protocol RegistrationUserNameViewControllable: ViewControllable {}

final class RegistrationUserNameRouter: ViewableRouter<RegistrationUserNameInteractable,
RegistrationUserNameViewControllable>,
RegistrationUserNameRouting {

    override init(interactor: RegistrationUserNameInteractable, viewController: RegistrationUserNameViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
