//
//  RegistrationUserNameViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol RegistrationUserNamePresentableListener: class {
    func didFinishInputUserName(withUserName name: String)
    func didRequestPreviousStep()
}

final class RegistrationUserNameViewController: UIViewController,
RegistrationUserNamePresentable,
RegistrationUserNameViewControllable {

    weak var listener: RegistrationUserNamePresentableListener?
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupRootView()
        self.linkUi()
        self.updateNextButtonState()
    }
    
    // MARK: - Private
    
    private weak var nameInputTextField: UITextField!
    private var nextButton: UIButton!
    
    // MARK: - UI
    
    // MARK: Builders
    
    private func buildNameInputTextField() -> UITextField {
        let textField = UITextField()
        view.addSubview(textField)
        textField.placeholder = "Введите Ваше имя"
        textField.font = UIFont.systemFont(ofSize: 24)
        textField.returnKeyType = .continue
        textField.becomeFirstResponder()
        textField.delegate = self
        textField.enablesReturnKeyAutomatically = true
        textField.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(self.view).inset(20)
            maker.bottom.equalTo(self.view.snp.centerY)
        }
        textField.addTarget(self,
                            action: #selector(didChangeNameInputValue),
                            for: .editingChanged)
        self.nameInputTextField = textField
        return textField
    }
    private func buildTitleLabel(withNameInput nameInput: UITextField) {
        let titleLabel = UILabel()
        view.addSubview(titleLabel)
        titleLabel.text = "Ваше имя"
        if #available(iOS 13.0, *) {
            titleLabel.font = UIFont.preferredFont(forTextStyle: .largeTitle)
        } else {
            titleLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        }
        titleLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(self.view).inset(20)
            maker.bottom.equalTo(nameInput.snp.top).offset(-20)
        }
    }
    private func buildNextButton(withNameInput nameInput: UITextField) {
        let nextButton = UIButton(type: .system)
        view.addSubview(nextButton)
        nextButton.setTitle("Продолжить", for: .normal)
        nextButton.addTarget(self,
                             action: #selector(didTapNextButton),
                             for: .touchUpInside)
        nextButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(nameInput.snp.bottom).offset(10)
            maker.trailing.equalTo(view.snp.trailing).inset(20)
        }
        self.nextButton = nextButton
    }
    private func buildCancelButton(withNameInput nameInput: UITextField) {
        let cancelButton = UIButton(type: .system)
        view.addSubview(cancelButton)
        cancelButton.setTitle("Назад", for: .normal)
        cancelButton.setTitleColor(.darkGray, for: .normal)
        cancelButton.addTarget(self,
                               action: #selector(didTapBackButton),
                               for: .touchUpInside)
        cancelButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(nameInput.snp.bottom).offset(10)
            maker.leading.equalTo(view.snp.leading).inset(20)
        }
    }
    
    // MARK: Linking
    
    private func linkUi() {
        let nameInput = buildNameInputTextField()
        buildNextButton(withNameInput: nameInput)
        buildTitleLabel(withNameInput: nameInput)
        buildCancelButton(withNameInput: nameInput)
    }
    
    // MARK: Actions
    
    @objc private func didTapNextButton() {
        completeEditing()
    }
    @objc private func didTapBackButton() {
        listener?.didRequestPreviousStep()
    }
    @objc private func didChangeNameInputValue() {
        updateNextButtonState()
    }
    
    // MARK: - View setup
    
    private func setupRootView() {
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
    }
    
    // MARK: - Helpers
    
    private func completeEditing() {
        if let userName = nameInputTextField.text {
            nameInputTextField.resignFirstResponder()
            listener?.didFinishInputUserName(withUserName: userName)
        }
    }
    private func updateNextButtonState() {
        if let nameValue = nameInputTextField.text {
            nextButton.isEnabled = !nameValue.isEmpty
        } else {
            nextButton.isHidden = false
        }
    }
    
}

// MARK: - UITextFieldDelegate
extension RegistrationUserNameViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        completeEditing()
        return true
    }
}
