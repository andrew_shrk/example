//
//  RIBMyOrdersRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 24.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

typealias RIBMyOrdersInteractableListener = RIBUserOrdersListener
    & RIBOrderCreatorListener

protocol RIBMyOrdersInteractable: Interactable, RIBMyOrdersInteractableListener {
    var router: RIBMyOrdersRouting? { get set }
    var listener: RIBMyOrdersListener? { get set }
}

protocol RIBMyOrdersViewControllable: ViewControllable {
    func presentAsMaster(viewController: ViewControllable)
    func presentAsDetailed(viewController: ViewControllable)
    func presentAsSheet(viewController: ViewControllable)
    func present(viewController: ViewControllable)
    func dismiss(viewController: ViewControllable)
}

typealias RIBMyOrdersRouterProtocol = ViewableRouter<RIBMyOrdersInteractable, RIBMyOrdersViewControllable>
    & RIBMyOrdersRouting

final class RIBMyOrdersRouter: RIBMyOrdersRouterProtocol {

    init(interactor: RIBMyOrdersInteractable,
         viewController: RIBMyOrdersViewControllable,
         childrenBuilders: RIBMyOrdersChildrenProtocol) {
        self.childrenBuilders = childrenBuilders
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
    
    // MARK: - Routing
    
    func presentViewControllers() {
        attachUserOrders()
    }
    func presentOrderCreator() {
        attachOrderCreator()
    }
    func dismissOrderCreator() {
        detachOrderCreator()
    }
    
    // MARK: - Private
    
    private let childrenBuilders: RIBMyOrdersChildrenProtocol
    
    // MARK: - Submodules
    
    private var orderCreator: RIBOrderCreatorRouting?
    
    // MARK: Attaching
    
    private func attachUserOrders() {
        let userOrders = childrenBuilders.userOrders.build(withListener: interactor)
        attachChild(userOrders)
        let navController = UINavigationController(rootViewController: userOrders.viewControllable)
        viewController.presentAsMaster(viewController: navController)
    }
    private func attachOrderCreator() {
        let orderCreator = childrenBuilders.orderCreator.build(withListener: interactor)
        self.orderCreator = orderCreator
        attachChild(orderCreator)
        viewController.present(viewController: orderCreator.viewControllable)
    }
    
    // MARK: - Detaching
    
    private func detachOrderCreator() {
        if let orderCreator = self.orderCreator {
            detachChild(orderCreator)
            viewController.dismiss(viewController: orderCreator.viewControllable)
        }
    }
    
}
