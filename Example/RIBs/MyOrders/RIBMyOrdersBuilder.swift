//
//  RIBMyOrdersBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 24.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import FirebaseAuth

protocol RIBMyOrdersDependency: RIBMyOrdersDependencyRIBOrderCreator {
    var dataModel: DataModel { get }
    var storage: StorageModel { get }
    var auth: Auth { get }
}
protocol RIBMyOrdersChildrenProtocol {
    var userOrders: RIBUserOrdersBuildable { get }
    var orderCreator: RIBOrderCreatorBuildable { get }
}
// MARK: Interactor args
protocol RIBMyOrdersInteractorArgsProtocol {
    var dataModel: DataModel { get }
    var auth: Auth { get }
    var mutableOrdersStream: MutableOrdersStream { get }
}

final class RIBMyOrdersComponent: Component<RIBMyOrdersDependency> {
    var mutableUserOrdersStream: MutableOrdersStream {
        shared { OrdersStreamImpl() }
    }
    var dataModel: DataModel {
        dependency.dataModel
    }
    var storage: StorageModel {
        dependency.storage
    }
    var uiBuilder: UIBuilder {
        dependency.uiBuilder
    }
    var auth: Auth {
        dependency.auth
    }
}

// MARK: - Builder

protocol RIBMyOrdersBuildable: Buildable {
    func build(withListener listener: RIBMyOrdersListener) -> RIBMyOrdersRouting
}

final class RIBMyOrdersBuilder: Builder<RIBMyOrdersDependency>, RIBMyOrdersBuildable {

    override init(dependency: RIBMyOrdersDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: RIBMyOrdersListener) -> RIBMyOrdersRouting {
        let component = RIBMyOrdersComponent(dependency: dependency)
        let viewController = RIBMyOrdersViewController()
        
        // Interactor
        let interactorArgs = RIBMyOrdersInteractorArgs(dataModel: component.dataModel,
                                                       auth: component.auth,
                                                       mutableOrdersStream: component.mutableOrdersStream)
        let interactor = RIBMyOrdersInteractor(presenter: viewController, args: interactorArgs)
        interactor.listener = listener
        
        // Routere
        let childrenBuilders =
            RIBMyOrdersChildren(userOrders: RIBUserOrdersBuilder(dependency: component),
                                orderCreator: RIBOrderCreatorBuilder(dependency: component))
        
        return RIBMyOrdersRouter(interactor: interactor,
                                 viewController: viewController,
                                 childrenBuilders: childrenBuilders)
    }
}
struct RIBMyOrdersChildren: RIBMyOrdersChildrenProtocol {
    var userOrders: RIBUserOrdersBuildable
    var orderCreator: RIBOrderCreatorBuildable
}
struct RIBMyOrdersInteractorArgs: RIBMyOrdersInteractorArgsProtocol {
    var dataModel: DataModel
    var auth: Auth
    var mutableOrdersStream: MutableOrdersStream
}
