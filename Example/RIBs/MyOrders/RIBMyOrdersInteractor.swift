//
//  RIBMyOrdersInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 24.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift

protocol RIBMyOrdersRouting: ViewableRouting {
    func presentViewControllers()
    func presentOrderCreator()
    func dismissOrderCreator()
}

protocol RIBMyOrdersPresentable: Presentable {
    var listener: RIBMyOrdersPresentableListener? { get set }
}

protocol RIBMyOrdersListener: class {}

typealias RIBMyOrdersInteractorType = PresentableInteractor<RIBMyOrdersPresentable>
    & RIBMyOrdersInteractable
    & RIBMyOrdersPresentableListener

final class RIBMyOrdersInteractor: RIBMyOrdersInteractorType {

    weak var router: RIBMyOrdersRouting?
    weak var listener: RIBMyOrdersListener?

    init(presenter: RIBMyOrdersPresentable, args: RIBMyOrdersInteractorArgsProtocol) {
        self.args = args
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        subscribe()
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - Presentable
    
    func viewControllerDidLoad() {
        router?.presentViewControllers()
    }
    func didRequestOrderCreate() {
        router?.presentOrderCreator()
    }
    
    // MARK: - RIBUserOrders listener
     
    func ribUserOrdersShowOrderDetailed(withOrderId orderId: String) {}
    
    // MARK: - Listeners
    
    // MARK: RIBOrderCreator
    
    func ribOrderCreatorClose() {
        router?.dismissOrderCreator()
    }
    
    // MARK: - Private
    
    private var args: RIBMyOrdersInteractorArgsProtocol
    
    // MARK: - RX
    
    private func subscribe() {
        subscribeOnUserOrdersChanges()
    }
    
    // MARK: Subscriptions
    
    private func subscribeOnUserOrdersChanges() {
        guard let uid = args.auth.currentUser?.uid else {
            return
        }
        args.dataModel
            .user
            .getOrders(uid: uid)
            .subscribe(onNext: { [unowned self] changes in
                self.onNextOrdersChanges(changes)
            })
            .disposeOnDeactivate(interactor: self)
    }
    
    // MARK: Handlers
    
    private func onNextOrdersChanges(_ changes: [Change<Order>]) {
        for change in changes {
            let identifier = change.identifier
            switch change.type {
            case .added:
                guard let item = change.item else { continue }
                args.mutableOrdersStream.append(order: item)
            case .modified:
                guard let item = change.item else { continue }
                args.mutableOrdersStream.update(whereOrderId: identifier, order: item)
            case .removed:
                args.mutableOrdersStream.remove(whereOrderId: identifier)
            }
        }
    }
    
}
