//
//  RIBMyOrdersComponent+RIBUserOrders.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 24.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

/// The dependencies needed from the parent scope of RIBMyOrders to provide for the RIBUserOrders scope.
protocol RIBMyOrdersDependencyRIBUserOrders: Dependency {}

extension RIBMyOrdersComponent: RIBUserOrdersDependency {
    var mutableOrdersStream: MutableOrdersStream {
        mutableUserOrdersStream
    }
}
