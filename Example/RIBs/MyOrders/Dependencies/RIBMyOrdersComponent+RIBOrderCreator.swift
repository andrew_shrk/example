//
//  RIBMyOrdersComponent+RIBOrderCreator.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 27.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

/// The dependencies needed from the parent scope of RIBMyOrders to provide for the RIBOrderCreator scope.
protocol RIBMyOrdersDependencyRIBOrderCreator: Dependency {
    var uiBuilder: UIBuilder { get }
}

extension RIBMyOrdersComponent: RIBOrderCreatorDependency {}
