//
//  RIBMyOrdersViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 24.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol RIBMyOrdersPresentableListener: class {
    func viewControllerDidLoad()
    func didRequestOrderCreate()
}

final class RIBMyOrdersViewController: UISplitViewController, RIBMyOrdersPresentable, RIBMyOrdersViewControllable {

    weak var listener: RIBMyOrdersPresentableListener?
    
    // MARK: - UIViewController override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildTabBarItem(withTitle: "Мои заказы", imageName: "tab_icon_my_orders")
        buildCreateOrderButton()
        listener?.viewControllerDidLoad()
    }
    
    // MARK: - Private
    
    // MARK: - UI
    
    // MARK: Actions
    
    @objc func didTapCreateOrderButton(_ button: UIButton) {
        listener?.didRequestOrderCreate()
    }
    
    // MARK: Builders
    
    private func buildCreateOrderButton() {
        let createOrderButton = UIButton(type: .system)
        createOrderButton.setTitle("Создать заказ", for: .normal)
        createOrderButton.layer.cornerRadius = 24
        createOrderButton.contentEdgeInsets = .init(top: 0, left: 20, bottom: 0, right: 20)
        createOrderButton.backgroundColor = .systemBlue
        createOrderButton.layer.shadowRadius = 10
        createOrderButton.layer.shadowColor = UIColor.black.cgColor
        createOrderButton.layer.shadowOpacity = 0.2
        createOrderButton.layer.shadowOffset = .zero
        createOrderButton.setTitleColor(.white, for: .normal)
        view.addSubview(createOrderButton)
        createOrderButton.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(self.view).inset(20)
            maker.bottom.equalTo(self.view.snp.bottomMargin).inset(40)
            maker.height.equalTo(48)
        }
        createOrderButton.addTarget(self,
                                    action: #selector(didTapCreateOrderButton),
                                    for: .touchUpInside)
    }
    
}
