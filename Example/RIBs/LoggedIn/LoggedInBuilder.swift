//
//  LoggedInBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import FirebaseAuth

protocol LoggedInDependency: LoggedInDependencyMain {
    var loggedInViewController: LoggedInViewControllable { get }
    var auth: Auth { get }
    var appVersion: AppVersionProtocol { get }
    var dataModel: DataModel { get }
    var storage: StorageModel { get }
}

final class LoggedInComponent: Component<LoggedInDependency> {

    fileprivate var loggedInViewController: LoggedInViewControllable {
        return dependency.loggedInViewController
    }
    var auth: Auth {
        dependency.auth
    }
    var appVersion: AppVersionProtocol {
        dependency.appVersion
    }
    var dataModel: DataModel {
        dependency.dataModel
    }
    var storage: StorageModel {
        dependency.storage
    }
    var uiBuilder: UIBuilder {
        dependency.uiBuilder
    }
}

// MARK: - Builder

protocol LoggedInBuildable: Buildable {
    func build(withListener listener: LoggedInListener) -> LoggedInRouting
}

final class LoggedInBuilder: Builder<LoggedInDependency>, LoggedInBuildable {

    override init(dependency: LoggedInDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: LoggedInListener) -> LoggedInRouting {
        let component = LoggedInComponent(dependency: dependency)
        let interactor = LoggedInInteractor()
        interactor.listener = listener
        let mainBuilder = MainBuilder(dependency: component)
        return LoggedInRouter(interactor: interactor,
                              viewController: component.loggedInViewController,
                              mainBuilder: mainBuilder)
    }
}
