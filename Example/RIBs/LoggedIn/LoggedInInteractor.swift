//
//  LoggedInInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift

protocol LoggedInRouting: Routing {
    func cleanupViews()
    // Routing
    func routeToMain()
 }

protocol LoggedInListener: class {}

final class LoggedInInteractor: Interactor, LoggedInInteractable {

    weak var router: LoggedInRouting?
    weak var listener: LoggedInListener?
    
    override init() {}

    override func didBecomeActive() {
        super.didBecomeActive()
        router?.routeToMain()
    }

    override func willResignActive() {
        super.willResignActive()

        router?.cleanupViews()
    }
}
