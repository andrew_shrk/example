//
//  LoggedInComponent+Main.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 12.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

/// The dependencies needed from the parent scope of LoggedIn to provide for the Main scope.
protocol LoggedInDependencyMain: Dependency {
    var uiBuilder: UIBuilder { get }
}

extension LoggedInComponent: MainDependency {}
