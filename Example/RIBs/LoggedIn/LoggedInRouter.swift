//
//  LoggedInRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol LoggedInInteractable: Interactable, MainListener {
    var router: LoggedInRouting? { get set }
    var listener: LoggedInListener? { get set }
}

protocol LoggedInViewControllable: ViewControllable {
    func present(viewController: ViewControllable)
    func dismiss(viewController: ViewControllable)
}

final class LoggedInRouter: Router<LoggedInInteractable>, LoggedInRouting {

    init(interactor: LoggedInInteractable,
         viewController: LoggedInViewControllable,
         mainBuilder: MainBuildable) {
        self.viewController = viewController
        self.mainBuilder = mainBuilder
        super.init(interactor: interactor)
        interactor.router = self
    }
    
    // MARK: - Routing

    func cleanupViews() {
        detachCurrentChild()
    }
    
    func routeToMain() {
        detachCurrentChild()
        attachMain()
    }

    // MARK: - Private

    private let viewController: LoggedInViewControllable
    
    private var currentChild: Routing?
    
    // MARK: Submodules
    
    // MARK: Builders
    
    private let mainBuilder: MainBuildable
    
    // MARK: Attaching
    
    private func attachMain() {
        let main = mainBuilder.build(withListener: interactor)
        self.currentChild = main
        attachChild(main)
        main.viewControllable.uiviewController.modalTransitionStyle = .crossDissolve
        viewController.present(viewController: main.viewControllable)
    }
    
    // MARK: Detaching
    
    private func detachCurrentChild() {
        if let currentChild = currentChild {
            detachChild(currentChild)
        }
        if let currentChildViewable = currentChild as? ViewableRouting {
            viewController.dismiss(viewController: currentChildViewable.viewControllable)
        }
        currentChild = nil
    }
    
}
