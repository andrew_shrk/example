//
//  MainViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 12.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol MainPresentableListener: class {}

typealias MainVC = UITabBarController
    & MainPresentable
    & MainViewControllable

final class MainViewController: MainVC {

    weak var listener: MainPresentableListener?
    
    // MARK: - MainPresentable
    
    func append(viewController: ViewControllable) {
        var viewControllers = self.viewControllers ?? []
        viewControllers.append(viewController.uiviewController)
        self.viewControllers = viewControllers
    }
}
