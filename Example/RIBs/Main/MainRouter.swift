//
//  MainRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 12.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

typealias MainInteractableListeners = RIBMyOrdersListener

protocol MainInteractable: Interactable, MainInteractableListeners {
    var router: MainRouting? { get set }
    var listener: MainListener? { get set }
}

protocol MainViewControllable: ViewControllable {
    func append(viewController: ViewControllable)
}

final class MainRouter: ViewableRouter<MainInteractable, MainViewControllable>, MainRouting {

    init(interactor: MainInteractable,
         viewController: MainViewControllable,
         childrenBuilders: MainChildrenProtocol) {
        self.childrenBuilders = childrenBuilders
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
    
    // MARK: Routing
    
    func attachSubmodules() {
        attachMyOrders()
    }
    
    // MARK: Submodules
    
    private var myOrdersWrapper: ViewControllable?
    
    // MARK: Builders
    
    private let childrenBuilders: MainChildrenProtocol
    
    // MARK: Attaching
    
    private func attachMyOrders() {
        let myOrders = childrenBuilders.userOrders.build(withListener: interactor)
        self.myOrdersWrapper = myOrders.viewControllable
        attachChild(myOrders)
        viewController.append(viewController: myOrders.viewControllable)
    }
}
