//
//  MainComponent+RIBMyOrders.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 24.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

/// The dependencies needed from the parent scope of Main to provide for the RIBMyOrders scope.
protocol MainDependencyRIBMyOrders: Dependency {
    var uiBuilder: UIBuilder { get }
}

extension MainComponent: RIBMyOrdersDependency {}
