//
//  MainBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 12.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import FirebaseAuth

protocol MainDependency: MainDependencyRIBMyOrders {
    var auth: Auth { get }
    var appVersion: AppVersionProtocol { get }
    var dataModel: DataModel { get }
    var storage: StorageModel { get }
}
protocol MainChildrenProtocol {
    var userOrders: RIBMyOrdersBuildable { get }
}

final class MainComponent: Component<MainDependency> {
    
    let viewController: MainViewController
    
    init(dependency: MainDependency, viewController: MainViewController) {
        self.viewController = viewController
        super.init(dependency: dependency)
    }
    
    var auth: Auth {
        dependency.auth
    }
    var appVersion: AppVersionProtocol {
        dependency.appVersion
    }
    var dataModel: DataModel {
        dependency.dataModel
    }
    var storage: StorageModel {
        dependency.storage
    }
    var uiBuilder: UIBuilder {
        dependency.uiBuilder
    }
}

// MARK: - Builder

protocol MainBuildable: Buildable {
    func build(withListener listener: MainListener) -> MainRouting
}

final class MainBuilder: Builder<MainDependency>, MainBuildable {

    override init(dependency: MainDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: MainListener) -> MainRouting {
        let viewController = MainViewController()
        let component = MainComponent(dependency: dependency, viewController: viewController)
        let interactor = MainInteractor(presenter: viewController)
        interactor.listener = listener
        
        let children = MainChildren(userOrders: RIBMyOrdersBuilder(dependency: component))
        
        return MainRouter(interactor: interactor,
                          viewController: viewController,
                          childrenBuilders: children)
    }
}
struct MainChildren: MainChildrenProtocol {
    let userOrders: RIBMyOrdersBuildable
}
