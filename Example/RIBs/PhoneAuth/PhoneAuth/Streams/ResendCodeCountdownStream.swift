//
//  ResendCodeCountdownStream.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 12.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift
import RxCocoa

protocol ResetCodeCountdownStream {
    var value: Observable<Int?> { get }
}

protocol MutableResetCodeCountdownStream: ResetCodeCountdownStream {
    func decrement()
    func reset(with value: Int)
}

class ResetCodeCountdownStreamImpl: MutableResetCodeCountdownStream {
    var value: Observable<Int?> {
        variable.asObservable()
    }
    func decrement() {
        if let value = variable.value {
            let decrementedValue = value - 1
            if decrementedValue < 0 {
                variable.accept(nil)
            } else {
                variable.accept(decrementedValue)
            }
        }
    }
    func reset(with value: Int) {
        variable.accept(value)
    }
    private var variable = BehaviorRelay<Int?>(value: nil)
}
