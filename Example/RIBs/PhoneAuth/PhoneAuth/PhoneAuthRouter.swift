//
//  PhoneAuthRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol PhoneAuthInteractable: Interactable, PhoneAuthPhoneInListener, PhoneAuthCodeInListener {
    var router: PhoneAuthRouting? { get set }
    var listener: PhoneAuthListener? { get set }
}

protocol PhoneAuthViewControllable: ViewControllable {
    func present(viewController: ViewControllable)
    func dismiss(viewController: ViewControllable)
}

final class PhoneAuthRouter: Router<PhoneAuthInteractable>, PhoneAuthRouting {

    init(interactor: PhoneAuthInteractable,
         viewController: PhoneAuthViewControllable,
         phoneInBuilder: PhoneAuthPhoneInBuildable,
         codeInBuilder: PhoneAuthCodeInBuildable) {
        
        self.viewController = viewController
        self.phoneNumberInBuilder = phoneInBuilder
        self.codeInBuilder = codeInBuilder
        
        super.init(interactor: interactor)
        interactor.router = self
    }

    func cleanupViews() {
        detachCurrentChild()
    }
    
    // MARK: - Routing
    func routeToCodeIn(withPhoneNumber phoneNumber: String) {
        if !(currentChild is PhoneAuthCodeInRouter) {
            detachCurrentChild()
            attachCodeIn(phoneNumber: phoneNumber)
        }
    }
    func routeToPhoneIn() {
        detachCurrentChild()
        attachPhoneNumberIn()
    }

    // MARK: - Private

    private let viewController: PhoneAuthViewControllable
    
    private var currentChild: Routing?
    
    // MARK: - Submodules
    
    // MARK: Builders
    
    private let phoneNumberInBuilder: PhoneAuthPhoneInBuildable
    private let codeInBuilder: PhoneAuthCodeInBuildable
    
    // MARK: Attaching
    
    private func attachPhoneNumberIn() {
        let phoneNumberIn = phoneNumberInBuilder.build(withListener: interactor)
        self.currentChild = phoneNumberIn
        attachChild(phoneNumberIn)
        phoneNumberIn.viewControllable.uiviewController.modalPresentationStyle = .fullScreen
        viewController.present(viewController: phoneNumberIn.viewControllable)
    }
    private func attachCodeIn(phoneNumber: String) {
        let codeIn = codeInBuilder.build(withListener: interactor, phoneNumber: phoneNumber)
        self.currentChild = codeIn
        attachChild(codeIn)
        codeIn.viewControllable.uiviewController.modalPresentationStyle = .fullScreen
        viewController.present(viewController: codeIn.viewControllable)
    }
    
    // MARK: Detaching
    
     private func detachCurrentChild() {
         if let currentChild = currentChild {
             detachChild(currentChild)
         }
         if let currentChild = currentChild as? ViewableRouting {
             viewController.dismiss(viewController: currentChild.viewControllable)
         }
         currentChild = nil
     }
    
}
