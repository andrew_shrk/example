//
//  PhoneAuthComponent+PhoneAuthPhoneIn.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

/// The dependencies needed from the parent scope of PhoneAuth to provide for the PhoneAuthPhoneIn scope.
protocol PhoneAuthDependencyPhoneAuthPhoneIn: Dependency {}

extension PhoneAuthComponent: PhoneAuthPhoneInDependency {}
