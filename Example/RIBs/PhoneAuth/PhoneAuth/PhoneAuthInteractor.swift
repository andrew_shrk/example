//
//  PhoneAuthInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import FirebaseAuth

protocol PhoneAuthRouting: Routing {
    func cleanupViews()
    // Routing
    func routeToPhoneIn()
    func routeToCodeIn(withPhoneNumber phoneNumber: String)
}

protocol PhoneAuthListener: class {}

final class PhoneAuthInteractor: Interactor, PhoneAuthInteractable {

    weak var router: PhoneAuthRouting?
    weak var listener: PhoneAuthListener?

    init(auth: Auth,
         phoneAuthProvider: PhoneAuthProvider,
         resetCodeCountdownStream: MutableResetCodeCountdownStream) {
        self.auth = auth
        self.phoneAuthProvider = phoneAuthProvider
        self.resetCodeCountdownStream = resetCodeCountdownStream
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        router?.routeToPhoneIn()
    }

    override func willResignActive() {
        super.willResignActive()

        invalidateCodeResetCountdown()
        
        router?.cleanupViews()
    }
    
    // MARK: - PhoneInListener
    
    func didCompletePhoneInput(withPhoneNumber phoneNumber: String) {
        requestConfirmationCode(with: phoneNumber)
    }
    
    // MARK: - CodeInListener
    
    func didRequestResendConfirmationCode(with phoneNumber: String) {
        requestConfirmationCode(with: phoneNumber)
    }
    func didRequestChangePhoneNumber() {
        router?.routeToPhoneIn()
        invalidateCodeResetCountdown()
    }
    func didCompleteCodeInput(with code: String) {
        if let verificationId = verificationId {
            let credential = phoneAuthProvider.credential(withVerificationID: verificationId,
                                                          verificationCode: code)
            auth.signIn(with: credential) { [weak self] auth, error in
                self?.didSingIn(auth: auth, error: error)
            }
        }
    }
    
    // MARK: - Private
    
    private let phoneAuthProvider: PhoneAuthProvider
    private let auth: Auth
    
    private let resetCodeCountdownStream: MutableResetCodeCountdownStream
    
    private var resetCodeCountdownTimer: Timer?
    
    private var verificationId: String? {
        get { UserDefaults.standard.string(forKey: "authVerificationID") }
        set { UserDefaults.standard.set(newValue, forKey: "authVerificationID") }
    }
    
    // MARK: - Handlers
    
    private func didPhoneAuthVerificationComplete(_ phoneNumber: String,
                                                  _ verificationId: String?,
                                                  _ error: Error?) {
        if let error = error {
            self.showError(withTitle: "Ошибка валидации номера", error: error)
            return
        }
        self.verificationId = verificationId
        self.invalidateCodeResetCountdown()
        self.restartCodeResetCountdown()
        self.router?.routeToCodeIn(withPhoneNumber: phoneNumber)
    }
    private func didSingIn(auth: AuthDataResult?, error: Error?) {
        if let error = error {
            self.showError(withTitle: "Ошибка", error: error)
        } else {
            self.invalidateCodeResetCountdown()
        }
    }
    
    // MARK: - Logic
    
    private func restartCodeResetCountdown() {
        resetCodeCountdownStream.reset(with: 20)
        let timer = Timer.scheduledTimer(
            withTimeInterval: 1.0,
            repeats: true) { [weak self] (_) in
                self?.resetCodeCountdownStream.decrement()
            }
        self.resetCodeCountdownTimer = timer
    }
    
    private func invalidateCodeResetCountdown() {
        self.resetCodeCountdownTimer?.invalidate()
    }
    
    private func requestConfirmationCode(with phoneNumber: String) {
        phoneAuthProvider
            .verifyPhoneNumber(phoneNumber,
                               uiDelegate: nil) { [weak self] (verificationID, error) in
                                self?.didPhoneAuthVerificationComplete(phoneNumber,
                                                                       verificationID, error)
            }
    }
    
    // MARK: - Helpers
    
    private func showError(withTitle title: String, error: Error) {
        UIApplication.shared.showErrorPrompt(withTitle: title, error)
    }
}
