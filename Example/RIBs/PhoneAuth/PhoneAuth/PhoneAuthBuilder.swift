//
//  PhoneAuthBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import FirebaseAuth

protocol PhoneAuthDependency: Dependency {
    var phoneAuthViewController: PhoneAuthViewControllable { get }
    var auth: Auth { get }
}

final class PhoneAuthComponent: Component<PhoneAuthDependency> {

    fileprivate var phoneAuthViewController: PhoneAuthViewControllable {
        dependency.phoneAuthViewController
    }
    fileprivate var auth: Auth {
        dependency.auth
    }
    fileprivate var phoneAuthProvider: PhoneAuthProvider {
        PhoneAuthProvider.provider(auth: auth)
    }
    var mutableResendCodeCountdownStream: MutableResetCodeCountdownStream {
        shared { ResetCodeCountdownStreamImpl() }
    }
    
}

// MARK: - Builder

protocol PhoneAuthBuildable: Buildable {
    func build(withListener listener: PhoneAuthListener) -> PhoneAuthRouting
}

final class PhoneAuthBuilder: Builder<PhoneAuthDependency>, PhoneAuthBuildable {

    override init(dependency: PhoneAuthDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: PhoneAuthListener) -> PhoneAuthRouting {
        let component = PhoneAuthComponent(dependency: dependency)
        
        let interactor =
            PhoneAuthInteractor(auth: component.auth,
                                phoneAuthProvider: component.phoneAuthProvider,
                                resetCodeCountdownStream: component.mutableResendCodeCountdownStream)
        interactor.listener = listener
        
        let phoneInBuilder = PhoneAuthPhoneInBuilder(dependency: component)
        let codeInBuilder = PhoneAuthCodeInBuilder(dependency: component)
        
        return PhoneAuthRouter(interactor: interactor,
                               viewController: component.phoneAuthViewController,
                               phoneInBuilder: phoneInBuilder,
                               codeInBuilder: codeInBuilder)
    }
}
