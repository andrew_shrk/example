//
//  PhoneAuthPhoneInRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol PhoneAuthPhoneInInteractable: Interactable {
    var router: PhoneAuthPhoneInRouting? { get set }
    var listener: PhoneAuthPhoneInListener? { get set }
}

protocol PhoneAuthPhoneInViewControllable: ViewControllable {}

final class PhoneAuthPhoneInRouter: ViewableRouter<PhoneAuthPhoneInInteractable,
PhoneAuthPhoneInViewControllable>,
PhoneAuthPhoneInRouting {

    override init(interactor: PhoneAuthPhoneInInteractable, viewController: PhoneAuthPhoneInViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
