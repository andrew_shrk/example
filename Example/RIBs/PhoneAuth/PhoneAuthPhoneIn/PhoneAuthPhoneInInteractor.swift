//
//  PhoneAuthPhoneInInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift

protocol PhoneAuthPhoneInRouting: ViewableRouting {}

protocol PhoneAuthPhoneInPresentable: Presentable {
    var listener: PhoneAuthPhoneInPresentableListener? { get set }
}

protocol PhoneAuthPhoneInListener: class {
    func didCompletePhoneInput(withPhoneNumber phoneNumber: String)
}

final class PhoneAuthPhoneInInteractor: PresentableInteractor<PhoneAuthPhoneInPresentable>,
PhoneAuthPhoneInInteractable,
PhoneAuthPhoneInPresentableListener {

    weak var router: PhoneAuthPhoneInRouting?
    weak var listener: PhoneAuthPhoneInListener?

    override init(presenter: PhoneAuthPhoneInPresentable) {
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - PhoneAuthPhoneInInteractable
    
    func didCompletePhoneInput(withPhoneNumber phoneNumber: String) {
        listener?.didCompletePhoneInput(withPhoneNumber: phoneNumber)
    }
}
