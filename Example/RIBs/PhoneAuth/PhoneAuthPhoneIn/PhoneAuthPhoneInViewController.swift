//
//  PhoneAuthPhoneInViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import UIKit
import PhoneNumberKit
import InputMask

protocol PhoneAuthPhoneInPresentableListener: class {
    func didCompletePhoneInput(withPhoneNumber phoneNumber: String)
}

final class PhoneAuthPhoneInViewController: UIViewController,
PhoneAuthPhoneInPresentable,
PhoneAuthPhoneInViewControllable {

    weak var listener: PhoneAuthPhoneInPresentableListener?
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupRootView()
        self.linkUi()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    // MARK: - Private
    
    private var titleLabel: UILabel!
    private var phoneNumberIn: UITextField!
    private var nextButton: UIButton!
    
    private var editingListener: MaskedTextInputListener!
    
    // MARK: - UI
    
    // MARK: Builders
    
    private func buildTitleLabel(withPhoneNumberIn phoneNumberIn: UIView) {
        let titleLabel = UILabel()
        self.titleLabel = titleLabel
        if #available(iOS 13.0, *) {
            titleLabel.font = UIFont.preferredFont(forTextStyle: .largeTitle)
        } else {
            titleLabel.font = UIFont.preferredFont(forTextStyle: .title1)
        }
        titleLabel.text = "Ваш телефон"
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (maker) in
            maker.trailing.leading.equalTo(self.view).inset(20)
            maker.bottom.equalTo(phoneNumberIn.snp.top).offset(-20)
        }
    }
    private func buildPhoneInTextField() -> UITextField {
        let phoneInTextField = PhoneNumberTextField()
        phoneInTextField.withExamplePlaceholder = true
        phoneInTextField.withPrefix = true
        phoneInTextField.keyboardType = .phonePad
        phoneInTextField.textAlignment = .left
        phoneInTextField.becomeFirstResponder()
        phoneInTextField.defaultTextAttributes.updateValue(2.0, forKey: .kern)
        if #available(iOS 13.0, *) {
            phoneInTextField.font = UIFont.preferredFont(forTextStyle: .largeTitle)
        } else {
            phoneInTextField.font = UIFont.preferredFont(forTextStyle: .title1)
        }
        view.addSubview(phoneInTextField)
        phoneInTextField.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(self.view).inset(20)
            maker.bottom.equalTo(self.view.snp.centerY)
        }
        self.phoneNumberIn = phoneInTextField
        return phoneInTextField
    }
    private func buildNextButton(withPhoneNumberIn phoneNumberIn: UIView) {
        let nextButton = UIButton(type: .system)
        nextButton.setTitle("Продолжить", for: .normal)
        nextButton.isEnabled = true
        view.addSubview(nextButton)
        nextButton.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(self.view)
            maker.top.equalTo(phoneNumberIn.snp.bottom).offset(20)
        }
        nextButton.addTarget(self, action: #selector(didTapNextButton), for: .touchUpInside)
        self.nextButton = nextButton
    }
    
    // MARK: Actions
    
    @objc private func didTapNextButton() {
        didCompletePhoneInput()
    }
    @objc private func didCompletePhoneInput() {
        if let phoneNumber = phoneNumberIn.text {
            phoneNumberIn.resignFirstResponder()
            listener?.didCompletePhoneInput(withPhoneNumber: phoneNumber)
        }
    }
    
    // MARK: Linkers
    
    private func linkUi() {
        let phoneNumberIn = buildPhoneInTextField()
        buildTitleLabel(withPhoneNumberIn: phoneNumberIn)
        buildNextButton(withPhoneNumberIn: phoneNumberIn)
    }
    
    // MARK: - View setup
    
    private func setupRootView() {
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
    }
    
}
