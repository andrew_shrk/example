//
//  PhoneAuthPhoneInBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol PhoneAuthPhoneInDependency: Dependency {}

final class PhoneAuthPhoneInComponent: Component<PhoneAuthPhoneInDependency> {}

// MARK: - Builder

protocol PhoneAuthPhoneInBuildable: Buildable {
    func build(withListener listener: PhoneAuthPhoneInListener) -> PhoneAuthPhoneInRouting
}

final class PhoneAuthPhoneInBuilder: Builder<PhoneAuthPhoneInDependency>, PhoneAuthPhoneInBuildable {

    override init(dependency: PhoneAuthPhoneInDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: PhoneAuthPhoneInListener) -> PhoneAuthPhoneInRouting {
        _ = PhoneAuthPhoneInComponent(dependency: dependency)
        let viewController = PhoneAuthPhoneInViewController()
        let interactor = PhoneAuthPhoneInInteractor(presenter: viewController)
        interactor.listener = listener
        return PhoneAuthPhoneInRouter(interactor: interactor, viewController: viewController)
    }
}
