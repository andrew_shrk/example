//
//  PhoneAuthCodeInBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol PhoneAuthCodeInDependency: Dependency {
    var resendCodeCountdownStream: ResetCodeCountdownStream { get }
}

final class PhoneAuthCodeInComponent: Component<PhoneAuthCodeInDependency> {
    
    fileprivate let phoneNumber: String
    
    init(dependency: PhoneAuthCodeInDependency, phoneNumber: String) {
        self.phoneNumber = phoneNumber
        super.init(dependency: dependency)
    }
    
    fileprivate var resendCodeCountdownStream: ResetCodeCountdownStream {
        dependency.resendCodeCountdownStream
    }
    
}

// MARK: - Builder

protocol PhoneAuthCodeInBuildable: Buildable {
    func build(withListener listener: PhoneAuthCodeInListener, phoneNumber: String) -> PhoneAuthCodeInRouting
}

final class PhoneAuthCodeInBuilder: Builder<PhoneAuthCodeInDependency>, PhoneAuthCodeInBuildable {

    override init(dependency: PhoneAuthCodeInDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: PhoneAuthCodeInListener, phoneNumber: String) -> PhoneAuthCodeInRouting {
        let component = PhoneAuthCodeInComponent(dependency: dependency, phoneNumber: phoneNumber)
        let viewController =
            PhoneAuthCodeInViewController(phoneNumber: phoneNumber,
                                          resendCodeCountdownStream: component.resendCodeCountdownStream)
        let interactor = PhoneAuthCodeInInteractor(presenter: viewController)
        interactor.listener = listener
        return PhoneAuthCodeInRouter(interactor: interactor, viewController: viewController)
    }
}
