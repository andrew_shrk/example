//
//  PhoneAuthCodeInRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol PhoneAuthCodeInInteractable: Interactable {
    var router: PhoneAuthCodeInRouting? { get set }
    var listener: PhoneAuthCodeInListener? { get set }
}

protocol PhoneAuthCodeInViewControllable: ViewControllable {}

final class PhoneAuthCodeInRouter: ViewableRouter<PhoneAuthCodeInInteractable,
PhoneAuthCodeInViewControllable>,
PhoneAuthCodeInRouting {

    override init(interactor: PhoneAuthCodeInInteractable, viewController: PhoneAuthCodeInViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
