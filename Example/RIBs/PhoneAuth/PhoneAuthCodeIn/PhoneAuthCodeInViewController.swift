//
//  PhoneAuthCodeInViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import UIKit
import InputMask

protocol PhoneAuthCodeInPresentableListener: class {
    func didCompleteCodeInput(withCode confirmationCode: String)
    func didRequestChangePhoneNumber()
    func didRequestResendCode(with phoneNumber: String)
}

final class PhoneAuthCodeInViewController: UIViewController,
PhoneAuthCodeInPresentable,
PhoneAuthCodeInViewControllable {

    weak var listener: PhoneAuthCodeInPresentableListener?
    
    init(phoneNumber: String, resendCodeCountdownStream: ResetCodeCountdownStream) {
        self.phoneNumber = phoneNumber
        self.resendCodeCountdownStream = resendCodeCountdownStream
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.linkUi()
        self.setupRootView()
        self.subscribeOnResetCodeCountdownStream()
    }
    
    // MARK: - Private
    
    private let phoneNumber: String
    
    private let resendCodeCountdownStream: ResetCodeCountdownStream
    
    private var titleLabel: UILabel!
    private var confirmationCodeIn: UITextField!
    private var codeResendLabel: UILabel!
    private var codeResendButton: UIButton!
    
    private var codeEditingListener: MaskedTextInputListener!
    
    private let disposeBag = DisposeBag()
    
    // MARK: - UI
    
    // MARK: State control
    
    private func showResendCodeButton() {
        codeResendLabel.isHidden = true
        codeResendButton.isHidden = false
    }
    
    private func showResendCodeLabel() {
        codeResendLabel.isHidden = false
        codeResendButton.isHidden = true
    }
    
    private func updateResendCodeLabel(with time: Int) {
        let timeString = String(time)
        codeResendLabel.text = "Отправить код повторно через \(timeString)с."
    }
    
    // MARK: Builders
    
    private func buildEditingListener(with codeTextField: UITextField) {
        let editingListener = MaskedTextInputListener()
        editingListener.primaryMaskFormat = "[000000]"
        editingListener.onMaskedTextChangedCallback = { [weak self] _, value, complete in
            self?.didCodeInputValueChanged(value: value, complete: complete)
        }
        codeTextField.delegate = editingListener
        self.codeEditingListener = editingListener
    }
    private func buildTitleLabel(withChangeNumberButton changeNumberButton: UIButton) {
        let titleLabel = UILabel()
        self.titleLabel = titleLabel
        titleLabel.font = UIFont.preferredFont(forTextStyle: .callout)
        titleLabel.text = "Укажите 6-ти значный код отправленный на номер:"
        titleLabel.numberOfLines = 0
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (maker) in
            maker.trailing.leading.equalTo(self.view).inset(20)
            maker.bottom.equalTo(changeNumberButton.snp.top)
        }
    }
    private func buildChangeNumberButton(codeInTextFiled codeIn: UITextField, phoneNumber: String) -> UIButton {
        let changeNumberButton = UIButton(type: .system)
        changeNumberButton.setTitle(phoneNumber, for: .normal)
        view.addSubview(changeNumberButton)
        changeNumberButton.snp.makeConstraints { (maker) in
            maker.leading.equalTo(view).inset(20)
            maker.bottom.equalTo(codeIn.snp.top).offset(-10)
        }
        changeNumberButton.addTarget(self,
                                     action: #selector(didTapChangeNumberButton),
                                     for: .touchUpInside)
        return changeNumberButton
    }
    private func buildCodeInTextField() -> UITextField {
        let confirmationCodeIn = UITextField()
        let placeholder = NSMutableAttributedString(string: "••••••")
        placeholder.addAttribute(.kern,
                                 value: 16.0,
                                 range: NSRange(location: 0, length: placeholder.length))
        confirmationCodeIn.attributedPlaceholder = placeholder
        confirmationCodeIn.keyboardType = .numberPad
        confirmationCodeIn.becomeFirstResponder()
        confirmationCodeIn.textAlignment = .center
        confirmationCodeIn
            .defaultTextAttributes
            .updateValue(16.0, forKey: .kern)
        if #available(iOS 13.0, *) {
            confirmationCodeIn.font = UIFont.preferredFont(forTextStyle: .largeTitle)
        } else {
            confirmationCodeIn.font = UIFont.preferredFont(forTextStyle: .title1)
        }
        view.addSubview(confirmationCodeIn)
        confirmationCodeIn.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(self.view).inset(20)
            maker.bottom.equalTo(self.view.snp.centerY)
        }
        self.confirmationCodeIn = confirmationCodeIn
        return confirmationCodeIn
    }
    private func buildCodeResendButton(withConfirmationCode confirmationCode: UITextField) -> UIButton {
        let codeResendButton = UIButton(type: .system)
        codeResendButton.setTitle("Отпаравить повторно", for: .normal)
        view.addSubview(codeResendButton)
        codeResendButton.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(self.view)
            maker.top.equalTo(confirmationCode.snp.bottom).offset(20)
        }
        codeResendButton.addTarget(self, action: #selector(didTapCodeResetButton), for: .touchUpInside)
        self.codeResendButton = codeResendButton
        return codeResendButton
    }
    private func buildCodeResendLabel(with codeResendButton: UIButton) {
        let codeResendLabel = UILabel()
        codeResendLabel.font = UIFont.preferredFont(forTextStyle: .footnote)
        view.addSubview(codeResendLabel)
        codeResendLabel.snp.makeConstraints { (maker) in
            maker.centerX.centerY.equalTo(codeResendButton)
        }
        self.codeResendLabel = codeResendLabel
    }
    
    // MARK: Actions
    
    @objc private func didTapCodeResetButton() {
        listener?.didRequestResendCode(with: phoneNumber)
    }
    
    @objc private func didTapNextButton() {
        if let confirmationCode = confirmationCodeIn.text {
            listener?.didCompleteCodeInput(withCode: confirmationCode)
        }
    }
    @objc private func didTapChangeNumberButton() {
        listener?.didRequestChangePhoneNumber()
    }
    
    @objc private func didCodeInputValueChanged(value: String, complete: Bool) {
        if complete {
            listener?.didCompleteCodeInput(withCode: value)
        }
    }
    
    // MARK: Linkers
    
    private func linkUi() {
        let confirmationCodeIn = buildCodeInTextField()
        let changeNumberButton = buildChangeNumberButton(codeInTextFiled: confirmationCodeIn,
                                                         phoneNumber: phoneNumber)
        let codeResendButton = buildCodeResendButton(withConfirmationCode: confirmationCodeIn)
        buildTitleLabel(withChangeNumberButton: changeNumberButton)
        buildCodeResendLabel(with: codeResendButton)
        buildEditingListener(with: confirmationCodeIn)
    }
    
    // MARK: - View setup
    
    private func setupRootView() {
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
    }
    
    // MARK: - RX
    
    // MARK: Subscriptions
    
    private func subscribeOnResetCodeCountdownStream() {
        resendCodeCountdownStream
            .value
            .subscribe(onNext: { [weak self]value in
                self?.onNextResetCodeCountdown(value)
            })
            .disposed(by: disposeBag)
    }
    
    // MARK: Handlers
    
    private func onNextResetCodeCountdown(_ value: Int?) {
        if let value = value {
            showResendCodeLabel()
            updateResendCodeLabel(with: value)
        } else {
            showResendCodeButton()
        }
    }
}
