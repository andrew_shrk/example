//
//  PhoneAuthCodeInInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift

protocol PhoneAuthCodeInRouting: ViewableRouting {}

protocol PhoneAuthCodeInPresentable: Presentable {
    var listener: PhoneAuthCodeInPresentableListener? { get set }
}

protocol PhoneAuthCodeInListener: class {
    func didRequestResendConfirmationCode(with phoneNumber: String)
    func didCompleteCodeInput(with code: String)
    func didRequestChangePhoneNumber()
}

final class PhoneAuthCodeInInteractor: PresentableInteractor<PhoneAuthCodeInPresentable>,
PhoneAuthCodeInInteractable,
PhoneAuthCodeInPresentableListener {

    weak var router: PhoneAuthCodeInRouting?
    weak var listener: PhoneAuthCodeInListener?
    
    override init(presenter: PhoneAuthCodeInPresentable) {
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - PhoneAuthCodeInInteractable
    
    func didCompleteCodeInput(withCode confirmationCode: String) {
        listener?.didCompleteCodeInput(with: confirmationCode)
    }
    func didRequestChangePhoneNumber() {
        listener?.didRequestChangePhoneNumber()
    }
    func didRequestResendCode(with phoneNumber: String) {
        listener?.didRequestResendConfirmationCode(with: phoneNumber)
    }
}
