//
//  LoggedOutComponent+PhoneAuth.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

/// The dependencies needed from the parent scope of LoggedOut to provide for the PhoneAuth scope.
protocol LoggedOutDependencyPhoneAuth: Dependency {}

extension LoggedOutComponent: PhoneAuthDependency {
    var phoneAuthViewController: PhoneAuthViewControllable {
        loggedOutViewController
    }
}
