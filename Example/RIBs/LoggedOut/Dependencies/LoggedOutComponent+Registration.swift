//
//  LoggedOutComponent+Registration.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

/// The dependencies needed from the parent scope of LoggedOut to provide for the Registration scope.
protocol LoggedOutDependencyRegistration: Dependency {}

extension LoggedOutComponent: RegistrationDependency {
    var registrationViewController: RegistrationViewControllable {
        self.loggedOutViewController
    }
}
