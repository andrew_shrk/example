//
//  LoggedOutRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol LoggedOutInteractable: Interactable, RegistrationListener, PhoneAuthListener {
    var router: LoggedOutRouting? { get set }
    var listener: LoggedOutListener? { get set }
}

protocol LoggedOutViewControllable: RegistrationViewControllable, PhoneAuthViewControllable {
    func present(viewController: ViewControllable)
    func dismiss(viewController: ViewControllable)
}

final class LoggedOutRouter: Router<LoggedOutInteractable>, LoggedOutRouting {

    init(interactor: LoggedOutInteractable,
         viewController: LoggedOutViewControllable,
         registrationBuilder: RegistrationBuildable,
         phoneAuthBuilder: PhoneAuthBuildable) {
        
        self.viewController = viewController
        
        self.registrationBuilder = registrationBuilder
        self.phoneAuthBuilder = phoneAuthBuilder
        
        super.init(interactor: interactor)
        interactor.router = self
    }

    func cleanupViews() {
        detachCurrentChild()
    }

    // MARK: - Private

    private let viewController: LoggedOutViewControllable
    
    private var currentChild: Routing?
    
    // MARK: - Routing
    
    func routeToRegister() {
        detachCurrentChild()
        attachRegistration()
    }
    func routeToAuth() {
        detachCurrentChild()
        attachPhoneAuth()
    }
    
    // MARK: - Submodules
    
    // MARK: Builders
    
    private let registrationBuilder: RegistrationBuildable
    private let phoneAuthBuilder: PhoneAuthBuildable
    
    // MARK: Attaching
    
    private func attachRegistration() {
        let registration = registrationBuilder.build(withListener: interactor)
        self.currentChild = registration
        attachChild(registration)
    }
    private func attachPhoneAuth() {
        let phoneAuth = phoneAuthBuilder.build(withListener: interactor)
        self.currentChild = phoneAuth
        attachChild(phoneAuth)
    }
    
    // MARK: Detaching
    
    private func detachCurrentChild() {
        if let currentChild = currentChild {
            detachChild(currentChild)
        }
        if let currentChildViewable = currentChild as? ViewableRouting {
            viewController.dismiss(viewController: currentChildViewable.viewControllable)
        }
        currentChild = nil
    }
    
}
