//
//  LoggedOutInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift

protocol LoggedOutRouting: Routing {
    func cleanupViews()
    // Routing
    func routeToAuth()
    func routeToRegister()
}

protocol LoggedOutListener: class {}

final class LoggedOutInteractor: Interactor, LoggedOutInteractable {

    weak var router: LoggedOutRouting?
    weak var listener: LoggedOutListener?

    init(authStateStream: AuthStateStream) {
        self.authStateStream = authStateStream
        super.init()
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        self.subscribeOnAuthStateStream()
    }

    override func willResignActive() {
        super.willResignActive()

        router?.cleanupViews()
    }
    
    // MARK: - Private
    
    private let authStateStream: AuthStateStream
    
    // MARK: - RX
    
    // MARK: Subscriptions
    
    private func subscribeOnAuthStateStream() {
        authStateStream
            .authState
            .subscribe(onNext: {[weak self] authState in
                self?.onNextAuthState(authState)
            })
            .disposeOnDeactivate(interactor: self)
    }
    
    // MARK: Handlers
    
    private func onNextAuthState(_ authState: AuthState) {
        switch authState {
        case .loggedOut:
            router?.routeToAuth()
        case .loggedInWithoutRegistration:
            router?.routeToRegister()
        default:
            break
        }
    }
    
}
