//
//  LoggedOutBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import FirebaseAuth

protocol LoggedOutDependency: Dependency {
    var loggedOutViewController: LoggedOutViewControllable { get }
    var mutableAuthStateStream: MutableAuthStateStream { get }
    var auth: Auth { get }
    var appVersion: AppVersionProtocol { get }
}

final class LoggedOutComponent: Component<LoggedOutDependency> {

    var loggedOutViewController: LoggedOutViewControllable {
        dependency.loggedOutViewController
    }
    var mutableAuthStateStream: MutableAuthStateStream {
        dependency.mutableAuthStateStream
    }
    var auth: Auth {
        dependency.auth
    }
    var appVersion: AppVersionProtocol {
        dependency.appVersion
    }
}

// MARK: - Builder

protocol LoggedOutBuildable: Buildable {
    func build(withListener listener: LoggedOutListener) -> LoggedOutRouting
}

final class LoggedOutBuilder: Builder<LoggedOutDependency>, LoggedOutBuildable {

    override init(dependency: LoggedOutDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: LoggedOutListener) -> LoggedOutRouting {
        let component = LoggedOutComponent(dependency: dependency)
        
        let interactor = LoggedOutInteractor(authStateStream: component.mutableAuthStateStream)
        interactor.listener = listener
        
        let phoneAuthBuilder = PhoneAuthBuilder(dependency: component)
        let registrationBuilder = RegistrationBuilder(dependency: component)
        
        return LoggedOutRouter(interactor: interactor,
                               viewController: component.loggedOutViewController,
                               registrationBuilder: registrationBuilder,
                               phoneAuthBuilder: phoneAuthBuilder)
    }
}
