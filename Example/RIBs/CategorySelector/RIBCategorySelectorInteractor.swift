//
//  RIBCategorySelectorInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 28.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift

protocol RIBCategorySelectorRouting: ViewableRouting {}

protocol RIBCategorySelectorPresentable: Presentable {
    var listener: RIBCategorySelectorPresentableListener? { get set }
    func cellForRow(at indexPath: IndexPath, with title: String?) -> UITableViewCell
    func reloadData()
}

protocol RIBCategorySelectorListener: class {
    func didSelectSpecialization(_ specialization: Specialization, _ specializationGroup: SpecializationGroup)
    func categorySelectorDidEndSelection()
}

typealias RIBCategorySelectorInteractorType = PresentableInteractor<RIBCategorySelectorPresentable>
    & RIBCategorySelectorInteractable
    & RIBCategorySelectorPresentableListener
    & RIBCategorySelectorDataSourceDelegate

final class RIBCategorySelectorInteractor: RIBCategorySelectorInteractorType {

    weak var router: RIBCategorySelectorRouting?
    weak var listener: RIBCategorySelectorListener?

    init(presenter: RIBCategorySelectorPresentable,
         args: RIBCategorySelectorInteractorArgsProtocol) {
        self.args = args
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
        args.dataModel
            .specializationGroups.first()
            .subscribe(onSuccess: { [weak self] value in
                self?.specializationGroups = value ?? []
                self?.presenter.reloadData()
            }, onError: nil)
            .disposeOnDeactivate(interactor: self)
        args.dataModel
            .specializations.first()
            .subscribe(onSuccess: { [weak self] value in
                self?.specializations = value ?? []
                self?.presenter.reloadData()
            }, onError: nil)
            .disposeOnDeactivate(interactor: self)
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - PresentableListener
    
    func didSelectRow(at indexPath: IndexPath) {
        let section = indexPath.section
        let row = indexPath.row
        let specializationGroup = specializationGroups[section]
        if let specializationGroupId = specializationGroup.identifier {
            let specialization = specializations.filter { $0.groupId == specializationGroupId }[row]
            listener?.didSelectSpecialization(specialization, specializationGroup)
        }
    }
    func didTapCloseButton() {
        listener?.categorySelectorDidEndSelection()
    }
    
    // MARK: - RIBCategorySelectorDataSourceDelegate
    
    func dataSource(numberOfRowsInSection section: Int) -> Int {
        if let specializationGroupId = specializationGroups[section].identifier {
            return specializations.filter { $0.groupId == specializationGroupId }.count
        } else {
            return 0
        }
    }
    func dataSource(cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        let row = indexPath.row
        if let specializationGroupId = specializationGroups[section].identifier {
            let title = specializations.filter { $0.groupId == specializationGroupId }[row].name
            return presenter.cellForRow(at: indexPath, with: title)
        } else {
            return presenter.cellForRow(at: indexPath, with: nil)
        }
    }
    func dataSourceNumberOfSections() -> Int {
        return specializationGroups.count
    }
    func dataSource(titleForHeaderInSection section: Int) -> String? {
        return specializationGroups[section].name
    }
    
    // MARK: - Private
    
    private let args: RIBCategorySelectorInteractorArgsProtocol
    
    private var specializations: [Specialization] = []
    private var specializationGroups: [SpecializationGroup] = []
    
}
