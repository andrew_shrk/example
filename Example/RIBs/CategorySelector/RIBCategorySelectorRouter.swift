//
//  RIBCategorySelectorRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 28.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RIBCategorySelectorInteractable: Interactable {
    var router: RIBCategorySelectorRouting? { get set }
    var listener: RIBCategorySelectorListener? { get set }
}

protocol RIBCategorySelectorViewControllable: ViewControllable {}

typealias RIBCategorySelectorRouterType =
    ViewableRouter<RIBCategorySelectorInteractable, RIBCategorySelectorViewControllable>
    & RIBCategorySelectorRouting

final class RIBCategorySelectorRouter: RIBCategorySelectorRouterType {

    override init(interactor: RIBCategorySelectorInteractable, viewController: RIBCategorySelectorViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
