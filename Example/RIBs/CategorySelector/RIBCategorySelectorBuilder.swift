//
//  RIBCategorySelectorBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 28.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RIBCategorySelectorDependency: Dependency {
    var dataModel: DataModel { get }
}
protocol RIBCategorySelectorInteractorArgsProtocol {
    var dataModel: DataModel { get }
}
protocol RIBCategorySelectorPresenterArgsProtocol {
    var dataSource: RIBCategorySelectorDataSource { get }
}

final class RIBCategorySelectorComponent: Component<RIBCategorySelectorDependency> {
    fileprivate var dataModel: DataModel {
        dependency.dataModel
    }
    fileprivate lazy var dataSource: RIBCategorySelectorDataSource = {
        RIBCategorySelectorDataSource()
    }()
}

// MARK: - Builder

protocol RIBCategorySelectorBuildable: Buildable {
    func build(withListener listener: RIBCategorySelectorListener) -> RIBCategorySelectorRouting
}

final class RIBCategorySelectorBuilder: Builder<RIBCategorySelectorDependency>, RIBCategorySelectorBuildable {

    override init(dependency: RIBCategorySelectorDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: RIBCategorySelectorListener) -> RIBCategorySelectorRouting {
        let component = RIBCategorySelectorComponent(dependency: dependency)
        
        let presenterArgs = RIBCategorySelectorPresenterArgs(dataSource: component.dataSource)
        
        let viewController = RIBCategorySelectorViewController(style: .insetGroupedIfAvailable, args: presenterArgs)
        viewController.tableView.dataSource = component.dataSource
        
        let interactorArgs = RIBCategorySelectorInteractorArgs(dataModel: component.dataModel)
        let interactor = RIBCategorySelectorInteractor(presenter: viewController, args: interactorArgs)
        component.dataSource.delegate = interactor
        
        interactor.listener = listener
        return RIBCategorySelectorRouter(interactor: interactor, viewController: viewController)
    }
}

struct RIBCategorySelectorInteractorArgs: RIBCategorySelectorInteractorArgsProtocol {
    var dataModel: DataModel
}
struct RIBCategorySelectorPresenterArgs: RIBCategorySelectorPresenterArgsProtocol {
    var dataSource: RIBCategorySelectorDataSource
}

class RIBCategorySelectorDataSource: NSObject, UITableViewDataSource {
    
    weak var delegate: RIBCategorySelectorDataSourceDelegate?
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return delegate?.dataSourceNumberOfSections() ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return delegate?.dataSource(numberOfRowsInSection: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return delegate?.dataSource(cellForRowAt: indexPath) ?? UITableViewCell()
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return delegate?.dataSource(titleForHeaderInSection: section)
    }
}

protocol RIBCategorySelectorDataSourceDelegate: class {
    func dataSource(numberOfRowsInSection section: Int) -> Int
    func dataSource(cellForRowAt indexPath: IndexPath) -> UITableViewCell
    func dataSource(titleForHeaderInSection section: Int) -> String?
    func dataSourceNumberOfSections() -> Int
}
