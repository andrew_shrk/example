//
//  RIBCategorySelectorViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 28.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol RIBCategorySelectorPresentableListener: class {
    func didSelectRow(at indexPath: IndexPath)
    func didTapCloseButton()
}

typealias RIBCategorySelectorVC = UITableViewController
    & RIBCategorySelectorPresentable
    & RIBCategorySelectorViewControllable

final class RIBCategorySelectorViewController: RIBCategorySelectorVC {

    weak var listener: RIBCategorySelectorPresentableListener?
    
    init(style: UITableView.Style, args: RIBCategorySelectorPresenterArgsProtocol) {
        self.args = args
        super.init(style: style)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UIViewController override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: reusableIdentifier)
        title = "Специализация"
        link()
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        listener?.didSelectRow(at: indexPath)
    }
    
    // MARK: - Presentable
    
    func cellForRow(at indexPath: IndexPath, with title: String?) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reusableIdentifier, for: indexPath)
        cell.textLabel?.text = title
        return cell
    }
    func reloadData() {
        tableView.reloadData()
    }
    
    // MARK: - Private
    
    private let reusableIdentifier = "category_cell"
    
    private let args: RIBCategorySelectorPresenterArgsProtocol
    
    // MARK: - UI
    
    // MARK: Actions
    
    @objc func didTapDoneButton() {
        listener?.didTapCloseButton()
    }
    
    // MARK: Builders
    
    private lazy var doneButton: UIBarButtonItem = {
        var item: UIBarButtonItem.SystemItem = {
            if #available(iOS 13.0, *) {
                return .close
            } else {
                return .done
            }
        }()
        let doneButton = UIBarButtonItem(barButtonSystemItem: item,
                                         target: self,
                                         action: #selector(didTapDoneButton))
        return doneButton
    }()
    
    // MARK: Linking
    
    private func link() {
        navigationItem.rightBarButtonItem = doneButton
    }
    
}
