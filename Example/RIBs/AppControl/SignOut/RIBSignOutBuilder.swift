//
//  RIBSignOutBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 18.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import FirebaseAuth

protocol RIBSignOutDependency: Dependency {
    var auth: Auth { get }
}

final class RIBSignOutComponent: Component<RIBSignOutDependency> {
    fileprivate var auth: Auth {
        dependency.auth
    }
}

// MARK: - Builder

protocol RIBSignOutBuildable: Buildable {
    func build(withListener listener: RIBSignOutListener,
               viewType: ViewType) -> RIBSignOutRouting
}

final class RIBSignOutBuilder: Builder<RIBSignOutDependency>, RIBSignOutBuildable {

    override init(dependency: RIBSignOutDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: RIBSignOutListener,
               viewType: ViewType) -> RIBSignOutRouting {
        
        let component = RIBSignOutComponent(dependency: dependency)
        let viewController: RIBSignOutVC = {
            switch viewType {
            case .cell:
                return RIBSignOutCellViewController()
            default:
                fatalError("View type \(viewType.rawValue) not allow for Rating")
            }
        }()
        let interactor = RIBSignOutInteractor(presenter: viewController,
                                              auth: component.auth)
        interactor.listener = listener
        return RIBSignOutRouter(interactor: interactor, viewController: viewController)
    }
}
