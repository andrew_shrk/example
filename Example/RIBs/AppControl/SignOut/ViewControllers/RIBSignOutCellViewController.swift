//
//  RIBSignOutCellViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 18.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit

final class RIBSignOutCellViewController: UIViewController,
RIBSignOutVC,
CellViewController {
   
    weak var listener: RIBSignOutPresentableListener?
    
    // MARK: UIViewController override
    
    override func loadView() {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "sign_out_cell")
        cell.isUserInteractionEnabled = true
        view = cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        link()
    }
    
    // MARK: - Private
    
    // MARK: - UI
    
    // MARK: Actions
    
    @objc func didTapOnCell(_ sender: UITapGestureRecognizer) {
        listener?.didRequestSingOut()
    }
    
    // MARK: Builders
    
    private func buildCell() {
        viewCell.textLabel?.text = "Выход"
        viewCell.textLabel?.textAlignment = .center
        viewCell.textLabel?.textColor = .systemRed
    }
    private func buildGestureRecognizer() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self,
                                                          action: #selector(didTapOnCell))
        tapGestureRecognizer.cancelsTouchesInView = false
        tapGestureRecognizer.isEnabled = true
        viewCell.addGestureRecognizer(tapGestureRecognizer)
    }
    
    // MARK: Linking
    
    private func link() {
        buildCell()
        buildGestureRecognizer()
    }
    
}
