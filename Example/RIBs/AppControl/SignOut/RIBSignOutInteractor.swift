//
//  RIBSignOutInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 18.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import FirebaseAuth

protocol RIBSignOutRouting: ViewableRouting {}

protocol RIBSignOutPresentable: Presentable {
    var listener: RIBSignOutPresentableListener? { get set }
    func showErrorPrompt(withTitle title: String?, error: Error)
}

protocol RIBSignOutListener: class {}

final class RIBSignOutInteractor: PresentableInteractor<RIBSignOutPresentable>,
RIBSignOutInteractable,
RIBSignOutPresentableListener {

    weak var router: RIBSignOutRouting?
    weak var listener: RIBSignOutListener?

    init(presenter: RIBSignOutPresentable, auth: Auth) {
        self.auth = auth
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - Presentable
    
    func didRequestSingOut() {
        do {
            try auth.signOut()
        } catch {
            presenter.showErrorPrompt(withTitle: nil, error: error)
        }
    }
    
    // MARK: - Private
    
    private let auth: Auth
    
}
