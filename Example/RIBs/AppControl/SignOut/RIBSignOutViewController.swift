//
//  RIBSignOutViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 18.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol RIBSignOutPresentableListener: class {
    func didRequestSingOut()
}

typealias RIBSignOutVC = RIBSignOutPresentable & RIBSignOutViewControllable

final class RIBSignOutViewController: UIViewController, RIBSignOutVC {
    weak var listener: RIBSignOutPresentableListener?
}
