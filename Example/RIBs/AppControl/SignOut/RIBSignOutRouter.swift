//
//  RIBSignOutRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 18.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RIBSignOutInteractable: Interactable {
    var router: RIBSignOutRouting? { get set }
    var listener: RIBSignOutListener? { get set }
}

protocol RIBSignOutViewControllable: ViewControllable {}

final class RIBSignOutRouter: ViewableRouter<RIBSignOutInteractable, RIBSignOutViewControllable>, RIBSignOutRouting {

    override init(interactor: RIBSignOutInteractable, viewController: RIBSignOutViewControllable) {
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
}
