//
//  RIBAboutAppComponent+RIBAboutApp.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 19.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

/// The dependencies needed from the parent scope of RIBAboutApp to provide for the RIBAboutApp scope.
protocol RIBAboutAppDependencyRIBAboutApp: Dependency {}

extension RIBAboutAppComponent: RIBAboutAppDependency {}
