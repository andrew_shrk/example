//
//  RIBAboutAppFullScreenViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 19.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit

final class RIBAboutAppFullScreenViewController: RIBAboutAppVC, UIAdaptivePresentationControllerDelegate {

    weak var listener: RIBAboutAppPresentableListener?
    
    init(appVersion: AppVersionProtocol) {
        self.appVersion = appVersion
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Presentable
    
    func resetCellSelection() {}
    
    // MARK: - UIViewController override
    
    override func loadView() {
        let view = UIView()
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
        self.view = view
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "О приложении"
        navigationController?.presentationController?.delegate = self
        link()
    }
    
    // MARK: - UIAdaptivePresentationControllerDelegate
    
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        listener?.didRequestCloseFullScreenView()
    }
    
    // MARK: - Private
    
    private let appVersion: AppVersionProtocol
    
    // MARK: - UI
    
    // MARK: Actions
    
    @objc func didTapCloseButton() {
        listener?.didRequestCloseFullScreenView()
    }
    @objc func didTapLicenseAgreement() {
        listener?.didRequestShowAgreements()
    }
    
    // MARK: Builders
    
    private func buildCloseButton() {
        let buttonItem: UIBarButtonItem.SystemItem = {
            if #available(iOS 13.0, *) {
                return .close
            } else {
                return .done
            }
        }()
        let closeButton = UIBarButtonItem(barButtonSystemItem: buttonItem,
                                           target: self,
                                           action: #selector(didTapCloseButton))
        navigationItem.rightBarButtonItem = closeButton
    }
    private func buildLogoImageView() -> UIImageView {
        let logoImageView = UIImageView()
        logoImageView.image = UIImage(named: "app_logo")
        logoImageView.contentMode = .scaleAspectFit
        view.addSubview(logoImageView)
        logoImageView.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(self.view.snp.centerY)
            maker.leading.trailing.equalTo(self.view).inset(20)
        }
        return logoImageView
    }
    private func buildAppVersionLabel(withLogoImageView logoImageView: UIImageView) {
        let appVersionLabel = UILabel()
        appVersionLabel.font = .preferredFont(forTextStyle: .subheadline)
        appVersionLabel.text = "Версия \(appVersion.versionNumber). Сборка \(appVersion.buildNumber)."
        view.addSubview(appVersionLabel)
        appVersionLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(logoImageView.snp.bottom).offset(10)
            maker.centerX.equalTo(self.view)
        }
    }
    private func buildLicenseAgreementButton() {
        let licenseAgreementButton = UIButton(type: .system)
        licenseAgreementButton.setTitle("Лицензионное соглашение", for: .normal)
        view.addSubview(licenseAgreementButton)
        licenseAgreementButton.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(self.view).inset(40)
            maker.centerX.equalTo(self.view)
        }
        licenseAgreementButton.addTarget(self,
                                         action: #selector(didTapLicenseAgreement),
                                         for: .touchUpInside)
    }
    
    // MARK: Linking
    
    private func link() {
        let logoImageView = buildLogoImageView()
        buildAppVersionLabel(withLogoImageView: logoImageView)
        buildCloseButton()
        buildLicenseAgreementButton()
    }
    
}
