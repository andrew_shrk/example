//
//  RIBAboutAppCellViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 19.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit

final class RIBAboutAppCellViewController: RIBAboutAppCellVC {

    weak var listener: RIBAboutAppPresentableListener?
    
    // MARK: UIViewController override
    
    override func loadView() {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "about_app_cell")
        view = cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        link()
    }
    
    // MARK: - Presentable
    
    func resetCellSelection() {
        viewCell.isSelected = false
    }
    
    // MARK: - UI
    
    // MARK: Actions
    
    @objc func didTapOnCell() {
        listener?.didRequestFullScreenView()
    }
    
    // MARK: Builders
    
    private func buildCell() {
        viewCell.textLabel?.text = "О приложении"
        viewCell.accessoryType = .disclosureIndicator
        viewCell.imageView?.image = UIImage(named: "icon_about_app_24dp")
    }
    private func buildGestureRecognizer() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self,
                                                          action: #selector(didTapOnCell))
        tapGestureRecognizer.cancelsTouchesInView = false
        viewCell.addGestureRecognizer(tapGestureRecognizer)
    }
    
    // MARK: Linking
    
    func link() {
        buildCell()
        buildGestureRecognizer()
    }
    
}
