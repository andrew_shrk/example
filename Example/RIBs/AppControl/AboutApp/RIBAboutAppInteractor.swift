//
//  RIBAboutAppInteractor.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 19.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift

protocol RIBAboutAppRouting: ViewableRouting {
    func presentFullScreenAppearance()
    func closeFullScreenAppearance()
}

protocol RIBAboutAppPresentable: Presentable {
    var listener: RIBAboutAppPresentableListener? { get set }
    func resetCellSelection()
}

protocol RIBAboutAppListener: class {
    func didRequestCloseFullScreenAppearance()
}

typealias RIBAboutAppInteractorProtocol = PresentableInteractor<RIBAboutAppPresentable>
    & RIBAboutAppInteractable
    & RIBAboutAppPresentableListener

final class RIBAboutAppInteractor: RIBAboutAppInteractorProtocol {

    weak var router: RIBAboutAppRouting?
    weak var listener: RIBAboutAppListener?

    init(presenter: RIBAboutAppPresentable, licenseAgreementUrl: URL) {
        self.licenseAgreementUrl = licenseAgreementUrl
        super.init(presenter: presenter)
        presenter.listener = self
    }

    override func didBecomeActive() {
        super.didBecomeActive()
    }

    override func willResignActive() {
        super.willResignActive()
    }
    
    // MARK: - Presentable
    
    func didRequestFullScreenView() {
        router?.presentFullScreenAppearance()
    }
    func didRequestCloseFullScreenView() {
        listener?.didRequestCloseFullScreenAppearance()
    }
    func didRequestShowAgreements() {
        openUrl(licenseAgreementUrl)
    }
    func movingFromParent() {
        listener?.didRequestCloseFullScreenAppearance()
    }
    
    // MARK: RIBAboutAppListener
    
    func didRequestCloseFullScreenAppearance() {
        router?.closeFullScreenAppearance()
        presenter.resetCellSelection()
    }
    
    // MARK: - Private
    
    private let licenseAgreementUrl: URL
    
    // MARK: - Helpers
    
    private func openUrl(_ url: URL) {
        UIApplication.shared.open(url)
    }
    
}
