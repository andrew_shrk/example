//
//  RIBAboutAppBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 19.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RIBAboutAppDependency: Dependency {
    var appVersion: AppVersionProtocol { get }
}

final class RIBAboutAppComponent: Component<RIBAboutAppDependency> {
    var appVersion: AppVersionProtocol {
        dependency.appVersion
    }
    fileprivate var licenseAgreementUrl: URL {
        shared {
            let string = "https://www.apple.com/"
            guard let url = URL(string: "\(string)") else {
                preconditionFailure("Invalid static URL string: \(string)")
            }
            return url
        }
    }
}

// MARK: - Builder

protocol RIBAboutAppBuildable: Buildable {
    func build(withListener listener: RIBAboutAppListener,
               viewType: ViewType) -> RIBAboutAppRouting
}

final class RIBAboutAppBuilder: Builder<RIBAboutAppDependency>, RIBAboutAppBuildable {

    override init(dependency: RIBAboutAppDependency) {
        super.init(dependency: dependency)
    }

    func build(withListener listener: RIBAboutAppListener,
               viewType: ViewType) -> RIBAboutAppRouting {
        let component = RIBAboutAppComponent(dependency: dependency)
        
        let viewController: RIBAboutAppVC = {
            switch viewType {
            case .cell:
                return RIBAboutAppCellViewController()
            case .fullScreen:
                return RIBAboutAppFullScreenViewController(appVersion: component.appVersion)
            }
        }()
        
        let interactor = RIBAboutAppInteractor(presenter: viewController,
                                               licenseAgreementUrl: component.licenseAgreementUrl)
        interactor.listener = listener
        
        let aboutAppBuilder = RIBAboutAppBuilder(dependency: component)
        
        return RIBAboutAppRouter(interactor: interactor,
                                 viewController: viewController,
                                 aboutAppBuilder: aboutAppBuilder)
    }
}
