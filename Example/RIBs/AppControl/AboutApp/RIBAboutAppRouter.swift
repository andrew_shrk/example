//
//  RIBAboutAppRouter.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 19.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

protocol RIBAboutAppInteractable: Interactable, RIBAboutAppListener {
    var router: RIBAboutAppRouting? { get set }
    var listener: RIBAboutAppListener? { get set }
}

protocol RIBAboutAppViewControllable: ViewControllable {
    func presentAsSheet(viewController: ViewControllable)
    func dismiss(viewController: ViewControllable)
}

typealias RIBAboutAppRouterProtocol =
    ViewableRouter<RIBAboutAppInteractable, RIBAboutAppViewControllable>
    & RIBAboutAppRouting

final class RIBAboutAppRouter: RIBAboutAppRouterProtocol {

    init(interactor: RIBAboutAppInteractable,
         viewController: RIBAboutAppViewControllable,
         aboutAppBuilder: RIBAboutAppBuildable) {
        self.aboutAppBuilder = aboutAppBuilder
        super.init(interactor: interactor, viewController: viewController)
        interactor.router = self
    }
    
    // MARK: - Routing
    
    func presentFullScreenAppearance() {
        if aboutApp == nil {
            attachAboutApp()
        }
    }
    
    func closeFullScreenAppearance() {
        detachAboutApp()
    }
    
    // MARK: - Private
    
    private weak var aboutApp: RIBAboutAppRouting?
    
    // MARK: Views
    
    private var fullScreenWrapper: ViewControllable?
    
    // MARK: Builders
    
    private let aboutAppBuilder: RIBAboutAppBuildable
    
    // MARK: Attaching
    
    func attachAboutApp() {
        let aboutApp = aboutAppBuilder.build(withListener: interactor,
                                             viewType: .fullScreen)
        let navController =
            UINavigationController(rootViewController: aboutApp.viewControllable)
        self.aboutApp = aboutApp
        self.fullScreenWrapper = navController
        attachChild(aboutApp)
        viewController.presentAsSheet(viewController: navController)
    }
    
    // MARK: Detaching
    
    func detachAboutApp() {
        if let fullScreenWrapper = fullScreenWrapper {
            viewController.dismiss(viewController: fullScreenWrapper)
            self.fullScreenWrapper = nil
        }
        if let aboutApp = aboutApp {
            detachChild(aboutApp)
        }
    }
    
}
