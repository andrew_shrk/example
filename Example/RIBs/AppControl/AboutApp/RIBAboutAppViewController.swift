//
//  RIBAboutAppViewController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 19.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import RxSwift
import UIKit

protocol RIBAboutAppPresentableListener: class {
    func didRequestFullScreenView()
    func didRequestCloseFullScreenView()
    func didRequestShowAgreements()
    func movingFromParent()
}

typealias RIBAboutAppVC = UIViewController
    & RIBAboutAppPresentable
    & RIBAboutAppViewControllable

typealias RIBAboutAppCellVC = RIBAboutAppVC
    & CellViewController
