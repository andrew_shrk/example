//
//  AuthStateStream.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift
import RxCocoa
import FirebaseAuth

enum AuthState {
    case loggedInWithRegistration, loggedInWithoutRegistration, loggedOut, none
}

protocol AuthStateStream {
    var authState: Observable<AuthState> { get }
}

protocol MutableAuthStateStream: AuthStateStream {
    func update(with state: AuthState)
}

class AuthStateStreamImpl: MutableAuthStateStream {
    var authState: Observable<AuthState> { variable.asObservable() }
    func update(with state: AuthState) {
        variable.accept(state)
    }
    private var variable = BehaviorRelay<AuthState>(value: .none)
}
