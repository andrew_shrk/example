//
//  ReviewsStream.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 20.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift
import RxCocoa

protocol ReviewsStreamProtocol {
    var value: Observable<[ReviewProtocol]> { get }
}

protocol MutableReviewsStreamProtocol: ReviewsStreamProtocol {
    func update(with newReviews: [ReviewProtocol])
}

class ReviewStream: MutableReviewsStreamProtocol {
    var value: Observable<[ReviewProtocol]> {
        relay.asObservable()
    }
    func update(with newReviews: [ReviewProtocol]) {
        relay.accept(newReviews)
    }
    private var relay = BehaviorRelay<[ReviewProtocol]>(value: [])
}
