//
//  OrdersStream.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 23.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RxSwift
import RxCocoa

protocol OrdersStream {
    var value: Observable<[OrderProtocol]> { get }
    func element(at index: Int) -> OrderProtocol
}
protocol MutableOrdersStream: OrdersStream {
    func update(with newOrders: [OrderProtocol])
    func update(whereOrderId orderId: String, order: OrderProtocol)
    func remove(whereOrderId orderId: String)
    func append(order: OrderProtocol)
}
class OrdersStreamImpl: MutableOrdersStream {
    
    var value: Observable<[OrderProtocol]> {
        relay.asObservable()
    }
    
    func update(with newOrders: [OrderProtocol]) {
        relay.accept(newOrders)
    }
    
    func update(whereOrderId orderId: String, order: OrderProtocol) {
        let oldValue = relay.value
        var newValue = oldValue.filter { $0.identifier != orderId }
        newValue.append(order)
        debugPrint(newValue.count)
        relay.accept(newValue)
    }
    func remove(whereOrderId orderId: String) {
        let oldValue = relay.value
        let newValue = oldValue.filter { $0.identifier != orderId }
        relay.accept(newValue)
    }
    func append(order: OrderProtocol) {
        var value = relay.value
        value.append(order)
        relay.accept(value)
    }
    func element(at index: Int) -> OrderProtocol {
        return relay.value[index]
    }
    
    private let relay = BehaviorRelay<[OrderProtocol]>(value: [])
}
