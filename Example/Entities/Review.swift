//
//  Review.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 20.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import Foundation

protocol ReviewProtocol: Codable {
    var title: String? { get set }
    var rating: Double? { get set }
    var text: String? { get set }
    var authorName: String? { get set }
    var dateCreation: Date? { get set }
}

struct Review: ReviewProtocol, Hashable {
    var title: String?
    var rating: Double?
    var text: String?
    var authorName: String?
    var dateCreation: Date?
}
