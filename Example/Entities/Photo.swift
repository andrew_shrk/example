//
//  Photo.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 23.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit

protocol PhotoProtocol: Codable {
    var storagePath: String? { get set }
    var downloadUrlString: String? { get set }
}
struct Photo: PhotoProtocol, Hashable {
    
    var storagePath: String?
    var downloadUrlString: String?
    
    enum CodingKeys: String, CodingKey {
        case storagePath = "storage_path"
        case downloadUrlString = "download_url_string"
    }
}
