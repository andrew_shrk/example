//
//  Location.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 02.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import CoreLocation
import FirebaseFirestore

protocol LocationType {
    var name: String? { get }
    var coordinates: GeoPoint { get }
}
protocol CoordinatesType {
    var longitude: Double { get }
    var latitude: Double { get }
}

struct Location: LocationType, Codable, Hashable {
    
    var name: String?
    var coordinates: GeoPoint
    
    static func == (lhs: Location, rhs: Location) -> Bool {
        return lhs.name == rhs.name
        && lhs.coordinates.latitude == rhs.coordinates.latitude
        && lhs.coordinates.longitude == rhs.coordinates.longitude
    }
}

extension CLLocationCoordinate2D: CoordinatesType {}
