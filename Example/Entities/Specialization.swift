//
//  Specialization.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 29.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import Foundation

struct Specialization: Unique, Codable, Hashable {
    
    var identifier: String?
    var name: String?
    var sort: Int?
    var groupId: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case sort
        case groupId = "group_id"
    }
}
