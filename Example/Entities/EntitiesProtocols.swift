//
//  Protocols.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 29.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import Foundation

protocol Unique {
    var identifier: String? { get set }
}
