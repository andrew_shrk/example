//
//  Order.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 23.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import FirebaseFirestore

protocol OrderProtocol: Codable {
    var identifier: String? { get set }
    var mainCategoryName: String? { get set }
    var subCategoryName: String? { get set }
    var description: String? { get set }
    var dateCreation: Timestamp? { get set }
    var attachedPhotos: [Photo]? { get set }
    var attachedPhotosUrs: [String]? { get set }
    var minBudget: Double? { get set }
    var maxBudget: Double? { get set }
    var authorName: String? { get set }
    var isFavorite: Bool? { get set }
    var status: OrderStatus? { get set }
    var locationName: String? { get set }
    var locationCoordinates: GeoPoint? { get set }
}
struct Order: OrderProtocol, Unique, Hashable {
    
    var identifier: String?
    var mainCategoryName: String?
    var mainCategoryId: String?
    var subCategoryName: String?
    var subCategoryId: String?
    var description: String?
    var dateCreation: Timestamp?
    var attachedPhotos: [Photo]?
    var attachedPhotosUrs: [String]?
    var minBudget: Double?
    var maxBudget: Double?
    var authorName: String?
    var isFavorite: Bool?
    var ownerIdentifier: String?
    var locationName: String?
    var locationCoordinates: GeoPoint?
    var displayPhoneNumber: String?
    var displayContactsPermission: Bool?
    var status: OrderStatus?
    
    enum CodingKeys: String, CodingKey {
        case mainCategoryName = "main_category_name"
        case mainCategoryId = "main_category_id"
        case subCategoryName = "sub_category_name"
        case subCategoryId = "sub_category_id"
        case description = "description"
        case dateCreation = "date_creation"
        case minBudget = "min_budget"
        case maxBudget = "max_budget"
        case authorName = "author_name"
        case ownerIdentifier = "owner_id"
        case locationName = "location_name"
        case locationCoordinates = "location_coordinates"
        case displayPhoneNumber = "display_phone_number"
        case displayContactsPermission = "display_contacts_permission"
        case status = "status"
        case attachedPhotos = "attached_photos"
        case attachedPhotosUrs = "attached_photos_urls"
    }
}

enum OrderStatus: String, Codable {
    case none
    case creating
    case creatingCanceled
    case draft
    case publishing
}
