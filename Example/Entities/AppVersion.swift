//
//  AppVersion.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 19.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import Foundation

protocol AppVersionProtocol {
    var versionNumber: String { get }
    var buildNumber: String { get }
}

struct AppVersion: AppVersionProtocol, Hashable {
    let versionNumber: String
    let buildNumber: String
}
