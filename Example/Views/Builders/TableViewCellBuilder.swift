//
//  TableViewCellBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 04.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit

class TableViewCellBuilder {
    
    func buildFilledButtonCell(withTitle title: String?,
                               fillColor: UIColor = .systemBlue) -> UITableViewCell {
        let filledButtonCell = UITableViewCell()
        filledButtonCell.backgroundColor = fillColor
        filledButtonCell.textLabel?.textAlignment = .center
        filledButtonCell.textLabel?.textColor = .white
        filledButtonCell.selectionStyle = .blue
        filledButtonCell.textLabel?.text = title
        return filledButtonCell
    }
    
    func buildButtonCellWithAccessory(withTitle title: String?,
                                      image: UIImage?,
                                      tintColor: UIColor = .systemBlue) -> UITableViewCell {
        let accessoryView = UIImageView(image: image, highlightedImage: nil)
        accessoryView.tintColor = tintColor
        let buttonWithAccessoryCell = UITableViewCell()
        buttonWithAccessoryCell.textLabel?.text = title
        buttonWithAccessoryCell.textLabel?.textColor = tintColor
        buttonWithAccessoryCell.accessoryView = accessoryView
        return buttonWithAccessoryCell
    }
    
    func buildTextFieldCell() -> UITextFieldTableViewCell {
        return UITextFieldTableViewCell()
    }
    
    func buildPhoneFieldCell() -> UIPhoneFieldTableViewCell {
        return UIPhoneFieldTableViewCell()
    }
    
    func buildSwitchCell() -> UISwitchTableViewCell {
        return UISwitchTableViewCell()
    }
    
}
