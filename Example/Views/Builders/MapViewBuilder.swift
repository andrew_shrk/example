//
//  MapViewBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 08.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewBuilder {
    
    func buildMapView() -> GMSMapView {
        let camera = GMSCameraPosition()
        let mapView = GMSMapView(frame: .zero, camera: camera)
        do {
          mapView.mapStyle = try GMSMapStyle(jsonString: kMapStyle)
        } catch {
          NSLog("One or more of the map styles failed to load. \(error)")
        }
        mapView.isMyLocationEnabled = true
        return mapView
    }
    
    // MARK: - Private
    
    private lazy var kMapStyle = """
    [
      {
        "featureType": "poi.business",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "labels.text",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      }
    ]
    """
}
