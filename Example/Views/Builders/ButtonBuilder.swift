//
//  Buttons.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 08.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit

class ButtonBuilder {
    
    func buildMyLocation() -> UIButton {
        let toUserLocationButton = UIButton(type: .system)
        let locationImage = UIImage(named: "icon_my_location_24dp")
        toUserLocationButton.setImage(locationImage, for: .normal)
        toUserLocationButton.layer.cornerRadius = 18
        toUserLocationButton.backgroundColor = .white
        toUserLocationButton.setShadow(.default)
        toUserLocationButton.snp.makeConstraints { (maker) in
            maker.height.width.equalTo(36)
        }
        return toUserLocationButton
    }
    
}
