//
//  UIBuilder.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 04.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import Foundation

// TODO: Реализовать протоколы описывающие UI builders
/// Фасад для UI builders
class UIBuilder {
    
    lazy var tableViewCellBuilder = TableViewCellBuilder()
    
    lazy var buttonBuilder = ButtonBuilder()
    
    lazy var mapViewBuilder = MapViewBuilder()
    
}
