//
//  UIImageViewer.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 24.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit
import Kingfisher

protocol UIImageViewerProtocol {
    var view: UIView! { get }
    func setPhotos(photos: [PhotoProtocol])
    func setPhotos(withDownloadUrls urls: [String])
}

class UIImageViewer: UIPageViewController, UIImageViewerProtocol {
    
    init() {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        dataSource = self
        delegate = self
        view.clipsToBounds = true
        buildPageControl()
    }
    
    // MARK: - Private
    
    private var orderedViewControllers: [UIViewController] = []
    
    private var pageControl: UIPageControl!
    
    // MARK: - UIImageViewerProtocol
    
    func setPhotos(photos: [PhotoProtocol]) {
        var viewControllers: [UIViewController] = []
        for photo in photos {
            if let urlString = photo.downloadUrlString, let url = URL(string: urlString) {
                let viewController = buildImageViewController(withUrl: url)
                viewControllers.append(viewController)
            }
        }
        if let first = viewControllers.first {
            self.setViewControllers([first],
                direction: .forward,
                animated: false,
                completion: nil)
        }
        self.orderedViewControllers = viewControllers
        self.pageControl.numberOfPages = viewControllers.count
        self.pageControl.currentPage = 0
    }
    func setPhotos(withDownloadUrls urls: [String]) {
        var viewControllers: [UIViewController] = []
        for urlString in urls {
            if let url = URL(string: urlString) {
                let viewController = buildImageViewController(withUrl: url)
                viewControllers.append(viewController)
            }
        }
        if let first = viewControllers.first {
            self.setViewControllers([first],
                direction: .forward,
                animated: false,
                completion: nil)
        }
        self.orderedViewControllers = viewControllers
        self.pageControl.numberOfPages = viewControllers.count
        self.pageControl.currentPage = 0
    }

    // MARK: - UI
    
    // MARK: Builders
    
    private func buildImageViewController(withUrl url: URL) -> UIViewController {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.kf.setImage(with: url)
        let viewController = UIViewController()
        viewController.view.addSubview(imageView)
        viewController.view.clipsToBounds = true
        imageView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(viewController.view)
        }
        return viewController
    }
    
    private func buildPageControl() {
        let pageControl = UIPageControl()
        view.addSubview(pageControl)
        pageControl.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(self.view)
            maker.bottom.equalTo(self.view.snp.bottom).offset(-4)
        }
        self.pageControl = pageControl
    }
    
}

// MARK: - UIPageViewControllerDataSource
extension UIImageViewer: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count

        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
}

// MARK: - UIPageViewControllerDelegate
extension UIImageViewer: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController],
                            transitionCompleted completed: Bool) {
        if let firstViewController = pageViewController.viewControllers?.first,
            let firstIndex = orderedViewControllers.firstIndex(of: firstViewController) {
            pageControl.currentPage = firstIndex
        }
    }
}
