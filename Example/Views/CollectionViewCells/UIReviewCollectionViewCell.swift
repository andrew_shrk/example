//
//  UIReviewCollectionViewCell.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 20.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit
import Cosmos

class UIReviewCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Views
    
    lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = .preferredFont(forTextStyle: .headline)
        titleLabel.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        return titleLabel
    }()
    lazy var ratingView: CosmosView = {
        let ratingView = CosmosView()
        ratingView.settings.starSize = 16
        ratingView.settings.starMargin = 0
        ratingView.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        return ratingView
    }()
    lazy var textView: UILabel = {
        let textView = UILabel()
        textView.isUserInteractionEnabled = false
        textView.numberOfLines = 0
        textView.font = UIFont.preferredFont(forTextStyle: .body).withSize(14)
        textView.contentMode = .top
        return textView
    }()
    lazy var userNameLabel: UILabel = {
        let userNameLabel = UILabel()
        userNameLabel.font = .preferredFont(forTextStyle: .footnote)
        userNameLabel.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        userNameLabel.textAlignment = .right
        if #available(iOS 13.0, *) {
            userNameLabel.textColor = .secondaryLabel
        } else {
            userNameLabel.textColor = .lightGray
        }
        return userNameLabel
    }()
    lazy var dateCreationLabel: UILabel = {
        let dateCreationLabel = UILabel()
        dateCreationLabel.font = .preferredFont(forTextStyle: .footnote)
        dateCreationLabel.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        dateCreationLabel.textAlignment = .right
        if #available(iOS 13.0, *) {
            dateCreationLabel.textColor = .secondaryLabel
        } else {
            dateCreationLabel.textColor = .lightGray
        }
        return dateCreationLabel
    }()
    private lazy var headerViewFirstLine: UIStackView = {
        let headerViewFirstLine = UIStackView()
        headerViewFirstLine.axis = .horizontal
        headerViewFirstLine.spacing = 2
        headerViewFirstLine.addArrangedSubview(titleLabel)
        headerViewFirstLine.addArrangedSubview(dateCreationLabel)
        return headerViewFirstLine
    }()
    private lazy var headerViewSecondLine: UIStackView = {
        let headerViewSecondLine = UIStackView()
        headerViewSecondLine.axis = .horizontal
        headerViewSecondLine.spacing = 2
        headerViewSecondLine.addArrangedSubview(ratingView)
        headerViewSecondLine.addArrangedSubview(userNameLabel)
        return headerViewSecondLine
    }()
    private lazy var headerView: UIStackView = {
        let headerView = UIStackView()
        headerView.axis = .vertical
        headerView.spacing = 4
        headerView.distribution = .fill
        headerView.addArrangedSubview(headerViewFirstLine)
        headerView.addArrangedSubview(headerViewSecondLine)
        return headerView
    }()
    private lazy var footerView: UIStackView = {
        let footerView = UIStackView()
        footerView.axis = .horizontal
        footerView.spacing = 2
        footerView.addArrangedSubview(userNameLabel)
        footerView.addArrangedSubview(dateCreationLabel)
        return footerView
    }()
    private lazy var container: UIStackView = {
        let container = UIStackView()
        container.axis = .vertical
        container.spacing = 10
        container.addArrangedSubview(headerView)
        container.addArrangedSubview(textView)
        return container
    }()
    
    // MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    // MARK: - Private

    private func setup() {
        contentView.addSubview(container)
        if #available(iOS 13.0, *) {
            contentView.backgroundColor = .secondarySystemGroupedBackground
        } else {
            contentView.backgroundColor = .white
        }
        contentView.layoutMargins = UIEdgeInsets(top: 15, left: 20, bottom: 15, right: 20)
        contentView.layer.cornerRadius = 10
        contentView.preservesSuperviewLayoutMargins = true
        setupConstraints()
    }
    
    // MARK: Contraints
    
    private func setupConstraints() {
        contentView.addSubview(headerView)
        contentView.addSubview(textView)
        headerView.snp.makeConstraints { (maker) in
            maker.top.equalTo(self.contentView.snp.topMargin)
            maker.leading.equalTo(self.contentView.snp.leadingMargin)
            maker.trailing.equalTo(self.contentView.snp.trailingMargin)
        }
        textView.snp.makeConstraints { (maker) in
            maker.top.equalTo(self.headerView.snp.bottom).offset(10)
            maker.leading.equalTo(self.contentView.snp.leadingMargin)
            maker.trailing.equalTo(self.contentView.snp.trailingMargin)
            maker.bottom.lessThanOrEqualTo(self.contentView.snp.bottomMargin)
        }
    }
    
}
