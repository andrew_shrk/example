//
//  UIImageCollectionViewCell.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 29.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit

class UIImageCollectionViewCell: UICollectionViewCell {
    
    // MARK: Inits
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    // MARK: Public methods
    
    func setImage(_ image: UIImage?) {
        imageView.image = image
    }
    func startActivityAnimating() {
        activityIndicator.startAnimating()
    }
    func stopActivityAnimating() {
        activityIndicator.stopAnimating()
    }
    
    // MARK: Views
    
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        return imageView
    }()
    lazy var removeButton: UIButton = {
        let image = UIImage(named: "icon_delete_forever")
        let removeButton = UIButton(type: .system)
        removeButton.setImage(image, for: .normal)
        if #available(iOS 13.0, *) {
            removeButton.tintColor = .systemBackground
        } else {
            removeButton.tintColor = .white
        }
        removeButton.layer.cornerRadius = 16
        removeButton.contentEdgeInsets = .init(top: 4, left: 4, bottom: 4, right: 4)
        removeButton.backgroundColor = .systemRed
        return removeButton
    }()
    
    // MARK: - Private
    
    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .largeIfAvailable)
        activityIndicator.startAnimating()
        if #available(iOS 13.0, *) {
            activityIndicator.backgroundColor = UIColor.systemBackground.withAlphaComponent(0.75)
        } else {
            activityIndicator.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        }
        return activityIndicator
    }()
    
    // MARK: Setup
    
    private func setup() {
        self.addSubview(imageView)
        imageView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self)
        }
        self.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { (maker) in
            maker.edges.equalTo(self)
        }
        self.addSubview(removeButton)
        removeButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(self).inset(8)
            maker.trailing.equalTo(self).inset(8)
        }
        
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }
    
}
