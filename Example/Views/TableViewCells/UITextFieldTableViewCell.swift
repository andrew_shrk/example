//
//  UITextFieldTableViewCell.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 04.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit

class UITextFieldTableViewCell: UITableViewCell {
    
    // MARK: Inits
    
    convenience init() {
        self.init(reuseIdentifier: nil)
    }
    init(reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        setup()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: UI
    
    lazy var textField: UITextField = {
        let textField = UITextField()
        return textField
    }()
    lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = .preferredFont(forTextStyle: .subheadline)
        return titleLabel
    }()
    
    // MARK: - Private
    
    private lazy var container: UIStackView = {
        let container = UIStackView()
        container.axis = .vertical
        container.spacing = 8
        return container
    }()
    
    private func setup() {
        contentView.addSubview(container)
        container.addArrangedSubview(titleLabel)
        container.addArrangedSubview(textField)
        container.snp.makeConstraints { (maker) in
            maker.edges.equalTo(contentView.snp.margins)
        }
    }
    
}
