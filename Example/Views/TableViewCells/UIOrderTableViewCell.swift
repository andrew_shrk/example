//
//  UIOrderTableViewCell.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 24.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit

protocol UIOrderTableViewCellProtocol {
    func setTitle(title: String?)
    func setSubTitle(subTitle: String?)
    func setDescriptionText(text: String?)
    func setBudget(budgetFrom: Double?, budgetTo: Double?)
    func setAuthorName(name: String?)
    func setDateCreation(dateString: String?)
    func setFavorite(isFavorite: Bool?)
    func setPhotos(_ photos: [PhotoProtocol]?)
    func setPhotos(withDownloadUrls urls: [String]?)
}

class UIOrderTableViewCell: UITableViewCell, UIOrderTableViewCellProtocol {
    
    // MARK: - UIOrderTableViewCellProtocol
    
    func setTitle(title: String?) {
        titleLabel.text = title
    }
    func setSubTitle(subTitle: String?) {
        subTitleLabel.text = subTitle
    }
    func setDescriptionText(text: String?) {
        descriptionLabel.text = text
    }
    func setBudget(budgetFrom: Double? = nil, budgetTo: Double? = nil) {
        var value: [String] = []
        if let budgetFrom = budgetFrom {
            value.append(String(format: "от %.0f", budgetFrom))
        }
        if let budgetTo = budgetTo {
            value.append(String(format: "до %.0f", budgetTo))
        }
        if value.count != 0 {
            budgetValueLabel.text = "\(value.joined(separator: " ")) руб."
            budgetContainer.isHidden = false
        } else {
            budgetContainer.isHidden = true
        }
    }
    func setAuthorName(name: String?) {
        authorNameLabel.text = name
    }
    func setDateCreation(dateString: String?) {
        dateCreationLabel.text = dateString
    }
    func setFavorite(isFavorite: Bool?) {
        favoriteButton.isSelected = isFavorite ?? false
    }
    func setPhotos(_ photos: [PhotoProtocol]?) {
        if let photos = photos, photos.count != 0 {
            photoViewer.setPhotos(photos: photos)
            photoViewer.view.isHidden = false
        } else {
            photoViewer.view.isHidden = true
        }
    }
    func setPhotos(withDownloadUrls urls: [String]?) {
        if let urls = urls, urls.count != 0 {
            photoViewer.setPhotos(withDownloadUrls: urls)
            photoViewer.view.isHidden = false
        } else {
            photoViewer.setPhotos(withDownloadUrls: [])
            photoViewer.view.isHidden = true
        }
    }
    func setStatus(_ orderStatus: OrderStatus?) {
        if let orderStatus = orderStatus {
            switch orderStatus {
            case .draft:
                statusLabel.text = "Черновик"
                statusLabel.backgroundColor = .lightGray
                statusLabel.isHidden = false
            default:
                statusLabel.isHidden = true
            }
        } else {
            statusLabel.isHidden = true
        }
    }
    
    // MARK: - Views
    
    lazy var header: UIStackView = {
        let header = UIStackView()
        header.axis = .horizontal
        header.addArrangedSubview(headerLeftContainer)
        header.addArrangedSubview(headerRightContainer)
        return header
    }()
    lazy var footer: UIStackView = {
        let footer = UIStackView()
        footer.axis = .horizontal
        footer.spacing = 4.0
        footer.addArrangedSubview(authorNameLabel)
        footer.addArrangedSubview(dateCreationLabel)
        return footer
    }()
    lazy var content: UIStackView = {
        let content = UIStackView()
        content.axis = .vertical
        content.spacing = 10
        content.alignment = .leading
        content.addArrangedSubview(statusLabel)
        content.addArrangedSubview(descriptionLabel)
        content.addArrangedSubview(budgetContainer)
        return content
    }()
    // MARK: Footer
    lazy var authorNameLabel: UILabel = {
        let authorNameLabel = UILabel()
        authorNameLabel.font = .preferredFont(forTextStyle: .footnote)
        if #available(iOS 13.0, *) {
            authorNameLabel.textColor = .secondaryLabel
        } else {
            authorNameLabel.textColor = .lightGray
        }
        return authorNameLabel
    }()
    lazy var dateCreationLabel: UILabel = {
        let dateCreationLabel = UILabel()
        dateCreationLabel.font = .preferredFont(forTextStyle: .footnote)
        dateCreationLabel.textAlignment = .right
        if #available(iOS 13.0, *) {
            dateCreationLabel.textColor = .secondaryLabel
        } else {
            dateCreationLabel.textColor = .lightGray
        }
        return dateCreationLabel
    }()
    // MARK: Header
    lazy var headerLeftContainer: UIStackView = {
        let headerLeftContainer = UIStackView()
        headerLeftContainer.axis = .vertical
        headerLeftContainer.spacing = 2.0
        headerLeftContainer.addArrangedSubview(titleLabel)
        headerLeftContainer.addArrangedSubview(subTitleLabel)
        return headerLeftContainer
    }()
    lazy var headerRightContainer: UIStackView = {
        let headerRightContainer = UIStackView()
        headerRightContainer.axis = .vertical
        return headerRightContainer
    }()
    lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = .preferredFont(forTextStyle: .headline)
        return titleLabel
    }()
    lazy var subTitleLabel: UILabel = {
        let subTitleLabel = UILabel()
        subTitleLabel.font = .preferredFont(forTextStyle: .subheadline)
        if #available(iOS 13.0, *) {
            subTitleLabel.textColor = .secondaryLabel
        } else {
            subTitleLabel.textColor = .lightGray
        }
        return subTitleLabel
    }()
    lazy var favoriteButton: UIButton = {
        let favoriteButton = UIButton()
        favoriteButton.setImage(UIImage(named: "icon_star_border_24pt"), for: .normal)
        favoriteButton.setImage(UIImage(named: "icon_star_24pt"), for: .selected)
        return favoriteButton
    }()
    // MARK: Content
    lazy var photoViewer: UIImageViewer = {
        let photoViewer = UIImageViewer()
        photoViewer.view.snp.makeConstraints { (maker) in
            maker.height.equalTo(photoViewer.view.snp.width).multipliedBy(0.75)
        }
        return photoViewer
    }()
    lazy var photoViewerContainer: UIStackView = {
        let photoViewerContainer = UIStackView()
        photoViewerContainer.axis = .vertical
        photoViewerContainer.spacing = 15
        photoViewerContainer.addArrangedSubview(photoViewer.view)
        photoViewerContainer.addArrangedSubview(header)
        return photoViewerContainer
    }()
    lazy var descriptionLabel: UILabel = {
        let descriptionLabel = UILabel()
        descriptionLabel.numberOfLines = 0
        return descriptionLabel
    }()
    lazy var budgetTitleLabel: UILabel = {
        let budgetTitleLabel = UILabel()
        budgetTitleLabel.text = "Бюджет:"
        budgetTitleLabel.font = .systemFont(ofSize: 16, weight: .thin)
        budgetTitleLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        return budgetTitleLabel
    }()
    lazy var budgetValueLabel: UILabel = {
        let budgetValueLabel = UILabel()
        budgetValueLabel.font = .boldSystemFont(ofSize: 16)
        budgetValueLabel.textAlignment = .left
        return budgetValueLabel
    }()
    lazy var budgetContainer: UIStackView = {
        let budgetContainer = UIStackView()
        budgetContainer.axis = .horizontal
        budgetContainer.spacing = 4.0
        budgetContainer.addArrangedSubview(budgetTitleLabel)
        budgetContainer.addArrangedSubview(budgetValueLabel)
        return budgetContainer
    }()
    lazy var statusLabel: UILabel = {
        let statusLabel = UILabel()
        statusLabel.layer.cornerRadius = 2
        statusLabel.textColor = .white
        statusLabel.font = .preferredFont(forTextStyle: .headline)
        statusLabel.clipsToBounds = true
        statusLabel.textAlignment = .center
        statusLabel.snp.makeConstraints { (maker) in
            maker.height.equalTo(24)
            maker.width.equalTo(100)
        }
        return statusLabel
    }()
    
    // MARK: - Initializers
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    // MARK: - Private
    
    private func setup() {
        setupViews()
    }
    
    private func setupViews() {
        contentView.addSubview(header)
        contentView.addSubview(content)
        contentView.addSubview(footer)
        contentView.addSubview(favoriteButton)
        contentView.addSubview(photoViewerContainer)
        photoViewerContainer.snp.makeConstraints { (maker) in
            maker.top.equalTo(self.contentView.snp.topMargin).offset(15)
            maker.leading.equalTo(self.contentView.snp.leadingMargin)
            maker.trailing.equalTo(self.contentView.snp.trailingMargin)
        }
        content.snp.makeConstraints { (maker) in
            maker.top.equalTo(header.snp.bottom).offset(10)
            maker.leading.equalTo(self.contentView.snp.leadingMargin)
            maker.trailing.equalTo(self.contentView.snp.trailingMargin)
        }
        footer.snp.makeConstraints { (maker) in
            maker.top.equalTo(content.snp.bottom).offset(15)
            maker.bottom.equalTo(self.contentView.snp.bottomMargin)
            maker.leading.equalTo(self.contentView.snp.leadingMargin)
            maker.trailing.equalTo(self.contentView.snp.trailingMargin)
        }
        favoriteButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(header)
            maker.trailing.equalTo(self.contentView.snp.trailingMargin)
        }
    }
    
}
