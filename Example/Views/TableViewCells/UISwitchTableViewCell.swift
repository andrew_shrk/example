//
//  UISwitcherTableViewCell.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 04.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit

class UISwitchTableViewCell: UITableViewCell {
    
    // MARK: Inits
    
    convenience init() {
        self.init(reuseIdentifier: nil)
    }
    init(reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        setup()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: UI
    
    lazy var switcher: UISwitch = {
        let switcher = UISwitch()
        return switcher
    }()
    
    // MARK: - Private
    
    private lazy var container: UIStackView = {
        let container = UIStackView()
        container.axis = .vertical
        container.spacing = 8
        return container
    }()
    
    private func setup() {
        accessoryView = switcher
    }
    
}
