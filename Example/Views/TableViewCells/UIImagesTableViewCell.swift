//
//  UIImagesTableViewCell.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 29.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit

class UIImagesTableViewCell: UITableViewCell {
    
    convenience init(layout: UICollectionViewLayout) {
        self.init(style: .default, reuseIdentifier: nil, layout: layout)
    }
    init(style: UITableViewCell.CellStyle, reuseIdentifier: String?, layout: UICollectionViewLayout) {
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private
    
    // MARK: UI
    
    let collectionView: UICollectionView
    
    // MARK: Cell setup
    
    private func setup() {
        contentView.addSubview(collectionView)
        collectionView.snp.makeConstraints { (maker) in
            maker.top.equalTo(contentView.snp.topMargin)
            maker.leading.equalTo(contentView.snp.leadingMargin)
            maker.trailing.equalTo(contentView.snp.trailingMargin)
            maker.bottom.equalTo(contentView.snp.bottomMargin)
        }
    }
    
}
