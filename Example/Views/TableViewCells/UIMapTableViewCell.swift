//
//  UIMapTableViewCell.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 02.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit
import GoogleMaps

protocol UIMapTableViewCellProtocol {
    func setCoordinates(_ coordinates: CLLocationCoordinate2D)
    func setLocationName(_ locationName: String?)
}

class UIMapTableViewCell: UITableViewCell, UIMapTableViewCellProtocol {
    
    // MARK: Inits
    
    convenience init() {
        self.init(reuseIdentifier: nil)
    }
    init(reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        setup()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UIMapTableViewCellProtocol
    
    func setCoordinates(_ coordinates: CLLocationCoordinate2D) {
        let cameraPosition = GMSCameraPosition(target: coordinates, zoom: 17)
        mapView.camera = cameraPosition
        marker.position = coordinates
    }
    func setLocationName(_ locationName: String?) {
        locationNameLabel.text = locationName
        locationNameLabel.isHidden = locationName == nil
    }
    
    // MARK: - Private
    
    private func setup() {
        mainContainer.addArrangedSubview(locationNameLabel)
        mainContainer.addArrangedSubview(mapView)
        contentView.addSubview(mainContainer)
        mapView.snp.makeConstraints { (maker) in
            maker.height.equalTo(128)
        }
        mainContainer.snp.makeConstraints { (maker) in
            maker.edges.equalTo(contentView.snp.margins)
        }
    }
    
    // MARK: UI
    
    private lazy var marker: GMSMarker = {
        let marker = GMSMarker()
        marker.map = mapView
        return marker
    }()
    private lazy var mapView: GMSMapView = {
        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
        let mapView = GMSMapView(frame: .zero, camera: camera)
        mapView.isUserInteractionEnabled = false
        mapView.layer.cornerRadius = 10
        mapView.setShadow(.default)
        return mapView
    }()
    private lazy var locationNameLabel: UILabel = {
        let locationNameLabel = UILabel()
        locationNameLabel.numberOfLines = 0
        return locationNameLabel
    }()
    private lazy var mainContainer: UIStackView = {
        let mainContainer = UIStackView()
        mainContainer.axis = .vertical
        mainContainer.spacing = 8.0
        return mainContainer
    }()
    
}
