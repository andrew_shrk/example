//
//  UITableViewCell.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 14.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit
import RIBs

extension UITableViewCell {
    convenience init(viewController: UIViewController, margins: Bool) {
        self.init()
        if let view = viewController.view {
            contentView.addSubview(viewController.view)
            view.snp.makeConstraints { (maker) in
                if margins {
                    maker.top.equalTo(contentView.snp_topMargin)
                    maker.bottom.equalTo(contentView.snp_bottomMargin)
                    maker.leading.equalTo(contentView.snp_leadingMargin)
                    maker.trailing.equalTo(contentView.snp_trailingMargin)
                } else {
                    maker.top.bottom.leading.trailing.equalTo(contentView)
                }
            }
        } else {
            fatalError()
        }
    }
}
