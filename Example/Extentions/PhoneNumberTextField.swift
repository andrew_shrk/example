//
//  PhoneNumberTextField.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 14.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import PhoneNumberKit

extension PhoneNumberTextField {
    var defaultRegion: String {
        get { 
            return "RU"
        }
        // swiftlint:disable  unused_setter_value
        set {}
    }
}
