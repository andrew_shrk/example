//
//  ViewControllable.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

extension UIViewController {
    func present(viewController: ViewControllable) {
        present(viewController: viewController, style: .fullScreen)
    }
    func presentAsSheet(viewController: ViewControllable) {
        present(viewController: viewController, style: .formSheet)
    }
    func present(viewController: ViewControllable, style: UIModalPresentationStyle) {
        viewController.uiviewController.modalPresentationStyle = style
        present(viewController.uiviewController, animated: true, completion: nil)
    }
    
    func dismiss(viewController: ViewControllable) {
        if presentedViewController === viewController.uiviewController {
            dismiss(animated: true, completion: nil)
        }
    }
    func buildTabBarItem(withTitle title: String, imageName: String) {
        let item = UITabBarItem()
        item.title = title
        if let image = UIImage(named: imageName) {
            item.image = image
        }
        tabBarItem = item
    }
    func addRightBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem, target: Any?, action: Selector?) {
        let barButtonItem = UIBarButtonItem(barButtonSystemItem: barButtonSystemItem, target: target, action: action)
        navigationItem.rightBarButtonItem = barButtonItem
    }
    func addLeftBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem, target: Any?, action: Selector?) {
        let barButtonItem = UIBarButtonItem(barButtonSystemItem: barButtonSystemItem, target: target, action: action)
        navigationItem.leftBarButtonItem = barButtonItem
    }
}
extension UISplitViewController {
    func presentAsMaster(viewController: ViewControllable) {
        viewControllers.append(viewController.uiviewController)
    }
    func presentAsDetailed(viewController: ViewControllable) {
        showDetailViewController(viewController.uiviewController, sender: self)
    }
}
extension UIBarButtonItem.SystemItem {
    static func close(or item: UIBarButtonItem.SystemItem) -> UIBarButtonItem.SystemItem {
        if #available(iOS 13.0, *) {
            return .close
        } else {
            return item
        }
    }
}
