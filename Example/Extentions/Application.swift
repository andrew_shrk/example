//
//  Application.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 12.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit

extension UIApplication {
    var visibleViewController: UIViewController? {
        var viewController = UIApplication.shared.keyWindow?.rootViewController
        while let presentedVc = viewController?.presentedViewController {
            if let navVc = (presentedVc as? UINavigationController)?.viewControllers.last {
                viewController = navVc
            } else if let tabVc = (presentedVc as? UITabBarController)?.selectedViewController {
                viewController = tabVc
            } else {
                viewController = presentedVc
            }
        }
        return viewController
    }
    func showErrorPrompt(withTitle title: String?, _ error: Error) {
        let title = title ?? "Ошибка"
        let message = error.localizedDescription
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        let action = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
        alert.addAction(action)
        visibleViewController?.present(alert, animated: true, completion: nil)
    }
}
