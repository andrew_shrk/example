//
//  UINavigationController.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 13.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs
import UIKit

extension UINavigationController: ViewControllable {
    
    public var uiviewController: UIViewController {
        return self
    }
    
    convenience init(rootViewController: ViewControllable) {
        self.init(rootViewController: rootViewController.uiviewController)
    }
}
