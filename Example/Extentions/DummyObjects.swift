//
//  DummyObjects.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 11.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit
import SnapKit

extension UIViewController {
    func loadWithDummyText(_ text: String) {
        let dummyLabel = UILabel()
        dummyLabel.text = text
        dummyLabel.textAlignment = .center
        if #available(iOS 11.0, *) {
            dummyLabel.font = UIFont.preferredFont(forTextStyle: .largeTitle)
        } else {
            dummyLabel.font = UIFont.preferredFont(forTextStyle: .title3)
        }
        dummyLabel.numberOfLines = 0
        view.addSubview(dummyLabel)
        dummyLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(self.view).inset(20)
            maker.centerY.equalTo(self.view)
        }
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
    }
}
