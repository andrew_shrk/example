//
//  UIActivityIndicatorView.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 03.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit

extension UIActivityIndicatorView.Style {
    static var largeIfAvailable: UIActivityIndicatorView.Style {
        if #available(iOS 13.0, *) {
            return .large
        } else {
            return .whiteLarge
        }
    }
}
