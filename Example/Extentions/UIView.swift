//
//  UIView.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 02.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit

enum ViewShadow {
    case `default`
    case none
}

extension UIView {
    func setShadow(_ shadow: ViewShadow) {
        switch shadow {
        case .none:
            layer.shadowOpacity = 0.0
            layer.shadowRadius = 0.0
            layer.shadowColor = nil
        case .default:
            layer.shadowColor = UIColor.black.cgColor
            layer.shadowOpacity = 0.2
            layer.shadowOffset = .zero
            layer.shadowRadius = 10
        }
    }
}
