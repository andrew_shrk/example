//
//  Codable.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 05.03.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import FirebaseFirestore
import CodableFirebase
import CoreLocation

extension DocumentReference: DocumentReferenceType {}
extension GeoPoint: GeoPointType {}
extension FieldValue: FieldValueType {}
extension Timestamp: TimestampType {}

extension GeoPoint {
    func clGeoPoint() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}
extension CLLocationCoordinate2D {
    func fsCoordinate() -> GeoPoint {
        return GeoPoint(latitude: latitude, longitude: longitude)
    }
}
 
