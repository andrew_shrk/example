//
//  CellViewControllable.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 18.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import UIKit

protocol CellViewController: class {
    var viewCell: UITableViewCell { get }
    var contentView: UIView { get }
}

extension CellViewController where Self: UIViewController {
    var viewCell: UITableViewCell {
        guard let viewCell = view as? UITableViewCell else {
            fatalError("Cannot cast root view to UITableViewCell")
        }
        return viewCell
    }
    var contentView: UIView {
        return viewCell.contentView
    }
}

enum ViewType: String {
    case cell, fullScreen
}
