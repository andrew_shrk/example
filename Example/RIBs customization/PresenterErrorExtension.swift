//
//  PresenterErrorExtension.swift
//  Yamaster
//
//  Created by Андрей Дурыманов on 18.02.2020.
//  Copyright © 2020 Yamaster. All rights reserved.
//

import RIBs

extension Presentable where Self: UIViewController {
    func showErrorPrompt(withTitle title: String?, error: Error) {
        let title = title ?? "Ошибка"
        let message = error.localizedDescription
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        let action = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
